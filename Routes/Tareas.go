package Routes

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Middlewares"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Proyects"
)

func RouteTareas(app *gin.Engine) {
	tareas := app.Group("/tareas", Middlewares.Auth, Middlewares.Vigente)
	{
		tareas.GET("", Proyects.GetTareasMes)
		tareas.GET("/:ID", Proyects.GetTarea)
		tareas.PUT("/:ID", Middlewares.PermisoApp, Proyects.UpdateTarea)
		tareas.PUT("/:ID/cancelar", Middlewares.PermisoApp, Proyects.CancelarTarea)
		tareas.PUT("/:ID/estatus", Middlewares.PermisoApp, Proyects.CambiarEstatusTarea)
		tareas.PUT("/:ID/participantes", Middlewares.PermisoApp, Proyects.UpdateTareaParticipantes)
		tareas.POST("/:ID/comentarios", Middlewares.PermisoApp, Proyects.CreateComentario)
		tareas.POST("/:ID/comentarios/:Comentario/respuestas", Middlewares.PermisoApp, Proyects.CreateComentario)
		tareas.PUT("/:ID/comentarios/:Comentario", Middlewares.PermisoApp, Proyects.UpdateComentario)
	}

}
