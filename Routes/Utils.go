package Routes

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Middlewares"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Utils"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Notebooks"
)

func RouteUtils(app *gin.Engine) {
	utils := app.Group("/utils", Middlewares.Auth)
	{
		utils.GET("/paises", Utils.GetPaises)
		utils.GET("/escuelas", Utils.GetEscuelas)
		utils.GET("/empresas", Utils.GetEmpresas)
		utils.GET("/apps", Utils.GetApps)
	}
}

func RouteNotebooks(app *gin.Engine) {
	notebooks := app.Group("/notebooks", Middlewares.Auth, Middlewares.Vigente)
	{
		notebooks.POST("", Notebooks.Create)
		notebooks.PUT("/:ID", Notebooks.Update)
		notebooks.PUT("/:ID/default", Notebooks.UpdateDefault)
		notebooks.DELETE("/:ID", Notebooks.Delete)
	}
}
