package Routes

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Reuniones"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Middlewares"
)

func RouteReuniones(app *gin.Engine) {
	app.GET("/reuniones/:ID/desarrollo", Reuniones.ConnectReunion)
	app.GET("/reuniones/:ID/confirmar", Reuniones.Confirmar)
	app.GET("/reuniones/:ID/pdf", Reuniones.Pdf)
	reuniones := app.Group("/reuniones", Middlewares.Auth, Middlewares.Vigente)
	{
		reuniones.GET("/", Reuniones.Disponibilidad)
		reuniones.POST("/", Middlewares.PermisoApp, Reuniones.Agendar)
		reuniones.PUT("/:ID", Middlewares.PermisoApp, Reuniones.Save)
		reuniones.GET("/:ID/etiquetas", Reuniones.GetEtiquetas)
		reuniones.POST("/:ID/compartir", Middlewares.PermisoApp, Reuniones.Compartir)
		reuniones.PUT("/:ID/finalizar", Middlewares.PermisoApp, Reuniones.Finalizar)
		reuniones.PUT("/:ID/clasificar", Middlewares.PermisoApp, Reuniones.Clasificar)
		reuniones.PUT("/:ID/iniciar", Middlewares.PermisoApp, Reuniones.IniciarReunion)
		reuniones.DELETE("/:ID/files/:FILE", Middlewares.PermisoApp, Reuniones.RemoveFile)
		reuniones.DELETE("/:ID", Middlewares.PermisoApp, Reuniones.RemoveReunion)
		reuniones.GET("/:ID", Reuniones.GetReunion)
		reuniones.POST("/:ID/minuta", Middlewares.PermisoApp, Reuniones.CreateMinuta)
		reuniones.PUT("/:ID/minuta", Middlewares.PermisoApp, Reuniones.UpdateAllMinuta)
		reuniones.PUT("/:ID/paselista", Middlewares.PermisoApp, Reuniones.PaseLista)
		reuniones.PUT("/:ID/minuta/:Minuta", Middlewares.PermisoApp, Reuniones.UpdateMinuta)
		reuniones.DELETE("/:ID/minuta/:Minuta", Middlewares.PermisoApp, Reuniones.RemoveMinuta)
		reuniones.POST("/:ID/files", Reuniones.UploadFiles)
		reuniones.POST("/:ID/minuta/:Minuta/notas", Middlewares.PermisoApp, Reuniones.AddNota)
	}
}
