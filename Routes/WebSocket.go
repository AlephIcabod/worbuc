package Routes

import (
	"gopkg.in/olahol/melody.v1"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"github.com/Shaked/gomobiledetect"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"bitbucket.org/AlephIcabod/worbuc/controlers/User"
	"github.com/gin-gonic/gin"
)

func Sockets(app *gin.Engine) {
	m := Functions.GetMelody()
	app.GET("/ws", User.Socket)

	m.HandleMessage(func(session *melody.Session, bytes []byte) {
		reunion_emi, _ := session.Get("reunion")
		emisor, _ := session.Get("payload")
		id := emisor.(Functions.UsuarioLogeado).ID
		Functions.SendMesage(Constants.DESCRIPCION, string(bytes), id, reunion_emi.(string))
	})

	m.HandleConnect(func(session *melody.Session) {
		reunion := session.Request.Header.Get("reunion")
		payload, err := Functions.DecodeToken(session.Request.Header.Get("token"))
		if err != nil {
			session.CloseWithMsg([]byte("{\"Mensaje\":\"Token no válido\"}"))
			return
		}
		session.Set("payload", *payload)
		if reunion != "" {
			session.Set("reunion", reunion)
		} else {
			detect := mobiledetect.NewMobileDetect(session.Request, nil)
			tipo := "desktop"
			if detect.IsMobile() {
				tipo = "mobile"
			} else if detect.IsTablet() {
				tipo = "tablet"
			}
			if !payload.IsAdmin {
				Functions.Sesiones[tipo]++
			}
			session.Set("tipo", tipo)
			Functions.NotifyAdminConnects()
			go Querys.IniciarSession(payload.ID, tipo, session.Request.UserAgent())
		}

		//	session.Set("tipo", session.Request.UserAgent())
	})
	m.HandleDisconnect(func(session *melody.Session) {
		emisor, _ := session.Get("payload")
		id := emisor.(Functions.UsuarioLogeado)
		_, e := session.Get("reunion")
		if !e {
			go Querys.TerminarSession(id.ID, session.Request.UserAgent())
			tipo, _ := session.Get("tipo")
			if !id.IsAdmin {
				Functions.Sesiones[tipo.(string)]--
			}
			Functions.NotifyAdminConnects()
		}
	})
}
