package Routes

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Proyects"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Middlewares"
)

func RouteProyects(app *gin.Engine) {
	projects := app.Group("/proyects", Middlewares.Auth, Middlewares.Vigente)
	{
		projects.GET("/:ID", Proyects.GetOne)
		projects.GET("/:ID/participantes", Proyects.GetParticipantes)
		projects.DELETE("/:ID", Middlewares.PermisoApp, Proyects.Delete)
		projects.POST("", Middlewares.PermisoApp, Proyects.CreateProject)
		projects.PUT("/:ID", Middlewares.PermisoApp, Proyects.UpdateProject)
		projects.PUT("/:ID/estatus", Middlewares.PermisoApp, Proyects.ChangeEstatusProject)
		projects.PUT("/:ID/favorito", Middlewares.PermisoApp, Proyects.ToggleFavorito)
		projects.PUT("/:ID/clasificar", Middlewares.PermisoApp, Proyects.Clasificar)
		projects.POST("/:ID/participantes", Middlewares.PermisoApp, Proyects.AddParticipante)
		projects.DELETE("/:ID/participantes/:Participante", Middlewares.PermisoApp, Proyects.RemoveParticipante)
		projects.POST("/:ID/actividades", Middlewares.PermisoApp, Proyects.CreateActividad)
		projects.GET("/:ID/etiquetas", Proyects.GetEtiquetas)
	}

	actividades := app.Group("/actividades", Middlewares.Auth, Middlewares.Vigente)
	{
		actividades.GET("/:ID", Proyects.GetActividad)
		actividades.PUT("/:ID", Middlewares.PermisoApp, Proyects.UpdateActividad)
		actividades.DELETE("/:ID", Middlewares.PermisoApp, Proyects.DeleteActividad)
		actividades.POST("/:ID/tareas", Middlewares.PermisoApp, Proyects.CreateTarea)
	}
}
