package Routes

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Middlewares"
	"bitbucket.org/AlephIcabod/worbuc/controlers/User"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Notebooks"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Proyects"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Reuniones"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Auth"
	"bitbucket.org/AlephIcabod/worbuc/controlers/OpenPay"
)

func RouteUser(app *gin.Engine) {

	app.POST("/signup", User.SignUp)
	app.POST("/login", Auth.Login)
	app.POST("/recover", Auth.Recover)
	app.POST("/validate", Auth.VerifyCatch)
	app.GET("/refreshToken", Auth.RefreshToken)
	app.POST("/recover/:token", Auth.ChangePassword)
	app.GET("/recover/:token", Auth.RenderRecover)
	app.GET("/user/:ID/plan", Middlewares.Auth, User.GetPlan)
	app.GET("/user/:ID", Middlewares.Auth, User.GetOne)
	app.POST("user/:ID/cambioPlan", Middlewares.Auth, OpenPay.ProcesarPago)
	user := app.Group("/user", Middlewares.Auth, Middlewares.Vigente)
	{
		user.GET("", User.Search)
		user.DELETE("/:ID/plan", User.CancelarSuscripcion)
		user.GET("/:ID/notifications", User.GetNotifications)
		user.GET("/:ID/files", User.GetFiles)
		user.PUT("/:ID/photo", User.ChangePhoto)
		user.DELETE("/:ID/photo", User.DeletePhoto)
		user.PUT("/:ID/basics", User.UpdateBasics)
		user.PUT("/:ID/address", User.UpdateAddress)
		user.PUT("/:ID/school", User.UpdateSchool)
		user.PUT("/:ID/job", User.UpdateJob)
		user.PUT("/:ID/settings", User.UpdateSettings)
		user.PUT("/:ID/password", User.UpdatePassword)
		user.POST("/:ID/phones", User.SavePhone)
		user.POST("/:ID/contacts", User.AddContact)
		user.GET("/:ID/contacts", User.GetContacts)
		user.GET("/:ID/notebooks", Notebooks.GetAll)
		user.DELETE("/:ID/contacts/:contact", User.RemoveContact)
		user.PUT("/:ID/phones/:PHONE", User.SavePhone)
		user.DELETE("/:ID/phones/:PHONE", User.RemovePhone)

		user.GET("/:ID/proyects", Proyects.GetProyects)
		// user.POST("/:ID/proyects", Proyects.CreateProject)
		user.GET("/:ID/reuniones", Reuniones.GetReuniones)

	}

	comentarios := app.Group("/comentarios", Middlewares.Auth, Middlewares.Vigente)
	{
		comentarios.DELETE("/:ID", Proyects.DeleteComentario)
	}
	notificaciones := app.Group("/notificaciones", Middlewares.Auth, Middlewares.Vigente)
	{
		notificaciones.PUT("/:ID", User.CheckNotification)
	}
}
