package Routes

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Admin"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Middlewares"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Reportes"
)

func RouteAdmin(app *gin.Engine) {
	app.GET("/admin/planes", Admin.GetPlanes)
	admin := app.Group("/admin", Middlewares.Auth, Middlewares.Admin)
	{
		admin.GET("/email", Admin.GetEmailSettings)

		admin.POST("/planes", Admin.CreatePlan)
		admin.PUT("/planes/:ID", Admin.UpdatePlan)
		admin.DELETE("/planes/:ID", Admin.DeletePlan)
		admin.PUT("/email", Admin.UpdateEmailSettings)
		admin.PUT("/prueba", Admin.ChangePrueba)
	}

	reportes := app.Group("/reportes")
	{
		reportes.GET("/usuarios", Reportes.ReporteUsuarios)
		reportes.GET("/generales", Reportes.ReportesGenerales)
	}
}
