package Constants

const (
	PERFIL_PUBLICO  = "Publico"
	PERFIIL_PRIVADO = "Privado"
	DEFAULT_IMAGE   = "/public/defaults/avatar.png"
	
	REGISTRADO = "Registrado"
	PREREGISTRADO="No verificado"
	ACTIVO     = "Activo"
	BLOQUEADO  = "Bloqueado"
	INACTIVO   = "Inactivo"
	ARCHIVADO  = "Archivado"
	
	MASCULINO      = "M"
	FEMENINO       = "F"
	NOESPECIFICADO = "N"
)
