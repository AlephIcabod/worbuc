package Constants

const PENDIENTE_FINALIZAR = "Sin agendar"
const AGENDADA = "Agendada"
const FINALIZADA = "Finalizada"
const EN_PROCESO = "En proceso"
const ELIMINADO = "Eliminado"
