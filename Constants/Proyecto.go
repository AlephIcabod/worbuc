package Constants

const USUARIO_ACTIVO = "Activo"
const USUARIO_INACTIVO = "Inactivo"

const ACTIVIDAD_EN_PROCESO = "En proceso"
const ACTIVIDAD_CANCELADA = "Cancelada"
const ACTIVIDAD_FINALIZADA = "Finalizada"

const ROL_SUPERVISOR = "Supervisor"
const ROL_REPONSABLE = "Responsable"

const TAREA_PENDIENTE = "Pendiente"
const TAREA_CANCELADA = "Cancelada"
const TAREA_EN_PROCESO = "En proceso"
const TAREA_EN_ESPERA = "En espera"
const TAREA_FINALIZADA = "Finalizada"

const PRIORIDAD_ALTA = "Alta"
const PRIORIDAD_MEDIA = "Media"
const PRIORIDAD_BAJA = "Baja"
const PRIORIDAD_URGENTE = "Urgente"

const PROYECTO_PAUSADO = "Pausado"
const PROYECTO_FINALIZADO = "Finalizado"
const PROYECTO_ACTIVO = "Activo"
const PROYECTO_ELIMINADO = "Eliminado"
