package main

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-contrib/cors"
	"time"
	"bitbucket.org/AlephIcabod/worbuc/models"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Reuniones"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"github.com/gin-contrib/static"
	"fmt"
	"github.com/go-pg/pg"
	"bitbucket.org/AlephIcabod/worbuc/config"
	"bitbucket.org/AlephIcabod/worbuc/Routes"
	"bitbucket.org/AlephIcabod/worbuc/controlers/User"
	"os"
	"io"
)

var maincors = cors.Config{
	AllowAllOrigins: true,
	AllowMethods:    []string{"GET", "POST", "PUT", "OPTIONS", "DELETE"},
	AllowHeaders: []string{"Authorization", "Accept", "Origin", "Content-Length", "Content-Type",
		"Cache-Control", "X-Requested-With"},
	ExposeHeaders:    []string{"Content-Length"},
	AllowCredentials: true,
	MaxAge:           12 * time.Hour,
}

func main() {

	config.InitConfig()
	db := models.GetDataBase()
	db.OnQueryProcessed(func(event *pg.QueryProcessedEvent) {
		_, err := event.FormattedQuery()
		if err != nil {
			panic(err)
		}
		// log.Println(q)
	})
	defer db.Close()
	// gin.SetMode(gin.ReleaseMode)
	Reuniones.EnvioRecordatorio()
	go Functions.DoTask(1, 0, Reuniones.EnvioRecordatorio)

	go Functions.DoTaskAt(1, "08:00", User.RenovarSuscripciones)
	/*secureFunc := func() gin.HandlerFunc {
		fmt.Println("https redirect")
		return func(c *gin.Context) {
			secureMiddleware := secure.New(secure.Options{
				SSLRedirect:  false,
				SSLHost:      "http://192.168.1.69",
			})
			err := secureMiddleware.Process(c.Writer, c.Request)

			// If there was an error, do not continue.
			if err != nil {
				return
			}
			c.Next()
		}
	}()
*/
	f, _ := os.Create("worbuc.log")
	gin.DefaultWriter = io.MultiWriter(f)
	app := gin.Default()
	app.Use(cors.New(maincors))
	// app.Use(secureFunc)

	// app.Use(gin.Logger())
	app.Use(gin.Recovery())
	app.LoadHTMLGlob("pages/*")
	app.Use(static.Serve("/public", static.LocalFile("public", true)))
	app.Use(static.Serve("/", static.LocalFile("app", true)))
	Routes.RouteAdmin(app)
	Routes.RouteNotebooks(app)
	Routes.RouteProyects(app)
	Routes.RouteReuniones(app)
	Routes.RouteTareas(app)
	Routes.RouteUser(app)
	Routes.RouteUtils(app)
	Routes.Sockets(app)

	// app.RunTLS(":443",)
	var err error
	if config.GetConfig().TlsSettings.Cert != "" {
		tls := config.GetConfig().TlsSettings
		fmt.Println("Running tls server")
		// go app.Run(":80")
		err = app.RunTLS(":443", tls.Cert, tls.Key)
	} else {
		fmt.Println("Running standard server")
		// 	err = app.Run(":80")
	}
	err = app.Run(":1080")
	if err != nil {
		panic("Error running app : " + err.Error())
	}
}
