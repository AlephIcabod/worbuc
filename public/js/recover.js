const pass1 = document.getElementById("pass"),
    pass2 = document.getElementById("pass2"),
    form = document.getElementsByTagName("form")[0],
    error = document.getElementById("messageError");

form.addEventListener('submit', function (e) {
    e.preventDefault();
    const userId = form.id;
    if (pass1.value !== pass2.value) {
        error.innerText = "Las contraseñas deben ser iguales"
        error.style.display = 'block;'
    } else {
        fetch(`/recover/${userId}`, {
            method: "POST",
            body: JSON.stringify({pass1: pass1.value, pass2: pass2.value})
        }).then(d => d.json())
            .then(d => {
                window.location.href = "/";
            }).catch(e => console.log(e)
        )
    }
});
