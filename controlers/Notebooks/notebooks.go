package Notebooks

import (
	"github.com/gin-gonic/gin"
	"strconv"
	"errors"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
)

func Create(ctx *gin.Context) {
	var notebook Schemas.Libreta
	err := ctx.BindJSON(&notebook)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}
	userId, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No hay usuario logueado"), err)
		return
	}
	if userId != notebook.UsuarioID {
		Functions.ResponseError(ctx, errors.New("No puede crear libretas para otro usuario"))
		return
	}
	notebook.Estatus = Constants.ACTIVO
	err = Querys.CreateNotebook(&notebook)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(201, gin.H{"notebook": notebook}, ctx)
}

func Update(ctx *gin.Context) {
	id := ctx.Param("ID")
	var notebook Schemas.Libreta
	err := ctx.BindJSON(&notebook)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}
	nid, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("Id de libreta invalido"), err)
		return
	}
	userId, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	prev, err := Querys.GetNotebook(nid)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if prev.UsuarioID != userId {
		Functions.ResponseError(ctx, errors.New("No tiene permiso para modificar esa libreta"))
		return
	}
	err = Querys.UpdateNotebook(nid, &notebook)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(201, gin.H{"notebook": notebook}, ctx)
}

func Delete(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("ID"), 10, 64)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("Id inválido"), err)
		return
	}
	userId, _ := Functions.GetUserId(ctx.Request)
	err = Querys.DeleteNotebook(id, userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(204, nil, ctx)
}

func GetAll(ctx *gin.Context) {
	userId := ctx.Param("ID")
	if userId == "" {
		Functions.ResponseError(ctx, errors.New("Id invalido"))
		return
	}

	notebooks, err := Querys.GetNotebooks(userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}

	Functions.ResponseSuccess(200, gin.H{"notebooks": notebooks}, ctx)
}

func UpdateDefault(ctx *gin.Context) {
	id := ctx.Param("ID")
	nid, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("Id de libreta invalido"), err)
		return
	}
	userId, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	prev, err := Querys.GetNotebook(nid)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if prev.UsuarioID != userId {
		Functions.ResponseError(ctx, errors.New("No tiene permiso para modificar esa libreta"))
		return
	}
	err = Querys.UpdateDefaultNotebook(nid, userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(204, nil, ctx)
}
