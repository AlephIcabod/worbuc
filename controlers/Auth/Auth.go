package Auth

import (
	"github.com/gin-gonic/gin"

	"encoding/json"
	"net/url"
	"errors"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"net/http"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"bitbucket.org/AlephIcabod/worbuc/config"
	"time"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"github.com/go-pg/pg"
	"fmt"
)

func Login(ctx *gin.Context) {
	var request RequestLogin
	err := ctx.BindJSON(&request)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("Error al leer peticion"))
		return
	}
	user, err := Querys.Login(request.Email, request.Password)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}

	plan, _, err := Querys.GetUserPlan(user.Id)

	if err != nil {
		if err == pg.ErrNoRows {

			err = Querys.ChangePlan(user.Id, nil, config.GetConfig().PlanPrueba, config.GetConfig().DiasPrueba)
			if err != nil {
				Functions.ResponseError(ctx, err)
				return
			}
		} else {
			Functions.ResponseError(ctx, err)
			return
		}
	}
	plan, _, err = Querys.GetUserPlan(user.Id)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if user.IsAdmin {
		vigencia := time.Now().UTC().Add(time.Hour * 24)
		plan = &Schemas.PlanUsuario{Vigencia: &vigencia}
	}

	token, err := Functions.CreateTokenString(user.Id, user.Nombre+" "+user.ApellidoPaterno+" "+user.ApellidoMaterno, user.Email, user.TipoPerfil, user.Estatus, *plan.Vigencia, user.IsAdmin)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(201, gin.H{"token": token}, ctx)
	return
	// Usuario.Login(request.Email, request.Password)

}

func Recover(ctx *gin.Context) {
	var req RequestLogin
	err := ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"))
		return
	}
	if req.Email == "" {
		Functions.ResponseError(ctx, errors.New("Ingrese un email válido"))
		return
	}
	user, err := Querys.GetUserByEmail(req.Email)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	token, err := Functions.GenerarTokenUnUso(user.Id)
	link := config.GetServerAddress() + "/" + "recover/" + token

	body := fmt.Sprintf(`%s 
	        <div style="margin:0px auto;max-width:600px;">
            <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
                <tbody>
                    <tr>
                        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                            <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
                            <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                                <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                                    <tbody>
                                        <tr>
                                            <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
                                                <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                                    <p><span style="font-size:16px;">
                                                        Ha solicitado cambiar su contraseña, por favor acceda al siguiente enlace
                                                        para restaurar su contraseña.
                                                        </span>
                                                        </p> 
                                                        <p><span>Si usted no ha solicitado esto haga caso omiso de este correo.
                                                    </span></p>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
        <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">        <tr>          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]-->
        <div style="margin:0px auto;max-width:600px;">
            <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
                <tbody>
                    <tr>
                        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;">
                            <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:300px;">      <![endif]-->
                            <div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                                <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                                    <tbody>
                                        <tr>
                                            <td style="word-wrap:break-word;font-size:0px;padding:10px 0px 10px 0px;padding-top:10px;padding-left:25px;"
                                                align="center">
                                                <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:separate;"
                                                    align="center" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;"
                                                                align="center" valign="middle" bgcolor="#C034E8"><a href="%s" style="text-decoration:none;background:#C034E8;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%%;text-transform:none;margin:0px;"
                                                                    target="_blank">Cambiar mi contraseña</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]>      </td><td style="vertical-align:top;width:300px;">      <![endif]-->
                            
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>%s`, Constants.CABECERA_CORREO, link, Constants.PIE_CORREO)
	m, err := config.CargarDatosCorreo("Cambio de contraseña",
		body,
		user.Email, true)
	go config.EnviarCorreo(m)
	Functions.ResponseSuccess(200, nil, ctx)
}

func RenderRecover(ctx *gin.Context) {
	token := ctx.Param("token")
	id, valido := Functions.ValidaTokenUrl(token)
	user, err := Querys.GetUser(id)

	if err != nil || !valido {
		ctx.HTML(http.StatusBadRequest, "error.tmpl", gin.H{
			"title":       "Error de solicitud",
			"description": "El enlace para este procedimiento ha expirado o no existe el usuario indicado",
		})
		return
	}
	ctx.HTML(http.StatusOK, "recover.tmpl", gin.H{
		"title":    "Recuperacion de contraseña",
		"username": user.Nombre,
		"userId":   token,
	})
}

func ChangePassword(ctx *gin.Context) {
	token := ctx.Param("token")
	var req RequestRecover
	err := ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	id, valido := Functions.ValidaTokenUrl(token)
	if !valido {
		Functions.ResponseError(ctx, errors.New("Token ha caducado, solciite uno nuevo"))
		return
	}
	_, err = Querys.GetUser(id)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err = Querys.ChangePassword(id, req.Password)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, nil, ctx)
}

func RefreshToken(ctx *gin.Context) {
	token := ctx.Query("token")
	if token != "" {
		nuevoToken, err := Functions.RefreshToken(token)
		if err != nil {
			Functions.ResponseError(ctx, errors.New("No se pudo refrescar token"), err)
			return
		}
		Functions.ResponseSuccess(200, gin.H{"token": nuevoToken}, ctx)
		return
	} else {
		Functions.ResponseError(ctx, errors.New("No ha especificado un token válido"))
		return
	}
}

func VerifyCatch(ctx *gin.Context) {
	var req RequestCaptcha
	err := ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"))
		return
	}
	request := RequestToCaptcha{
		Secret:   Constants.API_SECRET,
		Response: req.Captcha,
	}

	data := url.Values{}
	data.Add("secret", request.Secret)
	data.Add("response", request.Response)
	resp, err := Functions.PostForm(Constants.URL_CAPTCHA, data)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"))
		return
	}
	var Respuesta ResponseCaptcha
	err = json.Unmarshal(resp, &Respuesta)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"))
		return
	}
	if Respuesta.Success {
		Functions.ResponseSuccess(200, gin.H{"ok": true}, ctx)
		return
	} else {
		Functions.ResponseSuccess(200, gin.H{"ok": false}, ctx)
		return
	}
}
