package Auth

import "time"

type RequestLogin struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
type RequestRecover struct {
	Password  string `json:"pass1"`
	Password2 string `json:"pass2"`
}

type RequestCaptcha struct{
	Captcha string `json:"captcha"`
}

type ResponseCaptcha struct{
	Success bool `json:"success"`
	Tiempo time.Time `json:"challenge_ts"`
	Hostname string `json:"hostname"`
}

type RequestToCaptcha struct {
	Response string `json:"response"`
	Secret string `json:"secret"`
}
