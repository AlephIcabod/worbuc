package Reuniones

import "time"

type ReqSave struct {
	Reunion Reunion `json:"reunion"`
	Coords  Coords  `json:"coords"`
	FechaS  string  `json:"fecha_s"`
}

type Reunion struct {
	Id          string            `json:"Id"`
	Fecha       time.Time         `json:"Fecha"`
	Titulo      string            `json:"Titulo"`
	Objetivo    string            `json:"Objetivo"`
	Lugar       string            `json:"Lugar"`
	Duracion    int64             `json:"Duracion"`
	Estatus     string            `json:"Estatus"`
	Organizador string            `json:"Organizador"`
	Invitados   []ReunionInvitado `json:"Invitados"`
	Minuta      []Minuta          `json:"Minuta"`
	Etiquetas   []string          `json:"etiquetas"`
	LibretaID   int64             `json:"LibretaID"`
	Archivos    []Archivo         `json:"Archivos"`
}

type ReunionInvitado struct {
	UsuarioID       string `json:"UsuarioID"`
	Rol             string `json:"Rol"`
	Alias           string `json:"Alias"`
	Nombre          string `json:"Nombre"`
	ApellidoPaterno string `json:"ApellidoPaterno"`
	ApellidoMaterno string `json:"ApellidoMaterno"`
	Correo          string `json:"Correo"`
	Administrador   bool   `json:"Administrador"`
	Asistencia      bool   `json:"Asistencia"`
	Confirmacion    bool   `json:"Confirmacion"`
}

type Minuta struct {
	Id            int    `json:"Id"`
	Tema          string `json:"Tema"`
	Descripcion   string `json:"Descripcion"`
	Estatus       string `json:"Estatus"`
	ResponsableID string `json:"ResponsableID"`
	NumeroOrden   int    `json:"NumeroOrden"`
}

type Coords struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

type Archivo struct {
	ID    string `json:"ID"`
	Url   string `json:"Url"`
	Size  int64  `json:"Size"`
	Name  string `json:"Name"`
	Owner string `json:"Owner"`
}

type ReqClasificar struct {
	Id        string   `json:"id"`
	Libreta   int64    `json:"libreta"`
	Etiquetas []string `json:"etiquetas"`
}

type ReqMinuta struct {
	Id            int64  `json:"Id"`
	Tema          string `json:"Tema"`
	Descripcion   string `json:"Descripcion"`
	Estatus       string `json:"Estatus"`
	ResponsableID string `json:"ResponsableID"`;
	NumeroOrden   int    `json:"NumeroOrden"`
}

type ReqMinutas struct {
	Minuta []ReqMinuta `json:"minuta"`
}

type ReqPaseLista struct {
	Invitados []invitado `json:"invitados"`
}
type invitado struct {
	Alias   string `json:"alias"`
	Asistio bool   `json:"asistencia"`
}
