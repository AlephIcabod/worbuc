package Reuniones

import (
	"fmt"
	`github.com/rs/xid`
	"github.com/tkuchiki/go-timezone"
	"strings"
	"log"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"strconv"
	"time"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/config"
)

func crearReunionPendiente(req ReqSave, user string, agendar bool) (*Schemas.Reunion, error) {
	estatus := Constants.PENDIENTE_FINALIZAR
	if agendar || req.Reunion.Estatus == Constants.AGENDADA {
		estatus = Constants.AGENDADA
	}
	id := xid.New().String()
	reunionDB := Schemas.Reunion{
		Id:            id,
		Estatus:       estatus,
		Fecha:         req.Reunion.Fecha.UTC(),
		HoraInicio:    req.Reunion.Fecha.UTC(),
		Duracion:      req.Reunion.Duracion,
		HoraTermino:   req.Reunion.Fecha.Add(time.Minute * time.Duration(req.Reunion.Duracion)).UTC(),
		Objetivo:      req.Reunion.Objetivo,
		Titulo:        req.Reunion.Titulo,
		OrganizadorID: req.Reunion.Organizador,
		Archivos:      parseListArchivos(req, id),
	}
	if req.Coords.Lat != 0 || req.Coords.Lng != 0 {
		reunionDB.Lugar = req.Reunion.Lugar + "COORDS=" + strconv.FormatFloat(req.Coords.Lat, 'f', 4, 64) + ";" + strconv.FormatFloat(req.Coords.Lng, 'f', 4, 64)
	} else {
		reunionDB.Lugar = req.Reunion.Lugar
	}
	err := Querys.CreateReunion(&reunionDB, user)
	if err != nil {
		return nil, err
	}

	err = resolverUsuarios(&req.Reunion.Invitados)
	if err != nil {
		Querys.RemoveReunion(reunionDB.Id)
		return nil, err
	}
	listaInvitados := crearListaInvitados(req.Reunion.Invitados, reunionDB.Id, user, req.Reunion.LibretaID,
		req.Reunion.Etiquetas)

	minutaReunion := crearMinuta(req.Reunion.Minuta, req.Reunion.Invitados, reunionDB.Id)
	err = Querys.ActualizarInvitadosYMinuta(listaInvitados, minutaReunion, reunionDB.Id, user)
	if err != nil {
		Querys.RemoveReunion(reunionDB.Id)
		return nil, err
	}

	if agendar {
		enviarRecordatorio(1, req.Reunion, listaInvitados, reunionDB.Id)
	}
	return &reunionDB, nil
}

func crearMinuta(minuta []Minuta, invitados []ReunionInvitado, reunion string) ([]Schemas.Minuta) {
	res := make([]Schemas.Minuta, 0)
	if minuta == nil {
		return res
	}

	for _, v := range minuta {
		for _, w := range invitados {
			if w.Alias == v.ResponsableID {
				v.ResponsableID = w.UsuarioID
				break
			}
		}
		res = append(res, Schemas.Minuta{
			Id:            int64(v.Id),
			Estatus:       Constants.ACTIVO,
			CreatedAt:     time.Now(),
			Descripcion:   v.Descripcion,
			NumeroOrden:   v.NumeroOrden,
			ResponsableID: v.ResponsableID,
			Tema:          v.Tema,
			ReunionID:     reunion})
	}
	return res
}

func crearListaInvitados(invitados []ReunionInvitado, reunion, usuario string, libreta int64,
	tags []string) ([]Schemas.Participante) {
	res := make([]Schemas.Participante, 0)
	if invitados == nil {
		return res
	}

	for _, v := range invitados {
		aux := Schemas.Participante{ReunionID: reunion,
			UsuarioID:     v.UsuarioID,
			Alias:         v.Alias,
			Confirmacion:  v.Confirmacion,
			Administrador: v.Administrador,
			Rol:           v.Rol,
			Asistencia:    false,
		}
		if v.UsuarioID == usuario {
			aux.LibretaID = libreta
			aux.Etiquetas = tags
		}
		res = append(res, aux)

	}

	return res
}

func resolverUsuarios(invitados *[]ReunionInvitado) (error) {
	password, _ := Functions.Encrypt("12345")
	for i := range *invitados {
		existente, err := Querys.GetUserByEmail((*invitados)[i].Correo)
		if err != nil && err.Error() != Constants.ErrorNoResultsUser {
			return err
		}
		if existente == nil {
			nuevo := Schemas.Usuario{
				Email:           (*invitados)[i].Correo,
				Estatus:         Constants.PREREGISTRADO,
				ApellidoPaterno: (*invitados)[i].ApellidoPaterno,
				ApellidoMaterno: (*invitados)[i].ApellidoMaterno,
				Nombre:          (*invitados)[i].Nombre,
				TipoPerfil:      Constants.PERFIL_PUBLICO,
				Genero:          Constants.NOESPECIFICADO,
				Password:        password,
			}

			err := Querys.Create(&nuevo)
			if err != nil {

				return err
			}
			(*invitados)[i].UsuarioID = nuevo.Id
		} else {
			(*invitados)[i].UsuarioID = existente.Id
		}
	}

	return nil
}

func actualizarReunionPendiente(req ReqSave, user, id string, agendar, actualizarTodo bool) (*Schemas.Reunion, error) {
	reunion, err := Querys.FindReunion(id)
	if err != nil {
		return nil, err
	}
	estatus := Constants.PENDIENTE_FINALIZAR
	if agendar || req.Reunion.Estatus == Constants.AGENDADA {
		estatus = Constants.AGENDADA
	}
	reunionDB := Schemas.Reunion{
		Id:            reunion.Id,
		Estatus:       estatus,
		Fecha:         req.Reunion.Fecha.UTC(),
		HoraInicio:    req.Reunion.Fecha.UTC(),
		Duracion:      req.Reunion.Duracion,
		HoraTermino:   req.Reunion.Fecha.Add(time.Minute * time.Duration(req.Reunion.Duracion)).UTC(),
		Objetivo:      req.Reunion.Objetivo,
		Titulo:        req.Reunion.Titulo,
		OrganizadorID: req.Reunion.Organizador,
		Archivos:      parseListArchivos(req, reunion.Id),
	}
	if req.Coords.Lat != 0 || req.Coords.Lng != 0 {
		reunionDB.Lugar = req.Reunion.Lugar + "COORDS=" + strconv.FormatFloat(req.Coords.Lat, 'f', 4, 64) + ";" + strconv.FormatFloat(req.Coords.Lng, 'f', 4, 64)
	} else {
		reunionDB.Lugar = req.Reunion.Lugar
	}

	err = Querys.UpdateReunion(&reunionDB, user)
	if err != nil {
		return nil, err
	}

	err = resolverUsuarios(&req.Reunion.Invitados)
	if err != nil {
		fmt.Println("error creando invitados")
	}
	listaInvitados := crearListaInvitados(req.Reunion.Invitados, reunionDB.Id, user, req.Reunion.LibretaID,
		req.Reunion.Etiquetas)

	if actualizarTodo {
		minutaReunion := crearMinuta(req.Reunion.Minuta, req.Reunion.Invitados, reunionDB.Id)
		err = Querys.ActualizarInvitadosYMinuta(listaInvitados, minutaReunion, reunionDB.Id, user)
	}
	if err != nil {
		return nil, err
	}
	if agendar {
		enviarRecordatorio(1, req.Reunion, listaInvitados, reunionDB.Id)
		Functions.SendMesage(Constants.PAUSA_REUNION, reunion.Id, "", reunion.Id)
	}
	nuevo, err := Querys.GetReunion(id, user)

	return nuevo, nil
}

func parseListArchivos(req ReqSave, reunion string) ([]Schemas.ArchivoReunion) {
	res := make([]Schemas.ArchivoReunion, 0, 0)
	for _, v := range req.Reunion.Archivos {
		aux := Schemas.ArchivoReunion{Size: float64(v.Size),
			ReunionID: reunion,
			ID:        v.ID,
			Url:       v.Url,
			UsuarioID: v.Owner,
			Name:      v.Name}
		res = append(res, aux)
	}
	return res
}

func enviarInvitacion(organizador, titulo, direccion, reunion string,
	fecha time.Time,
	invitados []map[string]string) {
	serverAddress := config.GetServerAddress() + "/"
	direccion = strings.Split(direccion, "COORDS=")[0]
	for _, v := range invitados {
		var est time.Time
		est, err := timezone.FixedTimezone(fecha, v["timezone"])
		if err != nil {
			est = fecha
		}
		body := fmt.Sprintf(`
%s <div style="margin:0px auto;max-width:600px;">
    <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
        <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
                                        <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                            <p><span style="font-size:16px;">%s te ha invitado a la reunion %s que se llevara
                                                    a cabo el %s en %s.
                                                </span></p>
                                            <p><span style="font-size:16px;">Por favor confirma tu asistencia
                                                </span></p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
</div>
<!--[if mso | IE]>      </td></tr></table>      <![endif]-->
<!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">        <tr>          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]-->
<div style="margin:0px auto;max-width:600px;">
    <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
        <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;">
                    <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:300px;">      <![endif]-->
                    <div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:10px 0px 10px 0px;padding-top:10px;padding-left:25px;" align="center">
                                        <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:separate;" align="center" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" align="center" valign="middle" bgcolor="#C034E8"><a
                                                            href="%s" style="text-decoration:none;background:#C034E8;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%%;text-transform:none;margin:0px;"
                                                            target="_blank">Asistiré</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td><td style="vertical-align:top;width:300px;">      <![endif]-->
                    <div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:10px 0px 10px 0px;padding-top:10px;padding-left:25px;" align="center">
                                        <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:separate;" align="center" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" align="center" valign="middle" bgcolor="#e85034"><a
                                                            href="%s" style="text-decoration:none;background:#e85034;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%%;text-transform:none;margin:0px;"
                                                            target="_blank">No asistiré</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
</div>
<!--[if mso | IE]>      </td></tr></table>      <![endif]-->
<!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">        <tr>          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]--> %s`,
			Constants.CABECERA_CORREO,
			organizador,
			titulo,
			est.Format("02-01-2006 a las 15:04"),
			direccion,
			serverAddress+"reuniones/"+reunion+"/confirmar?asistencia=true&usuario="+v["id"],
			serverAddress+"reuniones/"+reunion+"/confirmar?asistencia=false&usuario="+v["id"],
			Constants.PIE_CORREO)

		m, err := config.CargarDatosCorreo("Invitacion a reunion", body, v["correo"], true)
		go config.EnviarCorreo(m)
	}
}

func enviarRecordatorio(option int, reunion Reunion, listaInvitados []Schemas.Participante, reunionID string) {
	ids := make([]string, 0, 0)
	for _, v := range listaInvitados {
		ids = append(ids, v.UsuarioID)
	}
	invitados, _ := Querys.GetEmails(ids)
	organizador, _ := Querys.GetUser(reunion.Organizador)

	switch option {
	case 1:
		enviarInvitacion(organizador.Nombre+" "+organizador.ApellidoPaterno+" "+organizador.ApellidoMaterno,
			reunion.Titulo, reunion.Lugar, reunionID, reunion.Fecha.UTC(),
			invitados)
	}

}

func EnvioRecordatorio() {
	reuniones, err := Querys.ReunionesRecordatorio()
	if err != nil {
		log.Print("Error al enviar recordatorios", err)
	}
	for i := range reuniones {
		go sendRemember(reuniones[i])
	}
}

func sendRemember(reunion Schemas.Reunion) {
	serverAddress := config.GetServerAddress() + "/"
	direccion := strings.Split(reunion.Lugar, "COORDS=")[0]
	for _, v := range reunion.Participantes {
		var est time.Time
		est, err := timezone.FixedTimezone(reunion.Fecha, v.Usuario.ZonaHoraria)
		if err != nil {
			est = reunion.Fecha
		}
		body := fmt.Sprintf(`%s
<div style="margin:0px auto;max-width:600px;">
    <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
        <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
                                        <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                            <p><span style="font-size:16px;">Este es un recordatorio de la reunion %s  organizada por %s que se llevara a cabo el %s en %s.
                                                </span></p>
                                            <p><span style="font-size:16px;">Por favor confirma tu asistencia
                                                </span></p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
</div>
<!--[if mso | IE]>      </td></tr></table>      <![endif]-->
<!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">        <tr>          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]-->
<div style="margin:0px auto;max-width:600px;">
    <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
        <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;">
                    <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:300px;">      <![endif]-->
                    <div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:10px 0px 10px 0px;padding-top:10px;padding-left:25px;" align="center">
                                        <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:separate;" align="center" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" align="center" valign="middle" bgcolor="#C034E8"><a
                                                            href="%s" style="text-decoration:none;background:#C034E8;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%%;text-transform:none;margin:0px;"
                                                            target="_blank">Asistiré</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td><td style="vertical-align:top;width:300px;">      <![endif]-->
                    <div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:10px 0px 10px 0px;padding-top:10px;padding-left:25px;" align="center">
                                        <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:separate;" align="center" border="0">
                                            <tbody>
                                                <tr>
                                                    <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;" align="center" valign="middle" bgcolor="#e85034"><a
                                                            href="%s" style="text-decoration:none;background:#e85034;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%%;text-transform:none;margin:0px;"
                                                            target="_blank">No asistiré</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
</div>
<!--[if mso | IE]>      </td></tr></table>      <![endif]-->
<!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">        <tr>          <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">      <![endif]-->
%s`, Constants.CABECERA_CORREO,
			reunion.Titulo,
			reunion.Organizador.Nombre+" "+reunion.Organizador.ApellidoPaterno,
			est.Format("02-01-2006 a las 15:04"),
			direccion,
			serverAddress+"reuniones/"+reunion.Id+"/confirmar?asistencia=true&usuario="+v.UsuarioID,
			serverAddress+"reuniones/"+reunion.Id+"/confirmar?asistencia=false&usuario="+v.UsuarioID,
			Constants.PIE_CORREO)

		m, err := config.CargarDatosCorreo("Recordatorio de reunion", body, v.Usuario.Email, true)
		go config.EnviarCorreo(m)
	}
}

func notifyReunion(reunion *Schemas.Reunion, logeado *Functions.UsuarioLogeado, text string) {
	notificaciones := make([]*Schemas.Notification, 0)
	for _, v := range reunion.Participantes {
		if v.Usuario.Id != logeado.ID {
			notificaciones = append(notificaciones, &Schemas.
			Notification{Id: xid.New().String(),
				Fecha:      time.Now().UTC(),
				Adicional:  reunion.Id,
				EmisorID:   logeado.ID,
				Tipo:       "Reunion",
				ReceptorID: v.Usuario.Id,
				Titulo:     reunion.Titulo,
				Texto:      logeado.Nombre + " " + text + " " + reunion.Titulo,
			})
		}
	}

	Querys.CreateNotification(notificaciones...)
}
