package Reuniones

import (
	"github.com/gin-gonic/gin"
	`time`
	`strings`
	`os`
	`github.com/rs/xid`
	"errors"
	"fmt"

	"strconv"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"net/http"
	"bitbucket.org/AlephIcabod/worbuc/config"
)

func Save(ctx *gin.Context) {
	id := ctx.Param("ID")
	soloReunion, _ := strconv.ParseBool(ctx.DefaultQuery("all", "true"))
	notificar, _ := strconv.ParseBool(ctx.DefaultQuery("notify", "true"))
	var request ReqSave
	userID, _ := Functions.GetUserId(ctx.Request)
	err := ctx.BindJSON(&request)
	if err != nil {
		Functions.ResponseError(ctx,
			errors.New(`No se pudo leer el cuerpo de la peticion`),
			err)
		return
	}

	request.Reunion.Fecha, _ = time.Parse(time.RFC3339, request.FechaS)
	if id == "nuevo" {
		reunion, err := crearReunionPendiente(request, userID, false)
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
		reunion, _ = Querys.GetReunion(reunion.Id, userID)
		Functions.ResponseSuccess(201, gin.H{"reunion": reunion}, ctx)
		return
	} else {
		reunion, err := actualizarReunionPendiente(request, userID, id,
			request.Reunion.Estatus == Constants.AGENDADA && notificar, soloReunion)
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
		reunion, _ = Querys.GetReunion(reunion.Id, userID)
		Functions.ResponseSuccess(200, gin.H{"reunion": reunion}, ctx)
	}
}

func GetReuniones(ctx *gin.Context) {
	userID := ctx.Param("ID")
	limit, page := Functions.GetLimitPage(ctx)

	libreta, etiquetas, filter, err := Functions.GetFiltros(ctx)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}

	reuniones, max, err := Querys.GetReuniones(userID, limit, page, libreta, etiquetas, filter)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"reuniones": reuniones,
		"maxResults": max}, ctx)

}

func GetReunion(ctx *gin.Context) {
	id := ctx.Param("ID")
	user, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	reunion, err := Querys.GetReunion(id, user)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"reunion": reunion}, ctx)
}

func Disponibilidad(ctx *gin.Context) {
	mes, err := strconv.ParseInt(ctx.DefaultQuery("mes", "-1"), 10, 32)
	userID, _ := Functions.GetUserId(ctx.Request)
	if mes > 0 {
		anio, _ := strconv.ParseInt(ctx.DefaultQuery("anio", "-1"), 10, 32)
		if anio < 0 {
			anio = int64(time.Now().UTC().Year())
		}
		mes++
		reuniones, err := Querys.GetReunionesMes(userID, int32(mes), int32(anio))
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
		Functions.ResponseSuccess(200, gin.H{"reuniones": reuniones}, ctx)
		return
	}

	fechaS := ctx.Query("fecha")
	reunion := ctx.Query("reunion")
	fecha, err := time.Parse(time.RFC3339, fechaS)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if fecha.UTC().Before(time.Now().UTC()) {
		Functions.ResponseError(ctx, errors.New("No puede agendar una reunion con fecha anterior a la actual"))
		return
	}

	r, err := Querys.Disponibilidad(fecha.UTC(), userID, reunion)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if r > 0 {
		Functions.ResponseError(ctx, errors.New("Ya tiene una reunion agendada a esa hora"))
		return
	}
	Functions.ResponseSuccess(200, nil, ctx)
}

func RemoveReunion(ctx *gin.Context) {
	id := ctx.Param("ID")
	userID, _ := Functions.GetUserId(ctx.Request)
	err := Querys.CancelReunion(id, userID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, nil, ctx)
}

func UploadFiles(ctx *gin.Context) {
	form, _ := ctx.MultipartForm()
	userId, _ := Functions.GetUserId(ctx.Request)
	plan, size, err := Querys.GetUserPlan(userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if _, err := os.Stat("public/" + userId); os.IsNotExist(err) {
		err = os.Mkdir("public/"+userId, os.ModePerm)
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
	}
	tipo := ctx.DefaultQuery("tipo", "REUNION");
	reunion := ctx.Param("ID")
	files := form.File
	filePaths := make([]map[string]interface{}, 0, 0)
	archivosBD := make([]Schemas.ArchivoReunion, 0, 0)
	for i := range files {
		archivo := form.File[i]
		id := xid.New().String()
		pathname := "public/" + userId + "/" + id + "_" + archivo[0].Filename
		aux := map[string]interface{}{
			"path": pathname,
			"name": archivo[0].Filename,
			"size": archivo[0].Size,
			"id":   id,
		}
		filePaths = append(filePaths, aux)
		archivoBD := Schemas.ArchivoReunion{UsuarioID: userId,
			Name: archivo[0].Filename,
			ID: id,
			Size: float64(archivo[0].Size),
			Url: pathname,
		}
		if plan.Plan.LimiteMb>0&&(plan.Plan.LimiteMb - size) < (archivoBD.Size / 1024 / 1024) {
			Functions.ResponseError(ctx, errors.New("No tiene mas espacio de almacenamiento disponible, cambiese a un mejor plan"))
			return
		}

		ctx.SaveUploadedFile(archivo[0], pathname)
		if tipo == "REUNION" {
			archivoBD.ReunionID = reunion
		}
		if tipo == "PROYECTO" {
			archivoBD.ProyectoID = reunion
		}
		archivosBD = append(archivosBD,
			archivoBD)

	}
	err = Querys.SaveFileUpload(archivosBD, reunion == "reunionX")
	if err != nil {
		for _, v := range filePaths {
			os.Remove(v["path"].(string))
		}
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.Response(200, "probando", gin.H{"data": filePaths}, ctx)

}

func RemoveFile(ctx *gin.Context) {
	userID, _ := Functions.GetUserId(ctx.Request)
	file := ctx.Param("FILE")
	path, err := Querys.RemoveFileReunion(file, userID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err = os.Remove(path)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(204, nil, ctx)
}

func Agendar(ctx *gin.Context) {
	var request ReqSave
	userID, _ := Functions.GetUserId(ctx.Request)
	err := ctx.BindJSON(&request)
	if err != nil {
		Functions.ResponseError(ctx,
			errors.New(`No se pudo leer el cuerpo de la peticion`),
			err)
		return
	}
	request.FechaS = strings.Replace(request.FechaS, "%2B", "+", -1)
	request.Reunion.Fecha, err = time.Parse(time.RFC3339, request.FechaS)

	if request.Reunion.Id == "" {
		reunion, err := crearReunionPendiente(request, userID, true)
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
		reunion, err = Querys.GetReunion(reunion.Id, userID)
		Functions.ResponseSuccess(201, gin.H{"reunion": reunion}, ctx)
		return
	}
	reunion, err := actualizarReunionPendiente(request, userID, request.Reunion.Id, true, false)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	reunion, _ = Querys.GetReunion(request.Reunion.Id, userID)
	user, _ := Functions.GetUser(ctx.Request)
	notifyReunion(reunion, user, "te ha invitado a la reunion")
	Functions.ResponseSuccess(200, gin.H{"reunion": reunion}, ctx)
}

func Clasificar(ctx *gin.Context) {
	id := ctx.Param("ID")
	_, err := xid.FromString(id)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("El id de la reunión es inválido"))
		return
	}
	var request ReqClasificar
	userId, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("Usuario no válido"))
		return
	}
	err = ctx.BindJSON(&request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err = Querys.ClasificarReunion(userId, request.Id, request.Libreta, request.Etiquetas)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"mensaje": "ok"}, ctx)
}

func IniciarReunion(ctx *gin.Context) {
	id := ctx.Param("ID")
	userID, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err = Querys.IniciarReunion(id, userID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.SendMesage(Constants.INICIO, nil, userID, id)
	Functions.ResponseSuccess(200, nil, ctx)
}

func RemoveMinuta(ctx *gin.Context) {
	reunion := ctx.Param("ID")
	minuta, err := strconv.ParseInt(ctx.Param("Minuta"), 10, 64)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se indico una minuta válida"))
		return
	}
	userID, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err = Querys.RemoveMinuta(minuta, reunion, userID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.SendMesage(Constants.ELIMINA_MINUTA, minuta, userID, reunion)
	Functions.ResponseSuccess(204, nil, ctx)
}

func UpdateMinuta(ctx *gin.Context) {
	reunion := ctx.Param("ID")
	minuta, err := strconv.ParseInt(ctx.Param("Minuta"), 10, 64)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se indico una minuta válida"))
		return
	}
	userID, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	var req ReqMinuta
	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la petición"))
		return
	}
	permitido, err := Querys.VerificarPermisoByReunion(reunion, userID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if !permitido {
		Functions.ResponseError(ctx, errors.New("No tiene permiso para realizar esta accion"))
		return
	}
	minutaB := Schemas.Minuta{
		Descripcion:   req.Descripcion,
		NumeroOrden:   req.NumeroOrden,
		ResponsableID: req.ResponsableID,
		Id:            minuta,
		Estatus:       req.Estatus,
		Tema:          req.Tema,
		UpdatedAt:     time.Now()}
	if minutaB.Estatus == "" {
		minutaB.Estatus = Constants.ACTIVO
	}
	err = Querys.UpdateMinuta(&minutaB, reunion, userID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.SendMesage(Constants.CAMBIO_MINUTA, minutaB, userID, reunion)
	Functions.ResponseSuccess(200, gin.H{"minuta": minutaB}, ctx)
}

func CreateMinuta(ctx *gin.Context) {
	reunion := ctx.Param("ID")
	userID, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	var req ReqMinuta
	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la petición"))
		return
	}
	minutaB := Schemas.Minuta{Estatus: Constants.ACTIVO,
		Descripcion:   req.Descripcion,
		NumeroOrden:   req.NumeroOrden,
		ResponsableID: req.ResponsableID,
		Tema:          req.Tema}
	err = Querys.CreateMinuta(&minutaB, reunion, userID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.SendMesage(Constants.NUEVA_MINUTA, minutaB, userID, reunion)
	Functions.ResponseSuccess(200, gin.H{"minuta": minutaB}, ctx)
}

func UpdateAllMinuta(ctx *gin.Context) {
	reunion := ctx.Param("ID")
	userID, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	var req ReqMinutas
	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"))
		return
	}
	permitido, err := Querys.VerificarPermisoByReunion(reunion, userID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if !permitido {
		Functions.ResponseError(ctx, errors.New("No tiene permiso para realizar esta accion"))
		return
	}
	for _, v := range req.Minuta {
		minutas := &Schemas.Minuta{Id: v.Id,
			UpdatedAt:     time.Now(),
			NumeroOrden:   v.NumeroOrden,
			Estatus:       v.Estatus,
			Tema:          v.Tema,
			ResponsableID: v.ResponsableID,
			Descripcion:   v.Descripcion}
		err = Querys.UpdateMinuta(minutas, reunion, userID)

	}
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.SendMesage(Constants.REORDENAMIENTO, req, userID, reunion)
	Functions.ResponseSuccess(200, gin.H{"minuta": req}, ctx)

}

func PaseLista(ctx *gin.Context) {
	reunion := ctx.Param("ID")
	user, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	var req ReqPaseLista
	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"))
		return
	}
	permiso, err := Querys.VerificarPermisoByReunion(reunion, user)
	if err != nil || !permiso {
		Functions.ResponseError(ctx, err)
		return
	}
	invitados := make([]Schemas.Participante, 0, 0)
	for _, v := range req.Invitados {
		invitados = append(invitados, Schemas.Participante{ReunionID: reunion,
			Asistencia: v.Asistio,
			Alias:      v.Alias})
	}
	err = Querys.UpdatePaseLista(invitados, reunion)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.SendMesage(Constants.PASE_LISTA, req.Invitados, user, reunion)
	Functions.ResponseSuccess(200, gin.H{"invitados": req.Invitados}, ctx)
}

func AddNota(ctx *gin.Context) {
	reunion := ctx.Param("ID")
	minuta, err := strconv.ParseInt(ctx.Param("Minuta"), 10, 64)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se indico una minuta válida"))
		return
	}
	userID, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}

	nota := struct {
		Nota string `json:"nota"`
	}{}
	err = ctx.BindJSON(&nota)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"))
		return
	}

	err = Querys.AddNota(minuta, reunion, userID, nota.Nota)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}

	Functions.ResponseSuccess(200, gin.H{"nota": nota.Nota}, ctx)
}

func ConnectReunion(ctx *gin.Context) {
	reunion := ctx.Param("ID")
	token := ctx.Query("token")
	usuario, err := Functions.DecodeToken(token)
	if err != nil {
		fmt.Println("error al decodificar token", err)
		Functions.ResponseError(ctx, err, errors.New("error al decodificar token"))
		return
	}
	permiso, err := Querys.VerificarUsuarioReunion(reunion, usuario.ID)
	if err != nil || !permiso {
		fmt.Println("error al verificar permiso", err, permiso)
		Functions.ResponseError(ctx, err, errors.New("Error al verificar usuario reunion"))
		return
	}
	melody := Functions.GetMelody()

	ctx.Request.Header.Set("reunion", reunion)
	ctx.Request.Header.Set("token", token)
	err = melody.HandleRequest(ctx.Writer, ctx.Request)
	if err != nil {
		fmt.Println("error al conectar socket", err)
		Functions.ResponseError(ctx, err, errors.New("error al conectar socket"))
		return
	}
}

func Finalizar(ctx *gin.Context) {
	reunion := ctx.Param("ID")
	usuario, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	permiso, err := Querys.VerificarPermisoByReunion(reunion, usuario)
	if err != nil || !permiso {
		Functions.ResponseError(ctx, errors.New("No tiene permiso para esta accion"), err)
		return
	}
	err = Querys.FinalizarReunion(reunion)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.SendMesage(Constants.FINALIZA_REUNION, reunion, usuario, reunion)
	reu, _ := Querys.GetReunion(reunion, usuario)
	Functions.ResponseSuccess(200, gin.H{"reunion": reu}, ctx)
}

func Confirmar(ctx *gin.Context) {
	asistencia, err := strconv.ParseBool(ctx.DefaultQuery("asistencia", "true"))
	if err != nil {
		asistencia = true
	}
	usuario := ctx.Query("usuario")
	reunion := ctx.Param("ID")
	if usuario == "" {
		ctx.HTML(http.StatusOK, "confirma.tmpl", gin.H{
			"title":    "No se pudo confirmar asistencia",
			"confirmo": false,
			"message": "No pudimos confirmar su asistencia por favor ingrese a la aplicación para " +
				"verificar su agenda.",
		})
		return
	}
	err = Querys.ConfirmarAsistenciaReunion(usuario, reunion, asistencia)
	if err != nil {
		ctx.HTML(http.StatusOK, "confirma.tmpl", gin.H{
			"title":    "No se pudo confirmar asistencia",
			"confirmo": false,
			"message": "No pudimos confirmar su asistencia por favor ingrese a la aplicación para " +
				"verificar su agenda.",
		})
		return
	}
	title := ""
	message := ""
	if asistencia {
		title = "Confirmacion de reunión"
		message = "Hemos confirmado su asistencia a la reunion, revise su agenda en Worbuc"
	} else {
		title = "Confirmacion de no asistencia a reunión"
		message = "Se ha confirmado que usted no asistira a la reunión. Ingrese a Worbuc para ver su agenda."
	}
	ctx.HTML(http.StatusOK, "confirma.tmpl", gin.H{
		"title":    title,
		"confirmo": true,
		"message":  message,
	})
}

func Pdf(ctx *gin.Context) {
	reunion := ctx.Param("ID")
	usuario, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		usuario = ""
	}
	r, err := Querys.GetReunion(reunion, usuario)
	pdf, err := Functions.ReunionPdf(r)
	if err != nil {
		Functions.ResponseError(ctx, err)
	}
	pdf.Output(ctx.Writer)
	pdf.Close()
}

func Compartir(ctx *gin.Context) {
	reunion := ctx.Param("ID")
	var req = struct{ Correos []string `json:"correos"` }{}
	err := ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}
	r, err := Querys.GetReunion(reunion, "")
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	serverAddress := config.GetServerAddress() + "/"
	body := fmt.Sprintf(`%s <div style="margin:0px auto;max-width:600px;">
    <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
        <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
                                        <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                            <p><span style="font-size:16px;">%s te ha compartido la minuta de la reunión  %s  que se llevo a cabo el %s en 
                                                %s.
                                                </span></p>
                                            <p><span style="font-size:16px;">Accede al siguiente enlace para poder ver los acuerdos de la reunión</span></p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
</div>
        <div style="margin:0px auto;max-width:600px;">
            <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
                <tbody>
                    <tr>
                        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:0px 0px 0px 0px;">
                            <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:300px;">      <![endif]-->
                            <div class="mj-column-per-50 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                                <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                                    <tbody>
                                        <tr>
                                            <td style="word-wrap:break-word;font-size:0px;padding:10px 0px 10px 0px;padding-top:10px;padding-left:25px;"
                                                align="center">
                                                <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:separate;"
                                                    align="center" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="border:none;border-radius:24px;color:#fff;cursor:auto;padding:10px 25px;"
                                                                align="center" valign="middle" bgcolor="#C034E8"><a href="%s" style="text-decoration:none;background:#C034E8;color:#fff;font-family:Ubuntu, Helvetica, Arial, sans-serif, Helvetica, Arial, sans-serif;font-size:13px;font-weight:normal;line-height:120%%;text-transform:none;margin:0px;"
                                                                    target="_blank">Ver minuta</a></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>                        
                        </td>
                    </tr>
                </tbody>
            </table>
        </div> %s`,
        Constants.CABECERA_CORREO,
		r.Organizador.Nombre+" "+r.Organizador.ApellidoPaterno,
		r.Titulo,
		r.Fecha.Format("02-01-2006 a las 15:04"),
		strings.Split(r.Lugar, "COORDS=")[0],
		serverAddress+"reuniones/"+reunion+"/pdf",Constants.PIE_CORREO)

	m, err := config.CargarDatosCorreo("Minuta de reunión", body, req.Correos[0],
		true, req.Correos[:1]...)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	go config.EnviarCorreo(m)
	Functions.ResponseSuccess(200, nil, ctx)
}

func GetEtiquetas(ctx *gin.Context) {
	user, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	etiquetas, err := Querys.GetEtiquetas(user, "reuniones")
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"etiquetas": etiquetas}, ctx)
}
