package Reportes

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"github.com/tealeg/xlsx"
	"time"
	"github.com/kataras/iris/core/errors"
)

func ReporteUsuarios(ctx *gin.Context) {
	limit, page := Functions.GetPagination(ctx)
	usuarios,total, err := Querys.GetFullUsers(limit, page)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if ctx.GetHeader("Accept") == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" {
		file := xlsx.NewFile()
		sheet, err := file.AddSheet("Hoja 1")
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
		err = UsuariotoXLSX(usuarios, sheet)
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
		ctx.Header("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
		err = file.Write(ctx.Writer)
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
		return
	}

	Functions.ResponseSuccess(200, gin.H{"usuarios": usuarios,"total":total}, ctx)
}

func ReportesGenerales(ctx *gin.Context) {
	fechaInicio := ctx.DefaultQuery("fechaInicio", "")
	fechaTermino := ctx.DefaultQuery("fechaTermino", "")
	fI, err := time.Parse(time.RFC3339, fechaInicio)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("Fecha invalida para el parámetro fechaInicio"))
		return
	}
	fT, err := time.Parse(time.RFC3339, fechaTermino)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("Fecha invalida para el parámetro fechaTermino"))
		return
	}

	reportes, err := Querys.GetReportes(fI.UTC(), fT.UTC())
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"reportes": reportes}, ctx)
}
