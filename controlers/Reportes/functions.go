package Reportes

import (
	"github.com/tealeg/xlsx"
	"reflect"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"strconv"
	"time"
)

func UsuariotoXLSX(usuarios []*Querys.UsuarioReporte, sheet *xlsx.Sheet) (err error) {

	fontPlain := xlsx.NewFont(12, "Calibri")
	stylePlain := xlsx.NewStyle()
	stylePlain.Font = *fontPlain
	for i := range usuarios {
		InsertStruct(*usuarios[i], stylePlain, sheet, nil, i == 0)
	}

	return
}

func InsertaEncabezado(styleBold *xlsx.Style, sheet *xlsx.Sheet, valores []string) {
	row := sheet.AddRow()

	for _, valor := range valores {
		cell := row.AddCell()
		cell.SetStyle(styleBold)

		cell.Value = valor
	}
}

func InsertStruct(object interface{}, stylePlain *xlsx.Style, sheet *xlsx.Sheet, row *xlsx.Row, isHeader bool) error {
	if row == nil {
		row = sheet.AddRow()
	}
	if isHeader {
		fontBold := xlsx.NewFont(11, "Calibri")
		fontBold.Bold = true
		styleBold := xlsx.NewStyle()
		styleBold.Font = *fontBold
		if reflect.ValueOf(object).Kind() == reflect.Struct {
			s := reflect.New(reflect.TypeOf(object)).Elem()
			typeOfT := s.Type()
			for i := 0; i < s.NumField(); i++ {
				cell := row.AddCell()
				cell.SetStyle(styleBold)
				cell.Value = typeOfT.Field(i).Name
			}
		}
		fontBold.Bold=false
		row=sheet.AddRow()
	}

	if reflect.ValueOf(object).Kind() == reflect.Struct {
		s := reflect.New(reflect.TypeOf(object)).Elem()
		for i := 0; i < s.NumField(); i++ {
			cell := row.AddCell()
			cell.SetStyle(stylePlain)
			if reflect.ValueOf(object).Field(i).Kind() == reflect.Int || reflect.ValueOf(object).Field(i).Kind() == reflect.Int64 {
				cell.Value = strconv.FormatInt(reflect.ValueOf(object).Field(i).Int(), 10)
				continue
			}
			if reflect.ValueOf(object).Field(i).Kind() == reflect.Float32 || reflect.ValueOf(object).Field(i).Kind() == reflect.Float64 {
				cell.Value = strconv.FormatFloat(reflect.ValueOf(object).Field(i).Float(), 'f', 4, 64)
				continue
			}
			if reflect.ValueOf(object).Field(i).Kind() == reflect.Bool {
				cell.Value = strconv.FormatBool(reflect.ValueOf(object).Field(i).Bool())
				continue
			}
			if reflect.ValueOf(object).Field(i).Kind() == reflect.String {
				cell.Value = reflect.ValueOf(object).Field(i).String()
				continue
			}

			if t, ok := reflect.ValueOf(object).Field(i).Interface().(time.Time); ok {
				if !t.IsZero() {
					cell.Value = t.Format(time.RFC3339)
				}
			}
		}

	}
	return nil
}
