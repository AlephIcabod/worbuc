package Utils

import (
	"github.com/gin-gonic/gin"
	"strconv"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"net/http"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
)

func GetPaises(ctx *gin.Context) {
	paises, err := Querys.GetPaises()
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"paises": paises}, ctx)
}

func GetEscuelas(ctx *gin.Context) {
	limit, _ := strconv.ParseInt(ctx.DefaultQuery("limit", "20"), 10, 64)
	page, _ := strconv.ParseInt(ctx.DefaultQuery("page", "1"), 10, 64)
	nombre := ctx.DefaultQuery("name", "")
	schools, err := Querys.GetSchools(int(limit), int(page), nombre)
	if err != nil {

		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"escuelas": schools}, ctx)
}

func GetEmpresas(ctx *gin.Context) {
	limit, _ := strconv.ParseInt(ctx.DefaultQuery("limit", "20"), 10, 64)
	page, _ := strconv.ParseInt(ctx.DefaultQuery("page", "1"), 10, 64)
	sector := ctx.DefaultQuery("sector", "")
	empresas, err := Querys.GetEmpresas(int(limit), int(page), sector)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"empresas": empresas}, ctx)
}

func GetApps(ctx *gin.Context) {
	aux := make([]struct {
		Id     int
		Nombre string
	}, 0)
	for i, v := range Constants.Aplicaciones {
		aux = append(aux, struct {
			Id     int
			Nombre string
		}{Id: v, Nombre: i})
	}

	Functions.Response(http.StatusOK, "Aplicaciones", gin.H{"apps": aux}, ctx)
}
