package User

import (
	"github.com/gin-gonic/gin"
	"strconv"

	"fmt"
	"os"
	"encoding/base64"
	"errors"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"github.com/rs/xid"
	"bitbucket.org/AlephIcabod/worbuc/config"
	"bitbucket.org/AlephIcabod/worbuc/controlers/OpenPay"
	"log"
)

func SignUp(ctx *gin.Context) {
	var user Register
	err := ctx.BindJSON(&user)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	user.Genero = "N"
	if user.Password == user.ConfirmPassword {
		cifrado, err := Functions.Encrypt(user.Password)
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
		user.Password = string(cifrado)

		usuarioNuevo := Schemas.Usuario{
			Password:        user.Password,
			Email:           user.Email,
			TipoPerfil:      Constants.PERFIL_PUBLICO,
			Nombre:          user.Nombre,
			ApellidoPaterno: user.ApellidoPaterno,
			ApellidoMaterno: user.ApellidoMaterno,
			Genero:          user.Genero,
			IsAdmin:         false,
			Estatus:         Constants.REGISTRADO}
		existentente, err := Querys.GetUserByEmail(user.Email)
		if err != nil && err.Error() == Constants.ErrorNoResultsUser {
			err = Querys.Create(&usuarioNuevo)
		} else {
			if existentente != nil && existentente.Estatus == Constants.PREREGISTRADO {
				err = Querys.UpdateUser(existentente, &usuarioNuevo)
				usuarioNuevo.Id = existentente.Id
			}
		}
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
		plan, _, err := Querys.GetUserPlan(usuarioNuevo.Id)
		token, err := Functions.CreateTokenString(usuarioNuevo.Id, usuarioNuevo.Nombre+" "+usuarioNuevo.ApellidoPaterno+" "+usuarioNuevo.ApellidoMaterno, user.Email, usuarioNuevo.TipoPerfil, usuarioNuevo.Estatus,
			*plan.Vigencia,
			usuarioNuevo.IsAdmin)
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
		body := fmt.Sprintf(`%s <div style="margin:0px auto;max-width:600px;">
    <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
        <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
                                        <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                            <p><span style="font-size:16px;"><strong>%s</strong> el equipo de Worbuc te da la bienvenida,
                                                y esperamos que tengas la mejor experiencia mientras usas Worbuc.
                                                </span></p>
                                            <p><span style="font-size:16px;">Ayudanos a crecer invitando a tus contactos</span></p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
</div> %s`, Constants.CABECERA_CORREO, usuarioNuevo.Nombre, Constants.PIE_CORREO)
		m, _ := config.CargarDatosCorreo("Bienvenido a Worbuc", body, usuarioNuevo.Email, true)
		go config.EnviarCorreo(m)

		Functions.ResponseSuccess(201, gin.H{"token": token}, ctx)
		return
	} else {
		Functions.ResponseError(ctx, errors.New("Las contraseñas no son iguales"))
	}

}

func GetOne(ctx *gin.Context) {
	id := ctx.Param("ID")
	if id != "" {
		user, err := Querys.GetUser(id)
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}

		if (user.ClienteId == nil || *user.ClienteId == "") && user.Estatus == Constants.REGISTRADO {
			go func() {
				clienteId, err := OpenPay.ObtenerCliente(user.Id)
				if err != nil {
					clienteId, err = OpenPay.CrearCuentaCliente(user)
				}
				err = Querys.CreateCuenta(user.Id, clienteId)
				if err != nil {
					fmt.Println(err)
				}
			}()
		}

		Functions.ResponseSuccess(200, gin.H{"user": user}, ctx)
		return
	}
	Functions.ResponseError(ctx, errors.New("Id invalido"))
}

func ChangePhoto(ctx *gin.Context) {
	id := ctx.Param("ID")
	var req Photo
	err := ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, err, errors.New("Error leyendo"))
		return
	}
	if id == req.Id {
		user, err := Querys.GetUser(id)
		if err != nil {
			Functions.ResponseError(ctx, err, errors.New("error obteniendo usuario"))
			return
		}
		url := "./public/imagenes/" + xid.New().String() + req.Id
		f, err := os.Create(url)
		dec, err := base64.StdEncoding.DecodeString(req.Image)
		if err != nil {
			Functions.ResponseError(ctx, err, errors.New("Error decodificando"))
			return
		}
		defer f.Close()
		if _, err := f.Write(dec); err != nil {

			Functions.ResponseError(ctx, err, errors.New("error escribiendo archivo"))
			return
		}
		if err := f.Sync(); err != nil {
			Functions.ResponseError(ctx, err, errors.New("error sync"))
			return
		}
		if user.FotoPerfil != "/public/defaults/avatar.png" {
			os.Remove("." + user.FotoPerfil)
		}
		err = Querys.UpdatePhoto(user, url[1:])
		if err != nil {
			fmt.Println("error actualizando base de datos")
			Functions.ResponseError(ctx, err)
			return
		}

		Functions.Response(200, "ok", gin.H{"nueva": url}, ctx)
		return
	}
	Functions.ResponseError(ctx, errors.New("Permiso denegado"))
}

func UpdateBasics(ctx *gin.Context) {
	id := ctx.Param("ID")
	var req Basics
	err := ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"))
		return
	}

	if req.Genero != "H" && req.Genero != "M" && req.Genero != "N" {
		Functions.ResponseError(ctx, errors.New("Valor para género inválido"))
		return
	}
	user, err := Querys.GetUser(id)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err = Querys.UpdateBasics(user, req.Nombre, req.ApellidoPaterno, req.ApellidoMaterno, req.Genero, req.FechaNacimiento)

	if err != nil {
		Functions.ResponseError(ctx, err)
	}
	Functions.ResponseSuccess(200, nil, ctx)
}

func SavePhone(ctx *gin.Context) {
	id := ctx.Param("ID")
	ph := ctx.Param("PHONE")
	var phone Schemas.Telefono
	err := ctx.BindJSON(&phone)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo decodificar la petición"))
		return
	}
	phone.UsuarioID = id
	if ph != "" {
		p, _ := strconv.ParseInt(ph, 10, 64)
		err = Querys.CreateOrUpdatePhone(&phone, int(p))
	} else {
		err = Querys.CreateOrUpdatePhone(&phone, -1)
	}
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo guardar telefono"), err)
		return
	}
	Functions.ResponseSuccess(201, gin.H{"telefono": phone}, ctx)
}

func RemovePhone(ctx *gin.Context) {
	id := ctx.Param("ID")
	ph := ctx.Param("PHONE")

	if ph != "" {
		p, _ := strconv.ParseInt(ph, 10, 64)
		err := Querys.RemovePhone(p, id)
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
		Functions.ResponseSuccess(204, nil, ctx)
	} else {
		Functions.ResponseError(ctx, errors.New("No se especifico un id válido"))
		return
	}
}

func UpdateAddress(ctx *gin.Context) {
	id := ctx.Param("ID")
	var req Address
	err := ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"))
		return
	}
	user, err := Querys.GetUser(id)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	user.Direccion = &Schemas.Direccion{}
	user.Direccion.EstadoID = req.Estado
	user.Direccion.Poblacion = req.Poblacion
	user.Direccion.CodigoPostal = req.CodigoPostal
	user.Direccion.UsuarioID = user.Id
	user.Direccion.PaisID = req.Pais

	err = Querys.CreateOrUpdateAddress(user, nil)
	if err != nil {
		Functions.ResponseError(ctx, err)
	}
	Functions.ResponseSuccess(200, gin.H{"Direccion": user.Direccion}, ctx)
}

func UpdateSchool(ctx *gin.Context) {
	id := ctx.Param("ID")
	var req ReqSchoolJob
	err := ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}
	if id != req.Id {
		Functions.ResponseError(ctx, errors.New("Id no coincide"), err)
		return
	}
	user, err := Querys.GetUser(id)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se encontro ningun usuario con ese id"), err)
		return
	}

	user.Educacion = &Schemas.Educacion{
		Institucion: &Schemas.Institucion{Nombre: req.NombreEscuela,},
		UsuarioID:   id,
		Periodo:     req.Periodo,
	}
	user.Escolaridad = req.Escolaridad

	err = Querys.CreateOrUpdateSchoolar(user, nil)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"escolaridad": user.Educacion}, ctx)
}

func UpdateJob(ctx *gin.Context) {
	id := ctx.Param("ID")
	var req ReqSchoolJob
	err := ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}
	if id != req.Id {
		Functions.ResponseError(ctx, errors.New("Id no coincide"), err)
		return
	}

	user, err := Querys.GetUser(id)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se encontro ningun usuario con ese id"), err)
		return
	}

	user.Empleo = &Schemas.Empleo{
		UsuarioID: id,
		Puesto:    req.Puesto,
		Empresa:   &Schemas.Empresa{Nombre: req.NombreEmpresa, Sector: req.Sector},
	}

	err = Querys.CreateOrUpdateJob(user, nil)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"empleo": user.Empleo}, ctx)

}

func UpdateSettings(ctx *gin.Context) {
	id := ctx.Param("ID")
	var req ReqSettings
	err := ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer peticion"), err)
		return
	}
	tipoPerfil := Functions.TernaryOperator(req.PerfilPublico, Constants.PERFIL_PUBLICO, Constants.PERFIIL_PRIVADO).(string)
	err = Querys.UpdateSettings(id, tipoPerfil, req.ZonaHoraria)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo actualizar: "+err.Error()))
		return
	}
	Functions.ResponseSuccess(200, gin.H{"settings": req}, ctx)
}

func UpdatePassword(ctx *gin.Context) {
	id := ctx.Param("ID")
	var req ReqRecover
	err := ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if len(req.Nuevo) < 6 || len(req.Previo) < 5 || len(req.Confirmacion) < 6 {
		Functions.ResponseError(ctx, errors.New("La longitud minima de la contraseña es de 6 caracteres"))
		return
	}

	if req.Nuevo != req.Confirmacion {
		Functions.ResponseError(ctx, errors.New("La contraseña nueva no coincide con la confirmacion"))
		return
	}

	err = Querys.UpdatePassword(id, req.Previo, req.Nuevo)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, nil, ctx)
}

func Search(ctx *gin.Context) {
	query := ctx.DefaultQuery("name", "")
	correo := ctx.DefaultQuery("email", "")
	incluyeOcultos := ctx.DefaultQuery("ocultos", "no")
	if correo != "" {
		u, err := Querys.GetUserByEmail(correo)
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
		Functions.ResponseSuccess(200, gin.H{"usuario": u}, ctx)
		return
	}
	if query == "" {
		Functions.ResponseError(ctx, errors.New("No hay parametro de busqueda"))
		return
	}
	limit, err := strconv.Atoi(ctx.DefaultQuery("limit", "10"))
	if err != nil {
		limit = 10
	}
	page, err := strconv.Atoi(ctx.DefaultQuery("page", "1"))
	if err != nil {
		page = 1
	}
	userID, _ := Functions.GetUserId(ctx.Request)
	users, err := Querys.FindUsers(query, limit, page, userID, incluyeOcultos == "si")
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	us := mapUsers(users)
	Functions.ResponseSuccess(200, gin.H{"usuarios": us}, ctx)
}

func AddContact(ctx *gin.Context) {
	id := ctx.Param("ID")
	create, _ := strconv.ParseBool(ctx.DefaultQuery("create", "false"))
	usuario, _ := Functions.GetUser(ctx.Request)
	if id != usuario.ID {
		Functions.ResponseError(ctx, errors.New("No puede agregar contactos a otro usuario"))
		return
	}
	var contacto *Querys.UserFinded
	link := config.GetServerAddress() + "/"
	body := "" + link
	subject := ""
	var err error
	if !create {
		contacto, err = addContact(ctx, usuario.ID)
		subject = "Nuevo contacto"
		body = fmt.Sprintf(`<h2>Nuevo contacto</h2>
		<p><strong>%s</strong> te ha agregado como contacto, encuentra a más personas
		 en tu cuenta y administra mejor tus recursos.</p>
		<p><small>Worbuc Team</small></p>
		<a href='%s'>Ir a Worbuc</a>
		`, usuario.Nombre, link)
	} else {
		subject = "Invitación"
		body = fmt.Sprintf(`%s <div style="margin:0px auto;max-width:600px;">
    <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
        <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
                                        <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                            <p><span style="font-size:16px;"><strong>%s</strong> te ha agregado como contacto, te invitamos a unirte a Worbuc, donde podras encontrar la mejor herramienta para administrar tu tiempo en la oficina. 
                                                </span></p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
</div> %s
		`, Constants.CABECERA_CORREO, usuario.Nombre, Constants.PIE_CORREO)
		contacto, err = createContact(ctx, usuario.ID)
	}
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	m, _ := config.CargarDatosCorreo(subject, body, contacto.Email, true)
	go config.EnviarCorreo(m)
	Functions.ResponseSuccess(201, gin.H{
		"id":              contacto.Id,
		"email":           contacto.Email,
		"nombre":          contacto.Nombre,
		"apellidoPaterno": contacto.ApellidoPaterno,
		"apellidoMaterno": contacto.ApellidoMaterno,
		"fotoPerfil":      contacto.FotoPerfil,
		"contacto":        contacto.Contacto,
	}, ctx)
}

func GetContacts(ctx *gin.Context) {
	id := ctx.Param("ID")
	if id == "" {
		Functions.ResponseError(ctx, errors.New("Id de usuario invalido"))
		return
	}
	contacts, err := Querys.GetUserContacts(id)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	// us := mapUsers(contacts)
	Functions.ResponseSuccess(200, gin.H{"contactos": contacts}, ctx)
}

func RemoveContact(ctx *gin.Context) {
	id := ctx.Param("ID")
	contact := ctx.Param("contact")
	if id == "" || contact == "" {
		Functions.ResponseError(ctx, errors.New("Ids invalidos"))
		return
	}

	err := Querys.RemoveContact(id, contact)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(204, nil, ctx)
}

func DeletePhoto(ctx *gin.Context) {
	id := ctx.Param("ID")
	if id == "" {
		Functions.ResponseError(ctx, errors.New("Id invalido"))
		return
	}
	user, err := Querys.GetUser(id)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	previo := user.FotoPerfil
	err = Querys.UpdatePhoto(user, Constants.DEFAULT_IMAGE)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err = os.Remove(previo)
	if err != nil {
		fmt.Println(err)
	}
	Functions.ResponseSuccess(200, gin.H{"photo": Constants.DEFAULT_IMAGE}, ctx)

}

func GetPlan(ctx *gin.Context) {
	userId := ctx.Param("ID")
	plan, ocupado, err := Querys.GetUserPlan(userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	disponible := plan.Plan.LimiteMb - ocupado
	Functions.ResponseSuccess(200, gin.H{"plan": plan, "ocupado": ocupado, "disponible": disponible}, ctx)
}

func GetFiles(ctx *gin.Context) {
	id := ctx.Param("ID")
	files, err := Querys.GetFiles(id)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"files": files}, ctx)
}

func Socket(ctx *gin.Context) {
	token := ctx.Query("token")
	_, err := Functions.DecodeToken(token)
	if err != nil {
		log.Println("error al decodificar token", err)
		Functions.ResponseError(ctx, err, errors.New("error al decodificar token"))
		return
	}
	melody := Functions.GetMelody()
	ctx.Request.Header.Set("token", token)
	err = melody.HandleRequest(ctx.Writer, ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err, errors.New("error al conectar socket"))
		return
	}
}

func GetNotifications(ctx *gin.Context) {
	userID := ctx.Param("ID")
	notifications, err := Querys.GetNotifications(userID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"notificaciones": notifications}, ctx)
}

func CheckNotification(ctx *gin.Context) {
	user, err := Functions.GetUser(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	id := ctx.Param("ID")
	err = Querys.CheckNotification(id, user.ID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(204, nil, ctx)
}

func CancelarSuscripcion(ctx *gin.Context) {
	userId := ctx.Param("ID")
	id, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if id != userId {
		Functions.ResponseError(ctx, errors.New("No tiene permiso para cancelar esta suscripción"))
		return
	}

	plan, _, err := Querys.GetUserPlan(userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if !plan.Renovar {
		Functions.ResponseError(ctx, errors.New("Este suscripcion ya ha sido cancelada anteriormente"))
		return
	}

	err = Querys.CancelarSuscripcion(userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}

	plan, _, err = Querys.GetUserPlan(userId)
	Functions.ResponseSuccess(200, gin.H{"plan": plan}, ctx)

}
