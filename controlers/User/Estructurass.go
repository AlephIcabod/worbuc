package User

import "time"

type Register struct {
	Email           string `json:"email"`
	Password        string `json:"password"`
	ConfirmPassword string `json:"confirmPassword"`
	ApellidoPaterno string `json:"apellidoPaterno"`
	ApellidoMaterno string `json:"apellidoMaterno"`
	Nombre          string `json:"nombre"`
	Genero          string `json:"genero"`
}

type Photo struct {
	Image     string `json:"photo"`
	Id        string `json:"id"`
	Extension string `json:"extension"`
}

type Basics struct {
	Nombre          string    `json:"nombre"`
	ApellidoPaterno string    `json:"apellidoPaterno"`
	ApellidoMaterno string    `json:"apellidoMaterno"`
	FechaNacimiento time.Time `json:"fechaNacimiento"`
	Genero          string    `json:"genero"`
}
type Address struct {
	Id           int64  `json:"Id"`
	Pais         int64  `json:"Pais"`
	Estado       int64  `json:"Estado"`
	Poblacion    string `json:"Poblacion"`
	CodigoPostal string `json:"CodigoPostal"`
}

type ReqSchoolJob struct {
	Id            string `json:"id"`
	Escolaridad   string `json:"escolaridad"`
	NombreEscuela string `json:"nombreEscuela"`
	Periodo       string `json:"periodo"`
	NombreEmpresa string `json:"nombreEmpresa"`
	Sector        string `json:"sector"`
	Puesto        string `json:"puesto"`
}

type ReqSettings struct {
	ZonaHoraria   string `json:"zonaHoraria"`
	PerfilPublico bool   `json:"perfilPublico"`
}

type ReqRecover struct {
	Previo       string `json:"previo"`
	Nuevo        string `json:"nuevo"`
	Confirmacion string `json:"confirmacion"`
}

type ReqContact struct {
	Contacto string `json:"contacto"`
}

type ReqNewContact struct {
	Nombre          string `json:"nombre"`
	ApellidoPaterno string `json:"apellidoPaterno"`
	ApellidoMaterno string `json:"apellidoMaterno"`
	Email           string `json:"email"`
}
