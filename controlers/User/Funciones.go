package User

import (
	"github.com/gin-gonic/gin"
	"errors"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"github.com/go-pg/pg"
	"log"
	"bitbucket.org/AlephIcabod/worbuc/config"
	"fmt"
	"bitbucket.org/AlephIcabod/worbuc/controlers/OpenPay"
)

func mapUsers(users []Querys.UserFinded) []gin.H {
	var us []gin.H = make([]gin.H, 0, len(users))
	for _, v := range users {
		us = append(us, gin.H{
			"id":              v.Id,
			"email":           v.Email,
			"nombre":          v.Nombre,
			"apellidoPaterno": v.ApellidoPaterno,
			"apellidoMaterno": v.ApellidoMaterno,
			"fotoPerfil":      v.FotoPerfil,
			"contacto":        v.Contacto})
	}
	return us
}

func addContact(ctx *gin.Context, id string) (*Querys.UserFinded, error) {
	var req ReqContact
	err := ctx.BindJSON(&req)
	if err != nil {
		return nil, err
	}
	if req.Contacto == "" {
		return nil, errors.New("Id invalido")
	}
	err = Querys.AddContact(id, req.Contacto)
	if err != nil {
		return nil, err
	}
	usuario, _ := Querys.GetUser(req.Contacto)
	return &Querys.UserFinded{Contacto: true, Usuario: *usuario}, err
}

func createContact(ctx *gin.Context, id string) (*Querys.UserFinded, error) {
	var err error
	var req ReqNewContact
	err = ctx.BindJSON(&req)
	if err != nil {
		return nil, err
	}
	if req.Email == "" {
		return nil, errors.New("Email inválido")
	}
	usuario, err := Querys.GetUserByEmail(req.Email)
	if err != nil && err.Error() != Constants.ErrorNoResultsUser {
		return nil, err
	}
	if err != nil && err.Error() == Constants.ErrorNoResultsUser || usuario == nil {
		password, _ := Functions.Encrypt("12345")
		nuevo := Schemas.Usuario{Email: req.Email,
			Estatus:         Constants.PREREGISTRADO,
			ApellidoPaterno: req.ApellidoPaterno,
			ApellidoMaterno: req.ApellidoMaterno,
			Nombre:          req.Nombre,
			TipoPerfil:      Constants.PERFIL_PUBLICO,
			Genero:          Constants.NOESPECIFICADO,
			Password:        password,
		}
		err = Querys.Create(&nuevo)
		if err != nil {
			return nil, err
		}
		err = Querys.AddContact(id, nuevo.Id)
		return &Querys.UserFinded{Contacto: true, Usuario: nuevo}, nil
	} else {
		err = Querys.AddContact(id, usuario.Id)
		return &Querys.UserFinded{Contacto: true, Usuario: *usuario}, nil
	}
}

func RenovarSuscripciones() {
	suscripciones, err := Querys.ObtenerSuscripciones()
	if err != nil {
		if err != pg.ErrNoRows {
			log.Println("Error al obtener suscripciones")
		}
	}

	for _, v := range suscripciones {
		if v.Renovar && v.TarjetaId != nil && *v.TarjetaId != "" {
			err = OpenPay.RenovarSuscripcion(v.Plan, 1, v.Usuario, *v.TarjetaId, nil, nil)
			if err != nil {
				go EnviarCorreoSuscripcion(v, 3)
				continue
			}
			go EnviarCorreoSuscripcion(v, 1)
		} else {
			if v.Renovar {
				go EnviarCorreoSuscripcion(v, 3)
			} else {
				go EnviarCorreoSuscripcion(v, 2)
			}
		}
	}
}

func EnviarCorreoSuscripcion(p *Schemas.PlanUsuario, tipo int) {
	//serverAddress := config.GetServerAddress() + "/"
	body := ""
	subject := ""
	switch tipo {
	case 1:
		subject = "Suscripción renovada"
		body = fmt.Sprintf(`%s
<div style="margin:0px auto;max-width:600px;">
            <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
                <tbody>
                    <tr>
                        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                            <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
                            <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                                <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                                    <tbody>
                                        <tr>
                                            <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
                                                <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                                    <p><span style="font-size:16px;">
                                                        Tu suscripción al plan %s ha sido renovada.                             
                                                        </span>
                                                    </p>
                                                    <p style="font-size:16px;">
                                                        Sigue disfrutando de los beneficios de worbuc en tu trabajo.
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
%s`, Constants.CABECERA_CORREO,
			p.Plan.Nombre,
			Constants.PIE_CORREO)
	case 2:
		subject = "Cancelación de suscripción"
		body = fmt.Sprintf(`%s
		<div style="margin:0px auto;max-width:600px;">
            <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
                <tbody>
                    <tr>
                        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                            <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
                            <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                                <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                                    <tbody>
                                        <tr>
                                            <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
                                                <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                                    <p><span style="font-size:16px;">
                                                        Tu suscripción al plan %s ha sido cancelada.                                                         
                                                        </span>
                                                    </p>                                         
                                                    <p style="font-size:16px;">Lamentamos que te hayas ido, esperamos que vuelvas a trabajar con nosotros.</p>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]>      </td></tr></table>      <![endif]--> 
%s`, Constants.CABECERA_CORREO,
			p.Plan.Nombre,
			Constants.PIE_CORREO)
	case 3:
		subject = "No se pudo renovar tu suscripción"
		body = fmt.Sprintf(`%s <div style="margin:0px auto;max-width:600px;">
            <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
                <tbody>
                    <tr>
                        <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                            <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
                            <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                                <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                                    <tbody>
                                        <tr>
                                            <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
                                                <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                                    <p><span style="font-size:16px;">
                                                        Tu suscripción al plan %s no pudo ser renovada de manera automática.		
                                                        </span>
                                                    </p>                                         
                                                    <p style="font-size:16px;">
                                                        Por favor ingresa a worbuc.com y renueva tu suscripción de manera manual. 
                                                    </p>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!--[if mso | IE]>      </td></tr></table>      <![endif]--> %s`,
			Constants.CABECERA_CORREO,
			p.Plan.Nombre,
			Constants.PIE_CORREO)
	}

	m, _ := config.CargarDatosCorreo(subject, body, p.Usuario.Email, true)
	go config.EnviarCorreo(m)
}
