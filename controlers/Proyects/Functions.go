package Proyects

import (
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"github.com/rs/xid"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"time"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
)

func parseTareaRequest(req *ReqTarea, id, actividad, userId string, nuevo bool) (*Schemas.Tarea) {
	tarea := &Schemas.Tarea{Nombre: req.Nombre,
		Descripcion:     req.Descripcion,
		FechaProgramada: req.FechaProgramada.UTC(),
		ActividadID:     actividad,
		Prioridad:       req.Prioridad,
		Id:              xid.New().String(),
	}
	if id != "" {
		tarea.Id = id
	}
	if nuevo {
		tarea.FechaCreacion = time.Now().UTC()
		tarea.Estatus = Constants.TAREA_PENDIENTE
		tarea.CreadorID = userId
	}

	responsables := make([]*Schemas.ParticipanteTarea, 0)
	supervisores := make([]*Schemas.ParticipanteTarea, 0)
	for _, p := range req.Participantes {
		aux := &Schemas.ParticipanteTarea{Rol: p.Rol,
			TareaID:   tarea.Id,
			UsuarioID: p.Usuario, Id: xid.New().String()}
		if p.Rol == Constants.ROL_SUPERVISOR {
			supervisores = append(supervisores, aux)
		} else {
			responsables = append(responsables, aux)
		}
	}
	tarea.Supervisores = supervisores
	tarea.Responsables = responsables
	tarea.Archivos = make([]*Schemas.ArchivoTarea, 0)
	for _, v := range req.Archivos {
		tarea.Archivos = append(tarea.Archivos,
			&Schemas.ArchivoTarea{Id: xid.New().String(),
				ArchivoID: v,
				TareaID:   tarea.Id,
			})
	}

	return tarea
}

func notifyTarea(tarea *Schemas.Tarea, logeado *Functions.UsuarioLogeado, text string) {
	notificaciones := make([]*Schemas.Notification, 0)
	for _, v := range tarea.Responsables {
		if v.Usuario.Usuario.Id != logeado.ID {
			notificaciones = append(notificaciones, &Schemas.
			Notification{Id: xid.New().String(),
				Fecha:      time.Now().UTC(),
				Adicional:  tarea.Id,
				EmisorID:   logeado.ID,
				Tipo:       "Tarea",
				ReceptorID: v.Usuario.Usuario.Id,
				Texto:      logeado.Nombre + " " + text + " " + tarea.Nombre,
				Titulo:  tarea.Actividad.Proyecto.Nombre,
			})
		}
	}
	for _, v := range tarea.Supervisores {
		if v.Usuario.Usuario.Id != logeado.ID {
			notificaciones = append(notificaciones, &Schemas.
			Notification{Id: xid.New().String(),
				Fecha:      time.Now().UTC(),
				Adicional:  tarea.Id,
				EmisorID:   logeado.ID,
				Tipo:       "Tarea",
				ReceptorID: v.Usuario.Usuario.Id,
				Texto:      logeado.Nombre + " " + text + " " + tarea.Nombre,
				Titulo:  tarea.Actividad.Proyecto.Nombre,
			})
		}
	}
	Querys.CreateNotification(notificaciones...)
}
