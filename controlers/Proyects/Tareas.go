package Proyects

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"errors"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"time"
	"github.com/rs/xid"
	"strconv"
)

func GetTarea(ctx *gin.Context) {
	idTarea := ctx.Param("ID")
	user, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	tarea, err := Querys.GetTarea(idTarea, user)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"tarea": tarea}, ctx)
}

func CreateTarea(ctx *gin.Context) {
	idAct := ctx.Param("ID")
	userId, err := Functions.GetUser(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	var req ReqTarea
	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo decodificar la peticion"), err)
		return
	}
	actividad, err := Querys.GetActividad(idAct, userId.ID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if actividad.Estatus == Constants.ACTIVIDAD_CANCELADA || actividad.Estatus == Constants.ACTIVIDAD_FINALIZADA {
		Functions.ResponseError(ctx, errors.New("No puede crear tareas sobre una actividad cancelada o finalizada"))
		return
	}
	nuevaTarea := parseTareaRequest(&req, "", actividad.Id, userId.ID, true)
	err = Querys.CreateTarea(nuevaTarea, actividad.ProyectoID, req.Archivos)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	nuevaTarea, _ = Querys.GetTarea(nuevaTarea.Id, userId.ID)
	go nuevaTarea.EnviarCorreo(Constants.ASIGNACION, userId.Nombre, userId.ID)

	go notifyTarea(nuevaTarea, userId, "te ha agregado a la tarea")
	Functions.ResponseSuccess(200, gin.H{"tarea": nuevaTarea}, ctx)
}

func UpdateTarea(ctx *gin.Context) {
	id := ctx.Param("ID")
	userId, err := Functions.GetUser(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	var req ReqTarea
	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo decodificar la peticion"), err)
		return
	}
	tarea := parseTareaRequest(&req, id, req.Actividad, userId.ID, false)
	_, err = Querys.GetActividad(req.Actividad, userId.ID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err = Querys.UpdateTarea(tarea, userId.ID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	tarea, _ = Querys.GetTarea(id, userId.ID)
	go tarea.EnviarCorreo(Constants.ACTUALIZACION, userId.Nombre, userId.ID)

	go notifyTarea(tarea, userId, "ha modificado la tarea")
	Functions.ResponseSuccess(200, gin.H{"tarea": tarea}, ctx)
}

func GetTareasMes(ctx *gin.Context) {
	mes, _ := strconv.ParseInt(ctx.DefaultQuery("mes", "-1"), 10, 32)
	anio, _ := strconv.ParseInt(ctx.DefaultQuery("anio", "-1"), 10, 32)
	userID, _ := Functions.GetUserId(ctx.Request)
	if mes > 0 {
		mes++
	} else {
		mes = 1
	}
	if anio < 0 {
		anio = int64(time.Now().UTC().Year())
	}
	tareas, err := Querys.GetTareasMes(userID, int32(mes), int32(anio))
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"tareas": tareas}, ctx)
	return

}

func CancelarTarea(ctx *gin.Context) {
	id := ctx.Param("ID")
	userId, err := Functions.GetUser(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	tarea, err := Querys.CambiarEstatusTarea(id, userId.ID, Constants.TAREA_CANCELADA, true)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	tarea, _ = Querys.GetTarea(id, userId.ID)
	go tarea.EnviarCorreo(Constants.CANCELACION, userId.Nombre, userId.ID)
	go notifyTarea(tarea, userId, "ha cancelado la tarea ")
	Functions.ResponseSuccess(200, gin.H{"tarea": tarea}, ctx)
}

func CreateComentario(ctx *gin.Context) {
	tarea := ctx.Param("ID")
	user, err := Functions.GetUser(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	original := ctx.Param("Comentario")
	var req ReqComentario
	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, err, errors.New("No se pudo leer la peticion"))
		return
	}
	comentario := Schemas.ComentarioTarea{Id: xid.New().String(),
		TareaID: tarea,
		Comentario: req.Comentario,
		Fecha: time.Now().UTC(),
		ParticipanteID: user.ID,
	}
	if req.Adjunto != "" {
		comentario.AdjuntoID = req.Adjunto
	}
	if original != "" {
		comentario.OriginalID = original
	}
	err = Querys.CreateComentario(&comentario, user.ID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	comment, _ := Querys.GetComentario(comentario.Id)
	if comment.OriginalID != "" {
		comment.Original, _ = Querys.GetComentario(comment.OriginalID)
	}
	tar, err := Querys.GetTarea(tarea, user.ID)
	go comment.EnviarCorreo(user.Nombre, user.ID, tar)
	go notifyTarea(tar, user, " ha comentado la tarea ")
	Functions.ResponseSuccess(200, gin.H{"comentario": comment}, ctx)
}

func UpdateComentario(ctx *gin.Context) {
	tarea := ctx.Param("ID")
	user, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	var req ReqComentario
	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, err, errors.New("No se pudo leer la peticion"))
		return
	}
	comentario := Schemas.ComentarioTarea{Id: ctx.Param("Comentario"),
		TareaID: tarea,
		Comentario: req.Comentario,
		Fecha: time.Now().UTC(),
		ParticipanteID: user,
	}
	if req.Adjunto != "" {
		comentario.AdjuntoID = req.Adjunto
	}
	err = Querys.UpdateComentario(&comentario, user)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	comment, _ := Querys.GetComentario(comentario.Id)
	Functions.ResponseSuccess(200, gin.H{"comentario": comment}, ctx)
}

func UpdateTareaParticipantes(ctx *gin.Context) {

}

func CambiarEstatusTarea(ctx *gin.Context) {
	id := ctx.Param("ID")
	userId, err := Functions.GetUser(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	var req struct {
		Estatus string `json:"estatus"`
	}

	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}

	if req.Estatus != Constants.TAREA_EN_PROCESO &&
		req.Estatus != Constants.TAREA_EN_ESPERA &&
		req.Estatus != Constants.TAREA_FINALIZADA {
		Functions.ResponseError(ctx, errors.New("Error al procesar la solicitud intente más tarde: "+req.Estatus))
		return
	}
	tarea, err := Querys.CambiarEstatusTarea(id, userId.ID, req.Estatus, req.Estatus == Constants.TAREA_FINALIZADA)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	go tarea.EnviarCorreo(Constants.CAMBIA_ESTATUS, userId.Nombre, userId.ID)
	go notifyTarea(tarea, userId, " ha cambiado el estatus de la tarea ")
	Functions.ResponseSuccess(204, nil, ctx)
}

func DeleteComentario(ctx *gin.Context) {
	comentario := ctx.Param("ID")
	user, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err = Querys.RemoveComentario(comentario, user)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	comment, _ := Querys.GetComentario(comentario)
	Functions.ResponseSuccess(200, gin.H{"comentario": comment}, ctx)
}
