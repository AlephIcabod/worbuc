package Proyects

import "time"

type ReqProject struct {
	Id            string   `json:"Id"`
	Nombre        string   `json:"Nombre"`
	Etiquetas     []string `json:"Etiquetas"`
	Descripcion   string   `json:"Descripcion"`
	CreadorID     string   `json:"CreadorID"`
	Libreta       int64    `json:"Libreta"`
	Participantes []string `json:"Participantes"`
	Archivos      []string `json:"Archivos"`
}

type ReqParticipante struct {
	Usuario         string `json:"usuario"`
	Email           string `json:"email"`
	Nombre          string `json:"nombre"`
	ApellidoPaterno string `json:"apellido_paterno"`
	ApellidoMaterno string `json:"apellido_materno"`
}

type ReqActividad struct {
	Nombre          string    `json:"nombre"`
	Descripcion     string    `json:"descripcion"`
	FechaProgramada time.Time `json:"fecha_programada"`
}

type ReqTarea struct {
	Nombre          string              `json:"nombre"`
	Descripcion     string              `json:"descripcion"`
	Prioridad       string              `json:"prioridad"`
	FechaProgramada time.Time           `json:"fecha_programada"`
	Participantes   []ParticipanteTarea `json:"participantes"`
	Archivos        []string            `json:"archivos"`
	Actividad       string              `json:"actividad"`
}

type ParticipanteTarea struct {
	Usuario string `json:"usuario"`
	Rol     string `json:"rol"`
}

type ReqComentario struct {
	Comentario string `json:"comentario"`
	Adjunto    string `json:"adjunto"`
}
