package Proyects

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"errors"
	"time"
)

func UpdateActividad(ctx *gin.Context) {
	idActividad := ctx.Param("ID")
	userId, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
	}
	var req ReqActividad
	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}
	actividad := Schemas.Actividad{
		Id:              idActividad,
		Nombre:          req.Nombre,
		Descripcion:     req.Descripcion,
		FechaProgramada: req.FechaProgramada,
		UpdatedAt:       time.Now(),
	}
	err = Querys.UpdateActividad(&actividad, userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"actividad": actividad}, ctx)
}

func DeleteActividad(ctx *gin.Context) {
	id := ctx.Param("ID")
	userId, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	actividad, err := Querys.GetActividad(id, userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if actividad.Tareas==nil||len(actividad.Tareas)==0{
		Functions.ResponseError(ctx,errors.New("No se puede eliminar la actividad ya que tiene asignadas tareas"))
		return
	}
	err=Querys.DeleteActividad(actividad.Id)
	if err != nil {
		Functions.ResponseError(ctx,err)
		return
	}

	Functions.ResponseSuccess(204,nil,ctx)
}

func GetActividad(ctx *gin.Context) {
	id := ctx.Param("ID")
	userId, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	actividad, err := Querys.GetActividad(id, userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"actividad": actividad}, ctx)
}
