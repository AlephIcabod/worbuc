package Proyects

import (
	"github.com/gin-gonic/gin"
	"time"
	"github.com/rs/xid"
	"errors"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"bitbucket.org/AlephIcabod/worbuc/controlers/Reuniones"
)

func GetOne(ctx *gin.Context) {
	id := ctx.Param("ID")
	userID, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err, proyecto := Querys.GetProyect(id, userID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"proyecto": proyecto}, ctx)
}

func GetParticipantes(ctx *gin.Context) {
	id := ctx.Param("ID")
	if id == "" {
		Functions.ResponseError(ctx, errors.New("Id invalido"))
		return
	}
	userId, _ := Functions.GetUserId(ctx.Request)
	res, err := Querys.GetParticipantes(id, userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"participantes": res}, ctx)
}

func Delete(ctx *gin.Context) {
	id := ctx.Param("ID")
	if id == "" {
		Functions.ResponseError(ctx, errors.New("Id invalido"))
	}
	userId, _ := Functions.GetUserId(ctx.Request)
	err := Querys.DeleteProject(id, userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(204, nil, ctx)
}

func CreateProject(ctx *gin.Context) {
	id, _ := Functions.GetUserId(ctx.Request)
	user, err := Functions.GetUser(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	var req ReqProject
	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}
	proyecto := Schemas.Proyecto{
		CreadorID:   id,
		Nombre:      req.Nombre,
		Estatus:     Constants.PROYECTO_ACTIVO,
		Descripcion: req.Descripcion,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now()}
	err = Querys.CreateProyect(&proyecto, req.Participantes, req.Libreta, req.Etiquetas, req.Archivos)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err, proy := Querys.GetProyect(proyecto.Id, id)
	go proy.EnviarCorreo(1, nil, id)

	go func() {
		notificaciones := make([]*Schemas.Notification, 0)
		for _, v := range proy.Participantes {
			if v.Usuario.Id != id {
				notificaciones = append(notificaciones, &Schemas.
				Notification{Id: xid.New().String(),
					Fecha:      time.Now().UTC(),
					Adicional:  proyecto.Id,
					EmisorID:   id,
					Tipo:       "Proyecto",
					ReceptorID: v.Usuario.Id,
					Titulo:proyecto.Nombre,
					Texto:      user.Nombre + " te ha agregado al proyecto " + proy.Nombre,
				})
			}

		}
		err = Querys.CreateNotification(notificaciones...)
	}()

	Functions.ResponseSuccess(201, gin.H{"proyect": proy}, ctx)
}

func GetProyects(ctx *gin.Context) {
	id := ctx.Param("ID")
	if _, err := xid.FromString(id); err != nil {
		Functions.ResponseError(ctx, errors.New("Id invalido"), err)
		return
	}
	libreta, etiquetas, filter, err := Functions.GetFiltros(ctx)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}

	proyectos, err := Querys.GetProyects(id, libreta, etiquetas, filter)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}

	propios := make([]Schemas.Proyecto, 0, 0)
	colaborador := make([]Schemas.Proyecto, 0, 0)
	for _, v := range proyectos {
		if v.CreadorID == id {
			propios = append(propios, v)
		} else {
			colaborador = append(colaborador, v)
		}
	}

	Functions.ResponseSuccess(200, gin.H{"propios": propios,
		"colaborador": colaborador}, ctx)
}

func UpdateProject(ctx *gin.Context) {
	pid := ctx.Param("ID")
	id, _ := Functions.GetUserId(ctx.Request)
	user, err := Functions.GetUser(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	var req ReqProject
	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}

	proyecto := Schemas.Proyecto{
		CreadorID:   id,
		Nombre:      req.Nombre,
		Estatus:     Constants.PROYECTO_ACTIVO,
		Descripcion: req.Descripcion,
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
		Id:          pid}

	err = Querys.UpdateProject(&proyecto, id, req.Libreta, req.Etiquetas, req.Participantes, req.Archivos)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err, proy := Querys.GetProyect(proyecto.Id, id)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	go func() {
		notificaciones := make([]*Schemas.Notification, 0)
		for _, v := range proy.Participantes {
			if v.Usuario.Id != id {
				notificaciones = append(notificaciones, &Schemas.
				Notification{Id: xid.New().String(),
					Fecha:      time.Now().UTC(),
					Adicional:  proyecto.Id,
					EmisorID:   id,
					Tipo:       "Proyecto",
					ReceptorID: v.Usuario.Id,
					Titulo:proyecto.Nombre,
					Texto:      user.Nombre + " ha actualizado el proyecto " + proy.Nombre,
				})
			}

		}
		err = Querys.CreateNotification(notificaciones...)
	}()
	Functions.ResponseSuccess(200, gin.H{"proyect": proy}, ctx)
}

func AddParticipante(ctx *gin.Context) {
	idProyecto := ctx.Param("ID")
	userID, err := Functions.GetUserId(ctx.Request)
	user, err := Functions.GetUser(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	var req ReqParticipante
	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo decodificar peticion"), err)
		return
	}

	err, proyecto := Querys.GetProyect(idProyecto, userID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if req.Usuario == "" {
		if req.Email == "" {
			Functions.ResponseError(ctx, errors.New("Indique un email válido o seleccione un participante del proyecto"))
			return
		}
		usuario, err := Querys.GetUserByEmail(req.Email)
		if err != nil && err.Error() != Constants.ErrorNoResultsUser {
			Functions.ResponseError(ctx, err)
			return
		}
		if usuario == nil {
			usuario = &Schemas.Usuario{Nombre: req.Nombre,
				ApellidoPaterno: req.ApellidoPaterno,
				ApellidoMaterno: req.ApellidoMaterno,
				Email:           req.Email,
			}
			err = Querys.Create(usuario)
			if err != nil {
				Functions.ResponseError(ctx, err)
				return
			}
		}
		req.Usuario = usuario.Id
	}

	Querys.AddContact(userID, req.Usuario)
	err, participante := Querys.AddParticipante(idProyecto, userID, req.Usuario)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	participante.Usuario, _ = Querys.GetUser(req.Usuario)
	go proyecto.EnviarCorreo(1, participante, userID)
	notification := &Schemas.Notification{
		ReceptorID: participante.Usuario.Id,
		Id:         xid.New().String(),
		Texto:      user.Nombre + " Te ha agregado al proyecto " + proyecto.Nombre,
		EmisorID:   user.ID,
		Adicional:  proyecto.Id,
		Titulo:  proyecto.Nombre,
		Tipo:       "Proyecto",
		Fecha:      time.Now(),
	}
	Querys.CreateNotification(notification)
	Functions.ResponseSuccess(200, gin.H{"participante": participante}, ctx)
}

func CreateActividad(ctx *gin.Context) {
	idProyecto := ctx.Param("ID")
	userId, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
	}
	var req ReqActividad
	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}
	nueva := Schemas.Actividad{ProyectoID: idProyecto,
		Estatus:         Constants.ACTIVIDAD_EN_PROCESO,
		Id:              xid.New().String(),
		FechaCreacion:   time.Now().UTC(),
		Nombre:          req.Nombre,
		Descripcion:     req.Descripcion,
		FechaProgramada: req.FechaProgramada,
	}
	err = Querys.CreateActividad(idProyecto, userId,
		&nueva)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"actividad": nueva}, ctx)
}

func GetEtiquetas(ctx *gin.Context) {
	user, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	etiquetas, err := Querys.GetEtiquetas(user, "proyectos")
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"etiquetas": etiquetas}, ctx)
}

func ToggleFavorito(ctx *gin.Context) {
	proyecto := ctx.Param("ID")
	userID, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}

	err = Querys.ToggleFavorito(proyecto, userID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, nil, ctx)
}

func Clasificar(ctx *gin.Context) {
	proyecto := ctx.Param("ID")
	userID, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	var req Reuniones.ReqClasificar
	err = ctx.BindJSON(&req)
	err = Querys.ClasificarProyecto(userID, proyecto, req.Libreta, req.Etiquetas)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, nil, ctx)
}

func RemoveParticipante(ctx *gin.Context) {
	proyecto := ctx.Param("ID")
	participante := ctx.Param("Participante")
	userId, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err, proyect := Querys.GetProyect(proyecto, userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err, particip := Querys.RemoveParticipanteProyect(proyecto, participante, userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	go proyect.EnviarCorreo(2, particip, userId)
	Functions.ResponseSuccess(204, nil, ctx)
}

func ChangeEstatusProject(ctx *gin.Context) {
	id := ctx.Param("ID")
	userId, err := Functions.GetUser(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	var req struct {
		Estatus string `json:"estatus"`
	}

	err = ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err, proy := Querys.GetProyect(id, userId.ID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	if req.Estatus == Constants.PROYECTO_ELIMINADO {
		if proy.CreadorID != userId.ID {
			Functions.ResponseError(ctx, errors.New("No tiene permiso para esta accion"))
			return
		}
		err = Querys.DeleteProject(id, userId.ID)
		if err != nil {
			Functions.ResponseError(ctx, err)
			return
		}
		Functions.ResponseSuccess(204, nil, ctx)

		go proy.EnviarCorreo(3, nil, id)

		return
	}

	if req.Estatus != Constants.PROYECTO_ACTIVO &&
		req.Estatus != Constants.PROYECTO_FINALIZADO &&
		req.Estatus != Constants.PROYECTO_PAUSADO {
		Functions.ResponseError(ctx, errors.New("Error al procesar la solicitud intente más tarde: "+req.Estatus))
		return
	}
	err = Querys.CambiarEstatusProject(id, userId.ID, req.Estatus)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(204, nil, ctx)
}
