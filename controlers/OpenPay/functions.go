package OpenPay

import (
	"net/http"
	"time"
	"bytes"
	"io/ioutil"
	"encoding/json"

	"errors"
	"bitbucket.org/AlephIcabod/worbuc/config"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"strconv"
	"github.com/rs/xid"
)

func RealizarCobro(data []byte) (error) {
	resp_body, err := RequestOpenPay(data, "/charges", "POST")
	var resp RespuestaExito
	err = json.Unmarshal(resp_body, &resp)
	if err != nil {

		return nil
	}
	return nil
}

func RequestOpenPay(data []byte, path, method string) ([]byte, error) {
	var netClient = &http.Client{
		Timeout: time.Second * 60,
	}
	url := config.GetConfig().OpenpaySettings.URL +
		config.GetConfig().OpenpaySettings.MerchantID + path
	req, err := http.NewRequest(method, url, bytes.NewBuffer(data))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")
	req.SetBasicAuth(config.GetConfig().OpenpaySettings.Private_Key, "")

	response, err := netClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	resp_body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	if response.StatusCode >= 400 {
		var resp RespuestaError
		err = json.Unmarshal(resp_body, &resp)
		if err != nil {
			return nil, errors.New("Error al procesar pago, no se pudo procesar")
		}
		return nil, errors.New(resp.Description)
	}

	return resp_body, nil
}

func CrearCuentaCliente(u *Schemas.Usuario) (id string, err error) {
	peticion := ClienteReq{
		Email:      u.Email,
		ExternalId: &u.Id,
		LastName:   u.ApellidoPaterno,
		Name:       u.Nombre,
	}
	data, err := json.Marshal(peticion)
	if err != nil {
		return
	}
	data, err = RequestOpenPay(data, "/customers", "POST")
	if err != nil {
		return
	}
	respuesta := struct {
		Id string `json:"id"`
	}{Id: ""}

	err = json.Unmarshal(data, &respuesta)
	if err != nil {
		return
	}
	return respuesta.Id, nil
}

func (p *PlanOpenPay) CrearPlan() (id string, err error) {
	data, err := json.Marshal(p)
	if err != nil {
		return
	}
	data, err = RequestOpenPay(data, "/plans", "POST")
	if err != nil {
		return
	}
	respuesta := struct {
		Id string `json:"id"`
	}{Id: ""}

	err = json.Unmarshal(data, &respuesta)
	if err != nil {
		return
	}
	return respuesta.Id, nil
}

func (p *PlanOpenPay) UpdatePlan() (id string, err error) {
	data, err := json.Marshal(p)
	if err != nil {
		return
	}
	data, err = RequestOpenPay(data, "/plans/"+p.Id, "PUT")
	if err != nil {
		return
	}
	respuesta := struct {
		Id string `json:"id"`
	}{Id: ""}

	err = json.Unmarshal(data, &respuesta)
	if err != nil {
		return
	}
	return respuesta.Id, nil
}

func crearTarjeta(device, token, cliente string) (id string, err error) {
	p := struct {
		TokenID         string `json:"token_id"`
		DeviceSessionID string `json:"device_session_id"`
	}{TokenID: token, DeviceSessionID: device}

	data, err := json.Marshal(p)
	if err != nil {
		return
	}
	data, err = RequestOpenPay(data, "/customers/"+cliente+"/cards", "POST")
	if err != nil {
		return
	}
	respuesta := struct {
		Id string `json:"id"`
	}{Id: ""}

	err = json.Unmarshal(data, &respuesta)
	if err != nil {
		return
	}
	return respuesta.Id, nil
}

func RenovarSuscripcion(plan *Schemas.Plan, tiempo float64,
	user *Schemas.Usuario, tarjeta string, deviceID, token *string) error {
	peticion := CargoReq{
		Amount:      plan.Precio * tiempo,
		Currency:    "MXN",
		Description: "Cobro por " + plan.Descripcion + " por " + strconv.FormatFloat(tiempo, 'f', 2, 64) + " meses",
		OrderId:     xid.New().String(),
		Method:      "card",
		Customer: ClienteReq{Name: user.Nombre, Email: user.Email,
			LastName:   user.ApellidoPaterno + " " + user.ApellidoMaterno,
			ExternalId: &user.Id,
		},
	}

	if tarjeta != "" {
		peticion.SourceId = &tarjeta
	} else {
		peticion.DeviceSessionId = deviceID
		peticion.SourceId = token
	}
	data, err := json.Marshal(peticion)
	if err != nil {
		return errors.New("error al iniciar la transacción")
	}

	err = RealizarCobro(data)
	if err != nil {
		return err
	}
	err = Querys.ChangePlan(user.Id, &tarjeta, plan.Id, int(tiempo*30))
	if err != nil {
		return err
	}
	return nil
}

func ObtenerCliente(usuario string) (string, error) {
	var netClient = &http.Client{
		Timeout: time.Second * 60,
	}
	url := config.GetConfig().OpenpaySettings.URL +
		config.GetConfig().OpenpaySettings.MerchantID + "/customers?external_id=" + usuario
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}
	req.Header.Add("Content-Type", "application/json")
	req.SetBasicAuth(config.GetConfig().OpenpaySettings.Private_Key, "")

	response, err := netClient.Do(req)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()
	resp_body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	if response.StatusCode >= 400 {
		var resp RespuestaError
		err = json.Unmarshal(resp_body, &resp)
		if err != nil {
			return "", errors.New("Error al obtener cliente")
		}
		return "", errors.New(resp.Description)
	}
	respuesta := make([]struct{ Id string `json:"id"` }, 0)

	err = json.Unmarshal(resp_body, &respuesta)
	if err != nil {
		return "", err
	}
	if len(respuesta) > 0 {
		return respuesta[0].Id, nil
	}
	return "", errors.New("Cliente no encontrado")
}
