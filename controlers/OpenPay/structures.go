package OpenPay

import "time"

type ClienteReq struct {
	ExternalId      *string `json:"external_id"`
	Name            string  `json:"name"`
	LastName        string  `json:"last_name"`
	Email           string  `json:"email"`
	RequiresAccount bool    `json:"requires_account"`
}

type CargoReq struct {
	Method          string     `json:"method"`
	SourceId        *string     `json:"source_id"`
	Amount          float64    `json:"amount"`
	Currency        string     `json:"currency"`
	Description     string     `json:"description"`
	OrderId         string     `json:"order_id"`
	DeviceSessionId *string     `json:"device_session_id"`

	Customer        ClienteReq `json:"customer"`
}

type ReqCobro struct {
	DeviceID string `json:"device_id"`
	Token    string `json:"token"`
	Plan     string `json:"plan"`
	Tiempo   int    `json:"tiempo"`
}

type RespuestaError struct {
	Description string `json:"description"`
	ErrorCode   int64  `json:"error_code"`
}

type RespuestaExito struct {
	Id            string `json:"id"`
	Status        string `json:"status"`
	Authorization string `json:"authorization"`
	OrderId       string `json:"order_id"`
}

type PlanOpenPay struct {
	Name             string    `json:"name"`
	Status           string    `json:"status"`
	Amount           float64   `json:"amount"`
	Currency         string    `json:"currency"`
	Id               string    `json:"id"`
	CreationDate     time.Time `json:"creation_date"`
	RepeatEvery      int       `json:"repeat_every"`
	RepeatUnit       string    `json:"repeat_unit"`
	RetryTimes       int       `json:"retry_times"`
	StatusAfterRetry string    `json:"status_after_retry"`
	TrialDays        int       `json:"trial_days"`
}
