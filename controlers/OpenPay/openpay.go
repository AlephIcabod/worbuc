package OpenPay

import (
	"github.com/gin-gonic/gin"
	"fmt"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"errors"
)

func ProcesarPago(ctx *gin.Context) {
	var req ReqCobro
	err := ctx.BindJSON(&req)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}
	plan, err := Querys.GetPlan(req.Plan)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se encontro el paquete"), err)
		return
	}
	if plan.Estatus != Constants.ACTIVO {
		Functions.ResponseError(ctx, errors.New("Ese plan ya no se encuentra disponible"))
		return
	}
	userId, err := Functions.GetUserId(ctx.Request)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	user, err := Querys.GetUser(userId)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	tarjeta := ""
	if user.ClienteId != nil {
		tarjeta, err = crearTarjeta(req.DeviceID, req.Token, *user.ClienteId)
		fmt.Println(err)
	}
	err = RenovarSuscripcion(plan, 1, user, tarjeta, &req.DeviceID, &req.Token)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	planU, _, err := Querys.GetUserPlan(userId)
	token, err := Functions.CreateTokenString(user.Id, user.Nombre+" "+user.ApellidoPaterno+" "+user.ApellidoMaterno, user.Email, user.TipoPerfil, user.Estatus, *planU.Vigencia, user.IsAdmin)

	Functions.ResponseSuccess(200, gin.H{"estado": "Ok", "token": token}, ctx)
}
