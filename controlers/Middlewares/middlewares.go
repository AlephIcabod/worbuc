package Middlewares

import (
	"github.com/gin-gonic/gin"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"time"
	"net/http"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"strings"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"github.com/kataras/iris/core/errors"
)

func Auth(ctx *gin.Context) {
	valido, err := Functions.ValidaToken(ctx.Request)
	if !valido || err != nil {
		Functions.Response(401, "No hay cabecera de autenticacion", nil, ctx)
		ctx.Request.Close = true
		return
	} else {
		ctx.Next()
	}
}

func Admin(ctx *gin.Context) {
	user, err := Functions.GetUser(ctx.Request)
	if err != nil {
		ctx.Request.Close = true
		Functions.Response(401, err.Error(), nil, ctx)
		return
	}
	if user.IsAdmin {
		ctx.Next()
	} else {
		Functions.Response(401, "Sin permiso", nil, ctx)
		return
	}
}

func Vigente(ctx *gin.Context) {
	userId, err := Functions.GetUser(ctx.Request)
	if err != nil {
		Functions.Response(403, "No se pudo obtener usuario", nil, ctx)
		ctx.Request.Close = true
		return
	}
	if userId.Vigencia.UTC().After(time.Now().UTC()) || userId.IsAdmin {
		ctx.Next()
	} else {
		Functions.Response(http.StatusLocked, "Su suscripcion ha caducado", nil, ctx)
		ctx.Request.Close = true
	}
}

func PermisoApp(ctx *gin.Context) {
	userId, err := Functions.GetUser(ctx.Request)
	if err != nil {
		Functions.Response(403, "No se pudo obtener usuario", nil, ctx)
		ctx.Request.Close = true
		return
	}
	p, _, err := Querys.GetUserPlan(userId.ID)
	if err != nil {
		Functions.ResponseError(ctx, err)
		ctx.Request.Close = true
		return
	}
	uri := ctx.Request.RequestURI
	switch {
	case strings.Index(uri, "tareas") >= 0 || strings.Index(uri, "proyects") >= 0 ||
		strings.Index(uri, "actividades") >= 0:
		for _, v := range p.Plan.Apps {
			if v == Constants.Aplicaciones["Proyectos"] {
				ctx.Next()
				return
			}
		}
		Functions.ResponseError(ctx, errors.New("No tienes permiso para esta aplicación, suscribete a un plan diferente"))
		ctx.Request.Close = true
		return
	case strings.Index(uri, "reuniones") >= 0:
		for _, v := range p.Plan.Apps {
			if v == Constants.Aplicaciones["Reuniones"] {
				ctx.Next()
				return
			}
		}
		Functions.ResponseError(ctx, errors.New("No tienes permiso para esta aplicación, suscribete a un plan diferente"))
		ctx.Request.Close = true
		return
	}
	ctx.Next()
}
