package Admin

import (
	"github.com/gin-gonic/gin"
	"errors"
	"strconv"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/models/Querys"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/config"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"github.com/rs/xid"
)

func GetEmailSettings(ctx *gin.Context) {
	email := config.GetConfig().EmailSettings
	Functions.ResponseSuccess(200, gin.H{"email": email,
		"dias_prueba": config.GetConfig().DiasPrueba,
		"plan_prueba": config.GetConfig().PlanPrueba,
	}, ctx)
}

func UpdateEmailSettings(ctx *gin.Context) {
	var email config.EmailSettings
	err := ctx.BindJSON(&email)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}
	err = config.UpdateEmailFileConfig(&email)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(201, nil, ctx)
}

func ChangePrueba(ctx *gin.Context) {
	var Config = struct {
		DiasPrueba int
		PlanPrueba string
	}{}
	err := ctx.BindJSON(&Config)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}
	err = config.UpdatePlanPrueba(Config.PlanPrueba, Config.DiasPrueba)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(201, nil, ctx)
}

func GetPlanes(ctx *gin.Context) {
	activosOnly, err := strconv.ParseBool(ctx.DefaultQuery("all", "false"))
	planes, err := Querys.GetAllPlans(activosOnly)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"planes": planes}, ctx)
}

func CreatePlan(ctx *gin.Context) {
	var p Schemas.Plan
	err := ctx.BindJSON(&p)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	p.Id=xid.New().String()
	err = Querys.CreatePlan(p.Precio, p.LimiteMb, p.Nombre, p.Descripcion, p.Id, p.Apps)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	p.Estatus = Constants.ACTIVO
	Functions.ResponseSuccess(200, gin.H{"plan": p}, ctx)
}

func UpdatePlan(ctx *gin.Context) {
	var p Schemas.Plan

	err := ctx.BindJSON(&p)
	if err != nil {
		Functions.ResponseError(ctx, errors.New("No se pudo leer la peticion"), err)
		return
	}
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	err = Querys.UpdatePlan(&p)
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"plan": p}, ctx)
}

func DeletePlan(ctx *gin.Context) {
	id := ctx.Param("ID")
	estatus, err := Querys.CambiarEstatusPlan(id)
	// Actualizar en openpay
	if err != nil {
		Functions.ResponseError(ctx, err)
		return
	}
	Functions.ResponseSuccess(200, gin.H{"estatus": estatus}, ctx)
}
