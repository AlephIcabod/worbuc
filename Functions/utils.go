package Functions

import (
	"database/sql"
	"time"
	"github.com/gin-gonic/gin"
	"github.com/dgrijalva/jwt-go"
	"strconv"
	"github.com/kataras/iris/core/errors"
	"strings"
)

func TernaryOperator(condition bool, trueValue, falseValue interface{}) (interface{}) {
	if condition {
		return trueValue
	}
	return falseValue
}

func Response(status int, message string, data map[string]interface{}, ctx *gin.Context) {
	ctx.JSON(status, gin.H{"message": message, "data": data})
	ctx.Abort()
}
func ResponseError(ctx *gin.Context, data ...error) {
	errors := make([]string, 0, len(data))
	for _, v := range data {
		errors = append(errors, v.Error())
	}
	Response(400, "Error en la solicitud", gin.H{"errors": errors}, ctx)
}

func ResponseSuccess(code int, data map[string]interface{}, ctx *gin.Context) {
	Response(code, "ok", data, ctx)
}

func RollbackByError(err error, tx *sql.Tx) error {
	if err != nil {
		if tx != nil {
			tx.Rollback()
		}
	}
	return err
}

func GenerarTokenUnUso(id string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := UsuarioLogeado{}
	claims.ExpiresAt = time.Now().Add(time.Minute * time.Duration(5)).Unix()
	claims.IssuedAt = time.Now().Unix()
	claims.ID = id
	token.Claims = claims
	tokenString, err := token.SignedString(key)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func ValidaTokenUrl(tk string) (string, bool) {
	var usuario UsuarioLogeado
	token, err := jwt.ParseWithClaims(tk, &usuario, func(token *jwt.Token) (interface{}, error) { return key, nil })
	if err != nil {
		return "", false
	}

	return usuario.ID, token.Valid

}

func GetFiltros(ctx *gin.Context) (int64, []string, string, error) {
	libreta, err := strconv.ParseInt(ctx.DefaultQuery("libreta", "0"), 10, 64)
	if err != nil {
		return -1, nil, "", errors.New("Libreta no válida")
	}
	tags := ctx.DefaultQuery("tags", "")
	etiquetas := strings.Split(tags, ",")
	filter := ctx.DefaultQuery("filter", "")
	return libreta, etiquetas, filter, nil
}

func GetLimitPage(ctx *gin.Context) (limit, page int64) {
	limit, err := strconv.ParseInt(ctx.DefaultQuery("limit", "50"), 10, 64)
	if err != nil || limit <= 0 {
		limit = 50
	}
	if limit > 100 {
		limit = 100
	}
	page, err = strconv.ParseInt(ctx.DefaultQuery("page", "1"), 10, 64)
	if err != nil || page == 0 {
		page = 1
	}
	return
}
