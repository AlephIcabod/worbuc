package Functions

import (
	"github.com/jasonlvhit/gocron"
)

func DoTask(frecuencia uint64, tipo int, Task interface{}, Params ...interface{}) {
	tarea := gocron.NewScheduler()
	switch tipo {
	case 0:
		if len(Params) > 0 {
			tarea.Every(frecuencia).Minutes().Do(Task, Params)
		} else {
			tarea.Every(frecuencia).Minutes().Do(Task)
		}
	case 1:
		if len(Params) > 0 {
			tarea.Every(frecuencia).Seconds().Do(Task, Params)
		} else {
			tarea.Every(frecuencia).Seconds().Do(Task)
		}
	case 2:
		if len(Params) > 0 {
			tarea.Every(frecuencia).Hours().Do(Task, Params)
		} else {
			tarea.Every(frecuencia).Hours().Do(Task)
		}
	case 3:
		if len(Params) > 0 {
			tarea.Every(frecuencia).Days().Do(Task, Params)
		} else {
			tarea.Every(frecuencia).Days().Do(Task)
		}
	case 4:
		if len(Params) > 0 {
			tarea.Every(frecuencia).Weeks().Do(Task, Params)
		} else {
			tarea.Every(frecuencia).Weeks().Do(Task)
		}
	case 5:
		if len(Params) > 0 {
			tarea.Every(frecuencia).Monday().Do(Task, Params)
		} else {
			tarea.Every(frecuencia).Monday().Do(Task)
		}
	case 6:
		if len(Params) > 0 {
			tarea.Every(frecuencia).Tuesday().Do(Task, Params)
		} else {
			tarea.Every(frecuencia).Tuesday().Do(Task)
		}
	case 7:
		if len(Params) > 0 {
			tarea.Every(frecuencia).Wednesday().Do(Task, Params)
		} else {
			tarea.Every(frecuencia).Wednesday().Do(Task)
		}
	case 8:
		if len(Params) > 0 {
			tarea.Every(frecuencia).Thursday().Do(Task, Params)
		} else {
			tarea.Every(frecuencia).Thursday().Do(Task)
		}
	case 9:
		if len(Params) > 0 {
			tarea.Every(frecuencia).Friday().Do(Task, Params)
		} else {
			tarea.Every(frecuencia).Friday().Do(Task)
		}
	case 10:
		if len(Params) > 0 {
			tarea.Every(frecuencia).Saturday().Do(Task, Params)
		} else {
			tarea.Every(frecuencia).Saturday().Do(Task)
		}
	case 11:
		if len(Params) > 0 {
			tarea.Every(frecuencia).Sunday().Do(Task, Params)
		} else {
			tarea.Every(frecuencia).Sunday().Do(Task)
		}
	default:
		if len(Params) > 0 {
			tarea.Every(frecuencia).Minutes().Do(Task, Params)
		} else {
			tarea.Every(frecuencia).Minutes().Do(Task)
		}
	}

	<-tarea.Start()
}

func DoTaskAt(frecuencia uint64, hora string, Task interface{}, Params ...interface{}) {
	tarea := gocron.NewScheduler()

	if len(Params) > 0 {
		tarea.Every(frecuencia).Day().At(hora).Do(Task, Params)
	} else {
		tarea.Every(frecuencia).Day().At(hora).Do(Task)
	}

	<-tarea.Start()
}
