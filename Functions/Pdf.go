package Functions

import (
	"github.com/jung-kurt/gofpdf"

	"strconv"
	"strings"
	"fmt"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
)

var (
	r  = 156
	g  = 39
	b  = 176
	ml = 12.0
	mr = 12.0
	mt = 10.0
	ft = 15.0
	fz = 11.0
	fs = 12.0
	cw = 255
	cb = 0
)

func ReunionPdf(reunion *Schemas.Reunion) (*gofpdf.Fpdf, error) {
	pdf := gofpdf.New("P", "mm", "Letter", "")
	pdf.AliasNbPages("")
	pdf.AddPage()
	pdf.SetMargins(ml, mt, mr)
	pdf.SetFont("Arial", "b", ft)
	w, h := pdf.GetPageSize()
	pdf.Image("./public/imagenes/LogoVertical.png", ml, mt, 0, 15, false,
		"", 0, "")
	tr := pdf.UnicodeTranslatorFromDescriptor("")
	title := "[" + reunion.Id[0:5] + "]" + " " + reunion.Titulo
	pdf.SetX(40)
	pdf.SetFillColor(r, g, b)
	pdf.SetTextColor(cw, cw, cw)
	pdf.CellFormat((w-ml)*.8, ft, tr(title),
		"", 2, "C", true, 0, "")

	pdf.SetFont("Arial", "", ft)
	pdf.SetTextColor(cb, cb, cb)
	pdf.SetY(25)
	xi := 40.0
	AppendTextPair("Objetivo de la reunión:", tr(reunion.Objetivo), xi, pdf)
	fecha := reunion.Fecha.Format("02-01-2006") + " / " + reunion.HoraInicio.Format("15:04") + " - " + reunion.HoraTermino.Format("15:04")
	AppendTextPair("Fecha: ", tr(fecha), xi, pdf)
	direccion := strings.Split(reunion.Lugar, "COORDS=")[0]
	AppendTextPair("Lugar: ", direccion, xi, pdf)
	//asistentes := ""
	archivos := ""
	for _, v := range reunion.Archivos {
		archivos += v.Name + ","
	}
	pdf.SetX(xi)
	pdf.SetFont("Arial", "B", fz)
	pdf.CellFormat(w*.20, 8, tr("Asistentes: "), "", 0, "R", false, 0, "")
	pdf.SetFont("Arial", "", fz)
	pdf.SetX(xi + w*.20)
	for _, v := range reunion.Participantes {
		if v.Asistencia {
			pdf.SetTextColor(0, 0, 0)
			pdf.Write(8, v.Usuario.Nombre+" "+v.Usuario.ApellidoPaterno+"("+v.Alias+"),")
		} else {
			pdf.SetTextColor(255, 0, 0)
			pdf.Write(8, v.Usuario.Nombre+" "+v.Usuario.ApellidoPaterno+"("+v.Alias+"),")
		}
	}
	pdf.SetTextColor(0, 0, 0)
	//AppendTextPair("Asistentes: ", asistentes, xi, pdf)
	pdf.SetY(pdf.GetY() + 8)
	if archivos != "" {
		AppendTextPair("Archivos Adjuntos:", archivos, xi, pdf)
	}
	pdf.SetY(pdf.GetY() + 10)
	pdf.Line(ml, pdf.GetY(), w-mr, pdf.GetY())

	pdf.SetFooterFunc(func() {
		pdf.SetFillColor(r, g, b)
		pdf.Rect(0, h-10, w, 10, "F")
		pdf.SetTextColor(cw, cw, cw)
		pdf.Text(w*.7, h-5, tr(fmt.Sprintf("Página %d de {nb}", pdf.PageNo())))
	})

	for _, v := range reunion.Minuta {
		for _, w := range reunion.Participantes {
			if w.UsuarioID == v.ResponsableID {
				v.ResponsableID = w.Alias
				break
			}
		}
		AddTema(pdf, w, v)
	}
	return pdf, nil

}

func AppendTextPair(text1, text2 string, xi float64, pdf *gofpdf.Fpdf) {
	tr := pdf.UnicodeTranslatorFromDescriptor("")
	w, _ := pdf.GetPageSize()
	pdf.SetX(xi)
	pdf.SetFont("Arial", "B", fz)
	pdf.CellFormat(w*.20, 8, tr(text1), "", 0, "R", false, 0, "")
	pdf.SetFont("Arial", "", fz)
	pdf.SetX(xi + w*.20)
	pdf.MultiCell(w*.80-xi-mr, 8, tr(text2), "", "", false)
}

func AddTema(pdf *gofpdf.Fpdf, w float64, minuta Schemas.Minuta) {
	tr := pdf.UnicodeTranslatorFromDescriptor("")
	pdf.SetX(ml)
	title := strconv.Itoa(minuta.NumeroOrden) + ". " + minuta.Tema + "- (" + minuta.ResponsableID + ")"
	pdf.SetFont("Arial", "i", fs)
	pdf.CellFormat(0, fs, tr(title),
		"", 2, "LM", false, 0, "")
	pdf.SetFont("Arial", "", fz)
	pdf.SetDrawColor(r, g, b)
	pdf.SetY(pdf.GetY() + 1)
	pdf.Line(ml, pdf.GetY(), w-mr, pdf.GetY())
	pdf.SetTextColor(0, 0, 0)
	html := pdf.HTMLBasicNew()
	pdf.SetX(ml)
	pdf.SetY(pdf.GetY() + 3)
	minuta.Descripcion = strings.Replace(minuta.Descripcion, "&nbsp;", " ", -1)
	minuta.Descripcion = strings.Replace(minuta.Descripcion, "<li>", "\n", -1)
	minuta.Descripcion = strings.Replace(minuta.Descripcion, "</li>", "\n", -1)
	html.Write(fz-1, tr(minuta.Descripcion))
	pdf.SetY(pdf.GetY() + 4)
}
