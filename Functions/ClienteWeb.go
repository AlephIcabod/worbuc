package Functions

import (
	"net/http"
	"time"
	"bytes"
	"io/ioutil"
	"net/url"
	"strings"
)

func Post(URL string, data []byte) ([]byte, error) {
	var netClient = &http.Client{
		Timeout: time.Second * 60,
	}
	req, err := http.NewRequest("POST", URL, bytes.NewBuffer(data))
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", "application/json")
	
	response, err := netClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	resp_body, err := ioutil.ReadAll(response.Body)
	
	if err != nil {
		return nil, err
	}
	return resp_body, nil
}

func PostForm(URL string, Data url.Values, ) ([]byte, error) {
	var netClient = &http.Client{}
	response, err := netClient.Post(URL, "application/x-www-form-urlencoded", strings.NewReader(Data.Encode()))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	
	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	
	return responseData, nil
}
