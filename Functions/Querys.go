package Functions

import (
	"github.com/gin-gonic/gin"
	"strconv"
)

func GetPagination(ctx *gin.Context) (limit, page int64) {
	limit, err := strconv.ParseInt(ctx.DefaultQuery("limit", "100"), 10, 64)
	if err != nil {
		limit = 100
	}
	page, err = strconv.ParseInt(ctx.DefaultQuery("page", "1"), 10, 64)
	if err != nil {
		page = 1
	}
	return
}
