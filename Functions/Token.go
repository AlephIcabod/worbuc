package Functions

import (
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"time"
	"strings"
	"errors"
)

type UsuarioLogeado struct {
	ID         string `json:"id"`
	Nombre     string `json:"nombre"`
	Email      string `json:"email"`
	TipoPerfil string `json:"tipoPerfil"`
	Estatus    string `json:"estatus"`
	IsAdmin    bool   `json:"isAdmin"`
	Vigencia time.Time `json:"vigencia"`
	jwt.StandardClaims
}

var key = []byte("secret")

//CreateTokenString función que crea el token de tipo string a partir de los datos de un usuario.
func CreateTokenString(id, nombre, email, tipoPerfil, estatus string,vigencia time.Time, admin bool) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := UsuarioLogeado{}
	claims.ExpiresAt = time.Now().Add(time.Minute * time.Duration(60)).Unix()
	claims.IssuedAt = time.Now().Unix()
	claims.ID = id
	claims.Nombre = nombre
	claims.TipoPerfil = tipoPerfil
	claims.Estatus = estatus
	claims.Email = email
	claims.IsAdmin = admin
	claims.Vigencia=vigencia
	token.Claims = claims
	tokenString, err := token.SignedString(key)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func decodeToken(r *http.Request) (*UsuarioLogeado, error) {
	header := r.Header.Get("Authorization")
	if header == "" {
		return nil, errors.New("No hay cabecera de autenticacion")
	}
	if len(strings.Split(header, " ")) <= 0 {
		return nil, errors.New("No hay cabecera de autenticacion")
	}
	token := strings.Split(header, " ")[1]
	return DecodeToken(token)
}

//ValidaToken un token para su vigencia
func ValidaToken(r *http.Request) (bool, error) {
	header := r.Header.Get("Authorization")
	if header == "" {
		return false, errors.New("No hay cabecera  Authorization")
	}
	tokens := strings.Split(header, " ")[1]
	var usuario UsuarioLogeado
	token, err := jwt.ParseWithClaims(tokens, &usuario, func(token *jwt.Token) (interface{}, error) { return key, nil })
	if err != nil {
		return false, err
	}
	if usuario.ExpiresAt < time.Now().Unix() {
		return false, err
	}

	return token.Valid, nil
}

//GetUserName regresa el nombre del usuario logueado

func RefreshToken(token string) (string, error) {
	var usuario UsuarioLogeado
	_, err := jwt.ParseWithClaims(token, &usuario,
		(func(token *jwt.Token) (interface{}, error) { return key, nil }))
	if err != nil {
		return "", err
	}
	nuevoToken, err := CreateTokenString(usuario.ID, usuario.Nombre, usuario.Email, usuario.TipoPerfil, usuario.Estatus, usuario.Vigencia,usuario.IsAdmin)
	if err != nil {
		return "", err
	}
	return nuevoToken, nil
}

func GetUserId(r *http.Request) (string, error) {
	user, err := decodeToken(r)
	if err != nil {
		return "", err
	}
	return user.ID, nil
}

func GetUser(r *http.Request) (*UsuarioLogeado, error) {
	return decodeToken(r)
}

func DecodeToken(token string) (*UsuarioLogeado, error) {
	var usuario UsuarioLogeado
	_, err := jwt.ParseWithClaims(token, &usuario, (func(token *jwt.Token) (interface{}, error) { return key, nil }))
	if err != nil {
		return nil, err
	}
	return &usuario, nil
}
