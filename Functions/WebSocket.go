package Functions

import (
	melody2 "gopkg.in/olahol/melody.v1"
	"encoding/json"
	"fmt"
)

type MensajeWebSocket struct {
	Tipo    string
	Content interface{}
	Emisor  string
}

var Sesiones = map[string]int{
	"mobile":0,
	"tablet":0,
	"desktop":0,
}

var melody *melody2.Melody

func init() {
	if melody == nil {
		melody = melody2.New()
	}
}

func GetMelody() *melody2.Melody {
	if melody == nil {
		melody = melody2.New()
	}
	return melody
}

func SendMesage(tipo string, content interface{}, emisor string, reunionF ...string) {

	bytes, _ := json.Marshal(MensajeWebSocket{tipo, content, emisor})
	if reunionF != nil && len(reunionF) > 0 {
		melody.BroadcastFilter(bytes, func(session *melody2.Session) bool {
			reunion, exist := session.Get("reunion")
			if exist {
				return reunion == reunionF[0]
			}
			return false
		})
	} else {
		melody.Broadcast(bytes)
	}
}

func NotifyUser(content interface{}, receptor string) {
	bytes, _ := json.Marshal(MensajeWebSocket{"NOTIFICACION", content, ""})
	fmt.Println("Notifica")
	melody.BroadcastFilter(bytes, func(session *melody2.Session) bool {
		emisor, _ := session.Get("payload")
		id := emisor.(UsuarioLogeado).ID
		fmt.Println(id, receptor)
		return id == receptor
	})
}

func NotifyAdminConnects() {
	bytes, _ := json.Marshal(MensajeWebSocket{"ADMIN_CONNECT", Sesiones, ""})
	melody.BroadcastFilter(bytes, func(session *melody2.Session) bool {
		emisor, _ := session.Get("payload")
		user := emisor.(UsuarioLogeado)
		return user.IsAdmin
	})
}
