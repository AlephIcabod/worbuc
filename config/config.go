package config

import (
	"io/ioutil"
	"encoding/json"
	"fmt"
	"os"
)

var GlobalConfig Config

type Config struct {
	ServerAddress   string          `json:"server_address"`
	Database        configDatabase  `json:"database"`
	EmailSettings   EmailSettings   `json:"email_settings"`
	OpenpaySettings OpenpaySettings `json:"openpay_settings"`
	TlsSettings     TlsSettings     `json:"tls"`
	DiasPrueba      int             `json:"dias_prueba"`
	PlanPrueba      string             `json:"plan_prueba"`
}

type TlsSettings struct {
	Cert string `json:"cert"`
	Key  string `json:"key"`
}

type configDatabase struct {
	Host     string `json:"host"`
	Port     string `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Dbname   string `json:"dbname"`
}
type EmailSettings struct {
	Nombre string  `json:"nombre"`
	Correo Account `json:"correo"`
}

type OpenpaySettings struct {
	URL         string `json:"url"`
	MerchantID  string `json:"merchant_id"`
	Private_Key string `json:"private_key"`
}

type Account struct {
	Usuario  string `json:"usuario"`
	Password string `json:"password"`
	Servidor string `json:"servidor"`
	Puerto   string `json:"puerto"`
}

func GetConfig() *Config {
	return &GlobalConfig
}

func InitConfig() error {
	bytes, err := ioutil.ReadFile("config.json")
	if err != nil {
		panic("Error de inicializacion " + err.Error())
		return err
	}
	err = json.Unmarshal(bytes, &GlobalConfig)
	if err != nil {
		panic("Error de inicializacion " + err.Error())
		return err
	}
	fmt.Println("Cargada configuracion inicial", GlobalConfig)
	return nil
}

func UpdateEmailFileConfig(settings *EmailSettings) error {
	nuevo := Config{Database: GlobalConfig.Database,
		EmailSettings:   *settings,
		PlanPrueba:      GlobalConfig.PlanPrueba,
		DiasPrueba:      GlobalConfig.DiasPrueba,
		OpenpaySettings: GlobalConfig.OpenpaySettings,
		ServerAddress:   GlobalConfig.ServerAddress,
		TlsSettings:     GlobalConfig.TlsSettings,
	}
	data, err := json.Marshal(nuevo)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile("config.json", data, os.ModePerm)
	if err != nil {
		return err
	}
	return InitConfig()
}

func GetServerAddress() string {
	return GlobalConfig.ServerAddress
}

func UpdatePlanPrueba(plan string, dias int) error {
	nuevo := Config{Database: GlobalConfig.Database,
		EmailSettings:   GlobalConfig.EmailSettings,
		OpenpaySettings: GlobalConfig.OpenpaySettings,
		ServerAddress:   GlobalConfig.ServerAddress,
		TlsSettings:     GlobalConfig.TlsSettings,
		PlanPrueba:      plan,
		DiasPrueba:      dias,
	}
	data, err := json.Marshal(nuevo)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile("config.json", data, os.ModePerm)
	if err != nil {
		return err
	}
	return InitConfig()
}
