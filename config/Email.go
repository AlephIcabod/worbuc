package config

import (
	"net/smtp"
	"github.com/scorredoira/email"
	"io/ioutil"
	"os"
	"net/mail"
)

func AddFile(fileBytes []byte, name string, body *email.Message) error {
	ioutil.WriteFile(name, fileBytes, os.ModePerm)
	err := body.Attach(name)
	return err
}

func CargarDatosCorreo(subject, body, destinatario string, html bool, copias ...string) (*email.Message, error) {
	settings := GetConfig().EmailSettings
	var m *email.Message
	if html {
		m = email.NewHTMLMessage(subject, body)
	} else {
		m = email.NewMessage(subject, body)
	}
	m.From = mail.Address{Name: settings.Nombre, Address: settings.Correo.Usuario}
	m.To = []string{destinatario}
	if copias != nil && len(copias) > 0 {
		m.Bcc = copias
	}
	return m, nil
}

func EnviarCorreo(m *email.Message) error {
	settings := GetConfig().EmailSettings
	auth2 := smtp.PlainAuth(
		"",
		settings.Correo.Usuario,
		settings.Correo.Password,
		settings.Correo.Servidor,
	)
	err := email.Send(settings.Correo.Servidor+":"+settings.Correo.Puerto, auth2, m)
	return err
}
