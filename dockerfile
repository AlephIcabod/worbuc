FROM        golang:1.8
MAINTAINER  Marshall Shen <marshall@test.com>

ENV     PORT  80

# Setting up working directory
WORKDIR     /go/src/bitbucket.org/AlephIcabod/worbuc
ADD         . /go/src/bitbucket.org/AlephIcabod/worbuc

RUN go get -v
RUN go get github.com/rs/xid
RUN go get github.com/jinzhu/inflection

RUN apt-get install -y wget
RUN wget https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh
RUN pwd
RUN chmod +x wait-for-it.sh

# Restore godep dependencies
#RUN godep restore

EXPOSE 80

