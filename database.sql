CREATE TABLE pais (
  id     SERIAL PRIMARY KEY NOT NULL,
  nombre VARCHAR            NOT NULL,
  codigo VARCHAR            NOT NULL
);

CREATE TABLE estado (
  id     SERIAL PRIMARY KEY NOT NULL,
  pais   INTEGER,
  nombre VARCHAR            NOT NULL,
  FOREIGN KEY (pais) REFERENCES pais (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE institucion (
  id     SERIAL PRIMARY KEY NOT NULL,
  nombre VARCHAR            NOT NULL
);

CREATE TABLE empresa (
  id     SERIAL PRIMARY KEY NOT NULL,
  nombre VARCHAR            NOT NULL,
  sector VARCHAR
);

CREATE TABLE usuario (
  id               VARCHAR PRIMARY KEY NOT NULL,
  nombre           VARCHAR             NOT NULL,
  apellido_paterno VARCHAR             NOT NULL,
  apellido_materno VARCHAR,
  fecha_nacimiento DATE,
  genero           CHAR(1)             NOT NULL,
  email            VARCHAR UNIQUE      NOT NULL,
  escolaridad      VARCHAR,
  password         VARCHAR             NOT NULL,
  fecha_registro   TIMESTAMP DEFAULT now(),
  tipo_perfil      VARCHAR             NOT NULL,
  foto_perfil      VARCHAR             NOT NULL,
  zona_horaria     VARCHAR             NOT NULL
);

CREATE TABLE usuario_direccion (
  id            SERIAL PRIMARY KEY NOT NULL,
  usuario       VARCHAR            NOT NULL,
  estado        INTEGER            NOT NULL,
  poblacion     VARCHAR            NOT NULL,
  codigo_postal VARCHAR            NOT NULL,
  FOREIGN KEY (usuario) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY (estado) REFERENCES estado (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE usuario_institucion (
  id          SERIAL PRIMARY KEY NOT NULL,
  usuario     VARCHAR            NOT NULL,
  institucion INT                NOT NULL,
  periodo     VARCHAR,
  FOREIGN KEY (usuario) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY (institucion) REFERENCES institucion (id) ON DELETE RESTRICT ON UPDATE CASCADE
);


CREATE TABLE usuario_empresa (
  id      SERIAL PRIMARY KEY NOT NULL,
  usuario VARCHAR            NOT NULL,
  empresa INTEGER            NOT NULL,
  puesto  VARCHAR,
  FOREIGN KEY (usuario) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY (empresa) REFERENCES empresa (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE usuario_telefono (
  id       SERIAL PRIMARY KEY NOT NULL,
  usuario  VARCHAR            NOT NULL,
  nombre   VARCHAR            NOT NULL,
  telefono VARCHAR            NOT NULL,
  tipo     VARCHAR            NOT NULL,
  FOREIGN KEY (usuario) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE usuario_contacto (
  id       SERIAL PRIMARY KEY NOT NULL,
  usuario  VARCHAR            NOT NULL,
  contacto VARCHAR            NOT NULL,
  estatus  VARCHAR            NOT NULL,
  fecha    TIMESTAMP DEFAULT now(),
  FOREIGN KEY (usuario) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY (contacto) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE notificacion (
  id          SERIAL PRIMARY KEY NOT NULL,
  emisor      VARCHAR            NOT NULL,
  receptor    VARCHAR            NOT NULL,
  titulo      VARCHAR            NOT NULL,
  descripcion VARCHAR            NOT NULL,
  FOREIGN KEY (emisor) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY (receptor) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE libreta (
  id          SERIAL PRIMARY KEY NOT NULL,
  usuario     VARCHAR            NOT NULL,
  nombre      VARCHAR            NOT NULL,
  descripcion VARCHAR,
  FOREIGN KEY (usuario) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE reunion (
  id          VARCHAR PRIMARY KEY NOT NULL,
  titulo      VARCHAR             NOT NULL,
  objetivo    TEXT                NOT NULL,
  fecha       DATE                NOT NULL,
  direccion   VARCHAR             NOT NULL,
  long        VARCHAR,
  latitude    VARCHAR,
  hora_inicio TIME                NOT NULL,
  duracion    FLOAT               NOT NULL,
  estatus     VARCHAR             NOT NULL,
  etiquetas   VARCHAR []          NOT NULL,
  organizador VARCHAR             NOT NULL,
  FOREIGN KEY (organizador) REFERENCES usuario (id) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE reunion_archivo (
  id             SERIAL PRIMARY KEY  NOT NULL,
  reunion        VARCHAR             NOT NULL,
  nombre         VARCHAR             NOT NULL,
  path           VARCHAR             NOT NULL,
  fecha_creacion TIMESTAMP DEFAULT now(),
  size           FLOAT               NOT NULL,
  FOREIGN KEY (reunion) REFERENCES reunion (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE reunion_invitado (
  reunion       VARCHAR NOT NULL,
  invitado      VARCHAR NOT NULL,
  libreta       INTEGER NOT NULL,
  rol           VARCHAR NOT NULL,
  alias         CHAR(3) NOT NULL,
  administrador BOOLEAN NOT NULL,
  asistio       BOOLEAN NOT NULL,
  confirmacion  BOOLEAN NOT NULL,
  FOREIGN KEY (reunion) REFERENCES reunion (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY (invitado) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY (libreta) REFERENCES libreta (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  PRIMARY KEY (reunion, invitado)
);

CREATE TABLE reunion_minuta (
  id           SERIAL PRIMARY KEY NOT NULL,
  reunion      VARCHAR            NOT NULL,
  responsable  VARCHAR            NOT NULL,
  nombre_tema  VARCHAR            NOT NULL,
  descripcion  TEXT               NOT NULL,
  acuerdos     TEXT,
  estatus      VARCHAR            NOT NULL,
  numero_orden INTEGER            NOT NULL,
  FOREIGN KEY (reunion, responsable) REFERENCES reunion_invitado (reunion, invitado) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE reunion_nota (
  reunion VARCHAR NOT NULL,
  usuario VARCHAR NOT NULL,
  nota    TEXT    NOT NULL,
  FOREIGN KEY (reunion, usuario) REFERENCES reunion_invitado (reunion, invitado) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE paquete (
  id            SERIAL PRIMARY KEY NOT NULL,
  nombre        VARCHAR            NOT NULL,
  costo         FLOAT              NOT NULL,
  descripcion   VARCHAR,
  tamanio_megas INTEGER            NOT NULL
);

CREATE TABLE usuario_paquete (
  id           SERIAL PRIMARY KEY  NOT NULL,
  usuario      VARCHAR             NOT NULL,
  paquete      INT                 NOT NULL,
  fecha_compra TIMESTAMP           NOT NULL DEFAULT now(),
  FOREIGN KEY (usuario) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY (paquete) REFERENCES paquete (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE proyecto (
  id          VARCHAR PRIMARY KEY  NOT NULL,
  nombre      VARCHAR              NOT NULL,
  descripcion VARCHAR
);

CREATE TABLE proyecto_usuario (
  id            SERIAL PRIMARY KEY NOT NULL,
  proyecto      VARCHAR            NOT NULL,
  usuario       VARCHAR            NOT NULL,
  rol           VARCHAR,
  estatus       VARCHAR            NOT NULL,
  fecha_ingreso TIMESTAMP          NOT NULL DEFAULT now(),
  FOREIGN KEY (proyecto) REFERENCES proyecto (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY (usuario) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE proyecto_actividad (
  id          VARCHAR PRIMARY KEY NOT NULL,
  proyecto    VARCHAR             NOT NULL,
  titulo      VARCHAR             NOT NULL,
  descripcion VARCHAR,
  FOREIGN KEY (proyecto) REFERENCES proyecto (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE tarea (
  id            VARCHAR PRIMARY KEY NOT NULL,
  titulo        VARCHAR             NOT NULL,
  descripcion   VARCHAR,
  prioridad     VARCHAR             NOT NULL,
  fecha_inicio  TIMESTAMP           NOT NULL,
  fecha_termino TIMESTAMP           NOT NULL,
  estatus       VARCHAR             NOT NULL,
  etiquetas     VARCHAR [],
  actividad     VARCHAR             NOT NULL,
  FOREIGN KEY (actividad) REFERENCES proyecto_actividad (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE tarea_usuario (
  id            SERIAL PRIMARY KEY NOT NULL,
  tarea         VARCHAR            NOT NULL,
  usuario       VARCHAR            NOT NULL,
  rol           VARCHAR            NOT NULL,
  fecha_ingreso TIMESTAMP          NOT NULL DEFAULT now(),
  estatus       VARCHAR            NOT NULL,
  libreta       INTEGER            NOT NULL,
  FOREIGN KEY (tarea) REFERENCES tarea (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY (usuario) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY (libreta) REFERENCES libreta (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE tarea_comentario (
  id         SERIAL PRIMARY KEY NOT NULL,
  tarea      VARCHAR            NOT NULL,
  usuario    VARCHAR            NOT NULL,
  comentario TEXT               NOT NULL,
  fecha      TIMESTAMP          NOT NULL DEFAULT now(),
  FOREIGN KEY (tarea) REFERENCES tarea (id) ON DELETE RESTRICT ON UPDATE CASCADE,
  FOREIGN KEY (usuario) REFERENCES usuario (id) ON DELETE RESTRICT ON UPDATE CASCADE
);

COPY pais FROM '/home/aleph/Documents/worbuc/paises.csv' WITH (FORMAT csv);