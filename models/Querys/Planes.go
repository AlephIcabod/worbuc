package Querys

import (
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/models"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"time"
	"github.com/go-pg/pg"
)

func GetPlan(id string) (*Schemas.Plan, error) {
	db := models.GetDataBase()
	var p Schemas.Plan
	err := db.Model(&p).Where("id=?", id).First()
	if err != nil {
		return nil, err
	}
	return &p, nil
}

func GetAllPlans(activos bool) ([]Schemas.Plan, error) {
	db := models.GetDataBase()
	var p []Schemas.Plan
	var err error
	if !activos {
		err = db.Model(&p).Order("id asc").Select()
	} else {
		err = db.Model(&p).Where("estatus=?", Constants.ACTIVO).Order("id asc").Select()
	}
	if err != nil {

		return nil, err
	}
	return p, nil
}

func CreatePlan(precio, limite float64, nombre, descripcion, id string, apps []int) (error) {
	db, err := models.GetDataBase().Begin()
	if err != nil {
		return  err
	}
	p := Schemas.Plan{
		Id:          id,
		Precio:      precio,
		Descripcion: descripcion,
		Apps:        apps,
		LimiteMb:    limite,
		Nombre:      nombre,
		CreatedAt:   time.Now()}

	err = db.Insert(&p)
	if err != nil {
		db.Rollback()
		return  err
	}
	return  db.Commit()
}

func UpdatePlan(plan *Schemas.Plan) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	r, err := db.Model(plan).
		Set("precio=?", plan.Precio).
		Set("nombre=?", plan.Nombre).
		Set("descripcion=?", plan.Descripcion).
		Set("limite_mb=?", plan.LimiteMb).
		Set("apps=?", pg.Array(plan.Apps)).
		Where("id=?", plan.Id).Update()

	if err != nil || r.RowsAffected() != 1 {
		tx.Rollback()
		return err
	}

	ids := make([]int, 0, len(plan.Apps))
	for _, v := range plan.Apps {
		ids = append(ids, v)
	}

	return tx.Commit()
}

func CambiarEstatusPlan(id string) (string, error) {
	db := models.GetDataBase()
	tx, err := db.Begin()
	var p Schemas.Plan
	if err != nil {
		return "", err
	}
	err = db.Model(&p).Where("id=?", id).First()
	if err != nil {
		tx.Rollback()
		return "", err
	}

	var estatus string
	if p.Estatus == Constants.ACTIVO {
		estatus = Constants.INACTIVO
	} else {
		estatus = Constants.ACTIVO
	}
	r, err := db.Model((*Schemas.Plan)(nil)).
		Set("estatus=?", estatus).
		Where("id=?", id).Update()
	if err != nil || r.RowsAffected() != 1 {
		tx.Rollback()
		return "", err
	}
	return estatus, tx.Commit()
}
