package Querys

import (
	"bitbucket.org/AlephIcabod/worbuc/models"
	"time"
	"github.com/go-pg/pg"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
)

type UsuarioReporte struct {
	Id              string
	Nombre          string
	ApellidoPaterno string
	ApellidoMaterno string
	Email           string
	Estatus         string
	Genero          string
	TipoPerfil      string
	ZonaHoraria     string
	FechaNacimiento time.Time
	FotoPerfil      string
	FechaRegistro   time.Time
	CodigoPostal    string
	Poblacion       string
	EstadoID        string
	PaisID          string
	Escolaridad     string
	InstitucionId   string
	Periodo         string
	Puesto          string
	EmpresaId       string
	Plan            string
	PlanNombre      string
	Precio          float64
	LimiteMb        float64
	UltimoCambio    time.Time
	Ocupado         float64
	Disponible      float64
	Sector          string
}

type ReporteUsuarioPlan struct {
	Nombre string
	Fecha  time.Time
	Total  int64
}

type ReporteRegistroFecha struct {
	Fecha       time.Time
	Registrados int64
}

type ReporteReuniones struct {
	Fecha   time.Time
	Inicio  time.Time
	Estatus string
	Total   int64
}

type ReporteProyectos struct {
	Fecha   time.Time
	Estatus string
	Total   int64
}

type ReporteDocumentos struct {
	Fecha     time.Time
	Total     int64
	PesoTotal float64
}

type ReporteConexiones struct {
	Fecha           time.Time
	TipoDispositivo string
	Total           int64
}

func GetFullUsers(limit, page int64) ([]*UsuarioReporte, int64, error) {
	db := models.GetDataBase()
	var res []*UsuarioReporte
	_, err := db.Query(&res, `select u.id,
  u.nombre,
  u.apellido_paterno,
  u.apellido_paterno,
  u.email,
  u.estatus,
  u.genero,
  u.tipo_perfil,
  u.zona_horaria,
  u.fecha_nacimiento,
  u.foto_perfil,
  u.created_at            fecha_registro,
  d.codigo_postal,
  d.poblacion,
  e3.nombre               estado_id,
  pais.nombre             pais_id,
  u.escolaridad,
  i.nombre                institucion_id,
  e.periodo,
  e2.puesto,
  em.nombre empresa_id,
  em.sector,
  p.id          as        plan,
  p.nombre      as        plan_nombre,
  p.precio,
  p.limite_mb,
  pu.updated_at as        ultimo_cambio,
  s2.ocupado              ocupado,
  (p.limite_mb - ocupado) disponible
from usuarios u
  inner join plan_usuarios pu on u.id = pu.usuario_id
  inner join plans p on pu.plan_id = p.id
  left join direccions d on d.usuario_id = u.id
  left join educacions e on u.id = e.usuario_id
  left join empleos e2 on u.id = e2.usuario_id
  left join (select
               usuario_id,
               sum(size / 1024 / 1024) ocupado
             from archivo_reunions
             group by usuario_id) as s2 on u.id = s2.usuario_id
  left join pais on pais_id = pais.id
  left join estados e3 on estado_id = e3.id
  left join institucions i on i.id = institucion_id
  left join empresas em on em.id = empresa_id
	ORDER BY u.nombre, u.apellido_paterno, u.apellido_materno, fecha_registro
	LIMIT  ? OFFSET  ?`, limit, (page-1)*limit)
	if err != nil {
		return nil, 0, err
	}
	total, err := db.Model((*Schemas.Usuario)(nil)).Count()

	return res, int64(total), nil
}

func GetReportes(fechaInicio, fechaTermino time.Time) (map[string]interface{}, error) {
	db := models.GetDataBase()
	res := make(map[string]interface{})
	planes, err := GetUserPlanDate(db, fechaInicio, fechaTermino)
	if err != nil {
		if err != pg.ErrNoRows {
			return nil, err
		}
	}
	registro, err := GetHistoryRegister(db, fechaInicio, fechaTermino)
	if err != nil {
		if err != pg.ErrNoRows {
			return nil, err
		}
	}

	reuniones, err := GetHistoryReuniones(db, fechaInicio, fechaTermino)
	if err != nil {
		if err != pg.ErrNoRows {
			return nil, err
		}
	}

	proyectos, err := GetHistoryProyectos(db, fechaInicio, fechaTermino)
	if err != nil {
		if err != pg.ErrNoRows {
			return nil, err
		}
	}

	documentos, err := GetHistoryDocumentos(db, fechaInicio, fechaTermino)
	if err != nil {
		if err != pg.ErrNoRows {
			return nil, err
		}
	}

	conexiones, err := GetHistoryConexiones(db, fechaInicio, fechaTermino)
	if err != nil {
		if err != pg.ErrNoRows {
			return nil, err
		}
	}

	res["planes"] = planes
	res["registro"] = registro
	res["reuniones"] = reuniones
	res["proyectos"] = proyectos
	res["documentos"] = documentos
	res["conexiones"] = conexiones
	return res, nil
}

func GetUserPlanDate(db *pg.DB, fechaInicio, fechaTermino time.Time) ([]*ReporteUsuarioPlan, error) {
	var res []*ReporteUsuarioPlan
	_, err := db.Query(&res, `select
  p.nombre,
  date_trunc('day', pu.updated_at) fecha,
  count(distinct pu.usuario_id)    total
from plan_usuarios pu inner join plans p on pu.plan_id = p.id
  where pu.updated_at between ? and ?
group by p.nombre, fecha`, fechaInicio, fechaTermino)
	return res, err
}

func GetHistoryRegister(db *pg.DB, fechaInicio, fechaTermino time.Time) ([]*ReporteRegistroFecha, error) {
	var res []*ReporteRegistroFecha
	_, err := db.Query(&res, `select
  date_trunc('day', u.created_at) fecha,
  count(distinct id)              registrados
from usuarios u
where u.is_admin is null and u.created_at between ? and ?
group by fecha`, fechaInicio, fechaTermino)

	return res, err
}

func GetHistoryReuniones(db *pg.DB, fechaInicio, fechaTermino time.Time) ([]*ReporteReuniones, error) {
	var res []*ReporteReuniones
	_, err := db.Query(&res, `select
  date_trunc('day', r.created_at) as fecha,
  r.hora_inicio as inicio,
  estatus,
  count(distinct id)            total
from reunions r
where estatus is not null and r.created_at between ? and ?
group by date_trunc('day', r.created_at), estatus, r.hora_inicio`, fechaInicio, fechaTermino)
	return res, err
}

func GetHistoryProyectos(db *pg.DB, fechaInicio, fechaTermino time.Time) ([]*ReporteProyectos, error) {
	var res []*ReporteProyectos
	_, err := db.Query(&res, `select
  date_trunc('day', r.created_at) as fecha,
  estatus,
  count(distinct id)                 total
from proyectos r
where estatus is not null and r.created_at between ? and ?
group by date_trunc('day', r.created_at), estatus`, fechaInicio, fechaTermino)
	return res, err
}

func GetHistoryDocumentos(db *pg.DB, fechaInicio, fechaTermino time.Time) ([]*ReporteDocumentos, error) {
	var res []*ReporteDocumentos
	_, err := db.Query(&res, `select
  count(distinct id)              total,
  date_trunc('day', a.created_at) fecha,
  sum(size / 1024 / 1024)         peso_total
from archivo_reunions a
  where a.created_at between ? and ?
group by fecha`, fechaInicio, fechaTermino)
	return res, err
}

func GetHistoryConexiones(db *pg.DB, fechaInicio, fechaTermino time.Time) ([]*ReporteConexiones, error) {
	var res []*ReporteConexiones
	_, err := db.Query(&res, `select
  date_trunc('minute', s.fecha_inicio) fecha,
  s.tipo_dispositivo,
  count(distinct usuario_id) total
from sesions s
  where s.fecha_inicio between ? and ?
group by  s.tipo_dispositivo,fecha`, fechaInicio, fechaTermino)
	return res, err
}
