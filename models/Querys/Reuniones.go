package Querys

import (
	`github.com/kataras/iris/core/errors`
	`fmt`
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/models"
	"github.com/go-pg/pg"
	"database/sql"
	"github.com/go-pg/pg/orm"
	"time"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"strings"
)

func CreateReunion(reunion *Schemas.Reunion, usuario string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	err = tx.Insert(reunion)
	if err != nil {
		tx.Rollback()
		return err
	}
	if reunion.Archivos != nil && len(reunion.Archivos) > 0 {
		err = updateFilesReunion(reunion.Archivos, usuario, tx)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit()
}

func RemoveReunion(reunion string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	tx.Model((*Schemas.ArchivoReunion)(nil)).Where("reunion_id=?", reunion).Delete()
	tx.Model((*Schemas.Participante)(nil)).Where("reunion_id=?", reunion).Delete()
	tx.Model((*Schemas.Minuta)(nil)).Where("reunion_id=?", reunion).Delete()
	tx.Model((*Schemas.Reunion)(nil)).Where("id=?", reunion).Delete()
	return tx.Commit()
}

func ActualizarInvitadosYMinuta(invitados []Schemas.Participante, minuta []Schemas.Minuta,
	reunion, userSelf string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	err = updateParticipantes(invitados, reunion, userSelf, tx)
	if err != nil {
		tx.Rollback()
		return err
	}
	err = updateMinuta(minuta, reunion, tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

func updateParticipantes(invitados []Schemas.Participante, reunion, usuario string, tx *pg.Tx) error {

	ids := make([]string, 0, len(invitados))
	for _, v := range invitados {
		ids = append(ids, v.UsuarioID)
	}

	_, err := tx.Model((*Schemas.Participante)(nil)).Where("reunion_id=?", reunion).
		Where("usuario_id not in (?)", pg.In(ids)).Delete()
	if err != nil {
		return err
	}

	var existentes []Schemas.Participante
	err = tx.Model(&existentes).Where("reunion_id=?", reunion).Select()
	if err != nil && err != sql.ErrNoRows {
		tx.Rollback()
		return err
	}
	participantes := make([]string, 0)
	for _, p := range invitados {
		participantes = append(participantes, p.UsuarioID)
	}

	var libretas []Schemas.Libreta
	err = tx.Model(&libretas).
		Where("libreta.default = ?", true).
		Where("usuario_id IN (?)", pg.In(participantes)).Select()
	if err != nil {
		tx.Rollback()
		return err
	}

	auxLibretas := make(map[string]Schemas.Libreta, 0)

	for _, v := range libretas {
		auxLibretas[v.UsuarioID] = v
	}

	for i := range invitados {
		nuevo := true
		for j := range existentes {
			if existentes[j].UsuarioID == invitados[i].UsuarioID {
				nuevo = false
				q := tx.Model(&invitados[i]).Set("rol=?", invitados[i].Rol).
					Set("alias=?", invitados[i].Alias).
					Set("administrador=?", invitados[i].Administrador)
				if usuario == invitados[i].UsuarioID {
					if invitados[i].LibretaID > 0 {
						q = q.Set("libreta_id=?", invitados[i].LibretaID)
					}
					if invitados[i].Etiquetas != nil {
						q = q.Set("etiquetas=?", pg.Array(invitados[i].Etiquetas))
					}
				} else {
					if invitados[i].LibretaID == 0 {
						q = q.Set("libreta_id=?", auxLibretas[invitados[i].UsuarioID].Id)
					}
				}
				_, err = q.
					Where("usuario_id=?", invitados[i].UsuarioID).
					Where("reunion_id=?", reunion).
					Update()
				break
			}
		}
		if nuevo {
			if invitados[i].LibretaID==0 {
				invitados[i].LibretaID = auxLibretas[invitados[i].UsuarioID].Id
			}
			_, err = tx.Model(&invitados[i]).
				Insert()
		}
		if err != nil {
			break
		}
		if usuario != invitados[i].UsuarioID {
			AddContact(usuario, invitados[i].UsuarioID)
		}
	}
	if err != nil {
		return err
	}
	return nil
}

func updateMinuta(minuta []Schemas.Minuta, reunion string, tx *pg.Tx) error {
	if minuta == nil || len(minuta) == 0 {
		return nil
	}
	var err error
	for _, v := range minuta {
		if v.Id > 0 {
			_, err = tx.Model(&v).Where("id=?", v.Id).Update()
		} else {
			_, err = tx.Model(&v).Insert()
		}
	}
	return err
	/*_, err := tx.Model((*Schemas.Minuta)(nil)).Where("reunion_id=?", reunion).Delete()
	if err != nil {
		return err
	}
	_, err = tx.Model(&minuta).Insert()
	if err != nil {
		return err
	}*/
}

func FindReunion(id string) (*Schemas.Reunion, error) {
	db := models.GetDataBase()
	var reunion Schemas.Reunion
	err := db.Model(&reunion).Where("id=?", id).First()
	if err != nil {
		return nil, err
	}
	return &reunion, nil
}

func UpdateReunion(reunion *Schemas.Reunion, usuario string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	var previo Schemas.Reunion
	err = tx.Model(&previo).Where("id=?", reunion.Id).First()
	if err != nil {
		return err
	}
	reunion.CreatedAt = previo.CreatedAt
	reunion.UpdatedAt = time.Now()
	_, err = tx.Model(reunion).Where("id=?", reunion.Id).Update()
	if err != nil {
		tx.Rollback()
		return err
	}
	if reunion.Archivos != nil && len(reunion.Archivos) > 0 {
		err = updateFilesReunion(reunion.Archivos, usuario, tx)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}

/*func GetReuniones(usuario string, limit, page int64, filtro, textoFiltro string,
	libreta int64, estatus string, admin bool) ([]Schemas.Reunion, int, error) {
	db := models.GetDataBase()
	max := 0
	var res []Schemas.Reunion
	ids := make([]string, 0, 0)
	q1 := db.Model((*Schemas.Participante)(nil)).
		ColumnExpr("array_agg(reunion_id)")
	evaluar := true
	if libreta > 0 {
		q1 = q1.
			Where("libreta_id=?", libreta)
	}
	if filtro == "etiquetas" {
		tags := strings.Split(textoFiltro, ",")
		for _, v := range tags {
			q1 = q1.WhereOr("?=ANY (etiquetas)", strings.TrimSpace(v))
		}
	}
	q1 = q1.Where("usuario_id=?", usuario)
	if estatus != "" {
		q1 = q1.Where("estatus=?", estatus)
	}

	q1.Select(pg.Array(&ids))

	if len(ids) == 0 {
		return res, 0, nil
	}

	q := db.Model(&res).
		Column("reunion.*", "Participantes", "Organizador").
		Relation("Participantes", func(q *orm.Query) (*orm.Query, error) {
		return q.Where("usuario_id=?", usuario), nil
	}).
		Relation("Organizador", func(q *orm.Query) (*orm.Query, error) {
		return q.Column("nombre", "apellido_paterno", "apellido_materno", "email"), nil
	}).
		Order("reunion.fecha DESC").
		Order("reunion.estatus ASC").
		Offset(int((page - 1) * limit)).
		Limit(int(limit))

	if filtro == "titulo" {
		q = q.Where("reunion.titulo ilike ?", "%"+textoFiltro+"%")
	}
	if evaluar {
		q.Where("reunion.id in (?)", pg.In(ids))
	}

	err := q.
		Select()
	if err != nil {
		fmt.Println("error", err)
		return nil, max, err
	}
	max, err = db.Model((*Schemas.Participante)(nil)).
		Where("usuario_id=?", usuario).Count()
	return res, max, nil
}
*/

func GetReuniones(usuario string, limit, page, libreta int64, tags []string, filter string) ([]Schemas.Reunion, int, error) {
	db := models.GetDataBase()
	max := 0
	var res []Schemas.Reunion
	ids := make([]string, 0, 0)

	q1 := db.Model((*Schemas.Participante)(nil)).
		ColumnExpr("array_agg(reunion_id)")
	if libreta > 0 && filter == "" {
		q1 = q1.Where("libreta_id=?", libreta)
	}
	if tags != nil && len(tags) > 0 && filter == "" {
		for _, v := range tags {
			if strings.TrimSpace(v) != "" {
				q1 = q1.Where("?=ANY (etiquetas)", strings.TrimSpace(v))
			}
		}
	}

	err := q1.Where("usuario_id=?", usuario).Select(pg.Array(&ids))
	if err != nil {
		return nil, 0, err
	}

	if len(ids) == 0 && filter == "" {
		return nil, 0, errors.New("No hay ninguna reunion")
	}

	q := db.Model(&res).
		Column("reunion.*", "Participantes", "Organizador").
		Relation("Participantes", func(q *orm.Query) (*orm.Query, error) {
		return q, nil
	}).
		Relation("Organizador", func(q *orm.Query) (*orm.Query, error) {
		return q.Column("nombre", "apellido_paterno", "apellido_materno", "email"), nil
	}).
		Order("reunion.fecha DESC").
		Order("reunion.estatus ASC").
		Offset(int((page - 1) * limit)).
		Limit(int(limit))
	if len(ids) > 0 && filter == "" {
		q = q.Where("reunion.id in (?)", pg.In(ids)).
			Where("reunion.id<>?", "PENDIENTE")
	}
	if filter != "" {
		filter = "%" + filter + "%"
		q = q.WhereOr("reunion.titulo ilike ?", filter).
			WhereOr("reunion.objetivo  ilike ?", filter)
	}
	err = q.
		Select()
	if err != nil {
		return nil, max, err
	}
	max, err = db.Model((*Schemas.Participante)(nil)).
		Where("usuario_id=?", usuario).Count()
	return res, max, nil
}

func GetReunion(id string, usuario string) (*Schemas.Reunion, error) {
	var res Schemas.Reunion
	db := models.GetDataBase()
	err := db.Model(&res).
		Column("reunion.*", "Participantes", "Minuta", "Archivos", "Organizador").
		Relation("Participantes", func(q *orm.Query) (*orm.Query, error) {
		return q, nil
	}).
		Relation("Minuta", func(q *orm.Query) (*orm.Query, error) {
		return q.Order("numero_orden ASC"), nil
	}).
		Relation("Organizador", func(q *orm.Query) (*orm.Query, error) {
		return q.Column("nombre", "apellido_paterno", "apellido_materno",
			"email", "foto_perfil"), nil
	}).
		Relation("Archivos", func(q *orm.Query) (*orm.Query, error) {
		return q, nil
	}).
		Where("reunion.id=?", id).
		First()
	if err != nil {
		return nil, err
	}
	err = db.Model(&res.Participantes).Column("participante.*", "Usuario.nombre", "Usuario.apellido_paterno",
		"Usuario.id", "Usuario.apellido_materno", "Usuario.email").
		Where("reunion_id=?", id).Select()

	for i := range res.Minuta {
		err = db.Model(&res.Minuta[i].Notas).
			Where("reunion_id=?", id).
			Where("minuta_id=?", res.Minuta[i].Id).
			Where("usuario_id=?", usuario).Select()
	}

	return &res, err
}

func Disponibilidad(fecha time.Time, usuario, reunion string) (int, error) {
	db := models.GetDataBase()
	res := 0
	var reuniones []Schemas.Reunion
	err := db.Model(&reuniones).
		Column("reunion.*", "Participantes").
		Relation("Participantes", func(q *orm.Query) (*orm.Query, error) {
		return q.Where("usuario_id=?", usuario), nil
	}).
		Where("reunion.fecha between ? and ? ", fecha, fecha.Add(time.Hour+24)).
		Where("reunion.estatus=?", Constants.AGENDADA).
		WhereOr("reunion.estatus=?", Constants.EN_PROCESO).
		Select()

	if err != nil {
		if err == pg.ErrNoRows {
			return 0, nil
		}
		return 0, err
	}
	for _, v := range reuniones {
		if v.Id != reunion && (fecha.After(v.HoraInicio) && fecha.Before(v.HoraTermino) || fecha == v.HoraInicio) {
			return res, errors.New("Ya hay reunion agendada a la misma hora")
		}
	}

	return 0, nil
}

func CancelReunion(id, user string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	var p Schemas.Participante
	err = tx.Model(&p).
		Column("participante.*", "Reunion").
		Relation("Reunion", func(q *orm.Query) (*orm.Query, error) {
		return q, nil
	}).
		Where("reunion_id=?", id).
		Where("usuario_id=?", user).First()
	if err != nil {
		tx.Rollback()
		return err
	}
	if p.Administrador {
		if p.Reunion.Estatus != Constants.FINALIZADA && p.Reunion.Estatus != Constants.EN_PROCESO {
			_, err = tx.Model((*Schemas.Minuta)(nil)).Where("reunion_id=?", id).Delete()
			_, err = tx.Model((*Schemas.Participante)(nil)).Where("reunion_id=?", id).Delete()
			_, err = tx.Model((*Schemas.ArchivoReunion)(nil)).Where("reunion_id=?", id).Delete()
			_, err = tx.Model((*Schemas.Reunion)(nil)).Where("id=?", id).Delete()
			if err != nil {
				tx.Rollback()
				return err
			}
		} else {
			tx.Rollback()
			return errors.New("La reunion ya no se puede cancelar, estatus: " + p.Reunion.Estatus)
		}
	} else {
		tx.Rollback()
		return errors.New("No tiene permisos para cancelar la reunion")
	}
	return tx.Commit()
}

func RemoveFileReunion(id string, usuario string) (string, error) {
	db := models.GetDataBase()
	var aux Schemas.ArchivoReunion
	err := db.Model(&aux).Where("id=?", id).First()
	if err != nil {
		return "", err
	}
	if aux.UsuarioID != usuario {
		return "", errors.New("No tiene permiso para eliminar ese archivo")
	}
	_, err = db.Model((*Schemas.ArchivoReunion)(nil)).Where("id=?", id).Delete()
	return aux.Url, err
}

func updateFilesReunion(files []Schemas.ArchivoReunion, usuario string, tx *pg.Tx) error {
	var err error
	tx.Model((*Schemas.ArchivoReunion)(nil)).
		Where("reunion_id=?", "PENDIENTE").
		Where("usuario_id=?", usuario).Delete()

	for _, v := range files {
		_, err = tx.Model(&v).
			OnConflict("DO NOTHING").
			Insert()
	}
	return err
}

func ClasificarReunion(usuario, reunion string, libreta int64, etiquetas []string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	var participante Schemas.Participante
	_, err = tx.Model(&participante).
		Where("reunion_id=?", reunion).
		Where("usuario_id=?", usuario).
		Set("libreta_id=?", libreta).
		Set("etiquetas=?", pg.Array(etiquetas)).
		Update()
	if err != nil {
		tx.Rollback()
		return err
	}
	//fmt.Println(res.RowsAffected())

	return tx.Commit()
}

func IniciarReunion(reunion, usuario string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	var r Schemas.Reunion

	err = tx.Model(&r).Column("reunion.*", "Participantes").
		Relation("Participantes", func(query *orm.Query) (*orm.Query, error) {
		return query.Where("usuario_id=?", usuario), nil
	}).Where("id=?", reunion).
		First()
	if err != nil {
		tx.Rollback()
		return err
	}
	if r.Participantes[0].Administrador {
		_, err = tx.Model((*Schemas.Reunion)(nil)).
			Set("estatus=?", Constants.EN_PROCESO).
			Where("id=?", reunion).
			Update()
		if err != nil {
			tx.Rollback()
			return err
		} else {
			return tx.Commit()
		}
	} else {
		tx.Rollback()
		return errors.New("No tiene autorizacion para iniciar esa reunion")
	}

	return tx.Commit()
}

func RemoveMinuta(minuta int64, reunion, usuario string) error {
	db := models.GetDataBase()
	var aux Schemas.Participante
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	err = tx.Model(&aux).Where("reunion_id=?", reunion).
		Where("usuario_id=?", usuario).First()
	if err != nil {
		tx.Rollback()
		return err
	}
	if !aux.Administrador {
		tx.Rollback()
		return errors.New("No tiene permisos para realizar esta acción")
	}

	_, err = tx.Model((*Schemas.Minuta)(nil)).
		Where("id=?", minuta).Where("reunion_id=?", reunion).
		Delete()
	if err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit()
}

func CreateMinuta(m *Schemas.Minuta, reunion, usuario string) error {
	db := models.GetDataBase()
	var part Schemas.Participante
	err := db.Model(&part).Where("reunion_id=?", reunion).
		Where("usuario_id=?", usuario).First()
	if err != nil {
		return err
	}
	if !part.Administrador {
		return errors.New("No tiene permisos para esta accion")
	}
	err = db.Model(&part).
		Where("reunion_id=?", reunion).
		Where("alias=?", m.ResponsableID).First()

	if err != nil {
		if err == pg.ErrNoRows {
			return errors.New("No se asigno un responsable válido")
		}
		return err
	}
	m.ResponsableID = part.UsuarioID
	m.ReunionID = reunion
	_, err = db.Model(m).Insert()
	return err
}

func UpdateMinuta(m *Schemas.Minuta, reunion, usuario string) error {
	db := models.GetDataBase()
	var part Schemas.Participante
	err := db.Model(&part).
		Where("reunion_id=?", reunion).
		Where("alias=?", m.ResponsableID).First()
	if err != nil {
		if err == pg.ErrNoRows {
			return errors.New("No se asigno un responsable válido")
		}
		return err
	}
	m.ResponsableID = part.UsuarioID
	m.ReunionID = reunion
	_, err = db.Model(m).
		Set("tema=?", m.Tema).
		Set("descripcion=?", m.Descripcion).
		Set("numero_orden=?", m.NumeroOrden).
		Set("responsable_id=?", m.ResponsableID).
		Set("estatus=?", m.Estatus).
		Set("updated_at=?", m.UpdatedAt).
		Where("reunion_id=?", m.ReunionID).
		Where("id=?", m.Id).
		Update()
	return err
}

func VerificarPermisoByReunion(reunion, usuario string) (bool, error) {
	db := models.GetDataBase()
	var part Schemas.Participante
	err := db.Model(&part).Where("reunion_id=?", reunion).
		Where("usuario_id=?", usuario).First()
	if err != nil {
		return false, err
	}
	if !part.Administrador {
		return false, errors.New("No tiene permisos para esta accion")
	}
	return true, nil
}

func UpdatePaseLista(invitados []Schemas.Participante, reunion string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	for _, v := range invitados {
		_, err = tx.Model(&v).Column("asistencia").
			Where("reunion_id=?", reunion).
			Where("alias=?", v.Alias).Update()
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit()
}

func AddNota(minuta int64, reunion, usuario, texto string) error {
	db := models.GetDataBase()
	nota := Schemas.Nota{
		ReunionID: reunion,
		UsuarioID: usuario,
		MinutaID:  minuta,
		Texto:     texto,
	}
	var aux []Schemas.Nota
	err := db.Model(&aux).Where("minuta_id=?", minuta).
		Where("usuario_id=?", usuario).Select()

	if err != nil || len(aux) == 0 {
		if err == pg.ErrNoRows || len(aux) == 0 {
			return db.Insert(&nota)
		}
		return err
	}
	_, err = db.Model(&nota).
		Set("texto=?", texto).
		Set("updated_at=?", time.Now()).
		Where("usuario_id=?", usuario).
		Where("reunion_id=?", reunion).
		Where("minuta_id=?", minuta).
		Update()
	fmt.Println("error2", err)
	return err
}

func VerificarUsuarioReunion(reunion, usuario string) (bool, error) {
	db := models.GetDataBase()
	var part Schemas.Participante
	err := db.Model(&part).Where("reunion_id=?", reunion).
		Where("usuario_id=?", usuario).First()
	if err != nil {
		return false, err
	}
	return true, nil
}

func FinalizarReunion(reunion string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	r, err := tx.Model((*Schemas.Reunion)(nil)).
		Set("estatus=?", Constants.FINALIZADA).
		Where("id=?", reunion).Update()
	if err != nil {
		tx.Rollback()
		return err
	}
	if r.RowsAffected() > 1 {
		tx.Rollback()
		return errors.New("Error al finalizar la reunion")
	}
	return tx.Commit()
}

func ConfirmarAsistenciaReunion(usuario, reunion string, confirma bool) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	var part Schemas.Participante
	r, err := tx.Model(&part).
		Set("confirmacion=?", confirma).
		Where("reunion_id=?", reunion).
		Where("usuario_id=?", usuario).
		Update()
	if err != nil {
		tx.Rollback()
		return err
	}
	if r.RowsAffected() == 0 {
		tx.Rollback()
		return errors.New("No ha sido invitado a la reunion")
	}
	if r.RowsAffected() > 1 {
		tx.Rollback()
		return errors.New("No se pudo confirmar su asistencia")
	}

	return tx.Commit()
}

func ReunionesRecordatorio() ([]Schemas.Reunion, error) {
	var res []Schemas.Reunion
	db := models.GetDataBase()
	fecha := time.Now().UTC()
	fecha = fecha.Truncate(time.Minute)
	err := db.Model(&res).
		Column("reunion.*", "Participantes", "Minuta", "Archivos", "Organizador").
		Relation("Participantes", func(q *orm.Query) (*orm.Query, error) {
		return q, nil
	}).
		Relation("Organizador", func(q *orm.Query) (*orm.Query, error) {
		return q.Column("nombre", "apellido_paterno", "apellido_materno", "email"), nil
	}).
		Where("reunion.estatus=?", Constants.AGENDADA).
		Where("fecha=?", fecha.Add(time.Minute*10)).
		WhereOr("fecha=?", fecha.Add(time.Minute*30)).
		WhereOr("fecha=?", fecha.Add(time.Hour*24)).
		Select()
	if err != nil {
		return nil, err
	}
	for _, v := range res {
		err = db.Model(&v.Participantes).Column("participante.*", "Usuario.nombre", "Usuario.apellido_paterno",
			"Usuario.id", "Usuario.apellido_materno", "Usuario.email", "Usuario.zona_horaria").
			Where("reunion_id=?", v.Id).Select()
	}

	return res, nil
}

func SaveFileUpload(files []Schemas.ArchivoReunion, pendiente bool) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	for i := range files {
		if pendiente {
			if files[i].ReunionID != "" {
				files[i].ReunionID = "PENDIENTE"
			}
			if files[i].ProyectoID != "" {
				files[i].ProyectoID = "PENDIENTE"
			}
		}
		files[i].CreatedAt = time.Now().UTC()
		err = tx.Insert(&files[i])
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit()
}

func GetReunionesMes(usuario string, mes, anio int32) ([]Schemas.Reunion, error) {
	db := models.GetDataBase()
	var res []Schemas.Reunion
	ids2 := make([]string, 0, 0)
	q2 := db.Model((*Schemas.Participante)(nil)).
		ColumnExpr("array_agg(reunion_id)").
		Where("usuario_id=?", usuario)
	err := q2.Select(pg.Array(&ids2))
	if err != nil {
		if err != pg.ErrNoRows {
			return nil, err
		}
		return nil, nil
	}
	q := db.Model(&res).
		Column("reunion.*", "Participantes", "Organizador").
		Relation("Participantes", func(q *orm.Query) (*orm.Query, error) {
		return q, nil
	}).
		Relation("Organizador", func(q *orm.Query) (*orm.Query, error) {
		return q.Column("nombre", "apellido_paterno", "apellido_materno", "email"), nil
	})
	if len(ids2) > 0 {
		q = q.Where("reunion.id in (?)", pg.In(ids2)).
			Where("reunion.id<>?", "PENDIENTE").
			Where("EXTRACT (MONTH FROM reunion.fecha) = ?", mes).
			Where("EXTRACT (YEAR FROM reunion.fecha) = ?", anio)
	} else {
		return res, nil
	}
	err = q.
		Select()
	if err != nil {
		return nil, err
	}
	return res, nil
}
