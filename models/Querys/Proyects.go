package Querys

import (
	"github.com/rs/xid"
	"time"
	"fmt"
	"strings"
	"errors"
	"github.com/go-pg/pg"

	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/models"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"github.com/go-pg/pg/orm"
)

type ProyectoFinded struct {
	Schemas.Proyecto
	CreadorNombre string
	CreadorEmail  string
	Libreta       int64
	Etiquetas     []string `pg:",array"`
}

func GetProyect(proyecto, userId string) (error, *Schemas.Proyecto) {
	db := models.GetDataBase()
	aux := Schemas.UsuarioProyecto{}
	err := db.Model(&aux).Where("proyecto_id=?", proyecto).Where("usuario_id=?", userId).First()
	if err != nil {
		if err != pg.ErrNoRows {
			return errors.New("No tiene permiso para ver este proyecto"), nil
		}
		return err, nil
	}
	var proyect Schemas.Proyecto
	err = db.Model(&proyect).Column("proyecto.*", "Creador", "Participantes",
		"Participantes.Usuario", "Actividades", "Archivos", "Actividades.Tareas", "Actividades.Tareas.Creador",
		"Actividades.Tareas.Creador.Usuario", "Actividades.Tareas.Archivos",
		"Actividades.Tareas.Responsables", "Actividades.Tareas.Supervisores",
		"Actividades.Tareas.Supervisores.Usuario", "Actividades.Tareas.Responsables.Usuario",
		"Actividades.Tareas.Responsables.Usuario.Usuario").
		Relation("Actividades.Tareas.Responsables", func(query *orm.Query) (*orm.Query, error) {
		return query.Where("rol=?", Constants.ROL_REPONSABLE), nil
	}).
		Relation("Actividades.Tareas.Supervisores", func(query *orm.Query) (*orm.Query, error) {
		return query.Where("rol=?", Constants.ROL_SUPERVISOR), nil
	}).
		Relation("Participantes", func(query *orm.Query) (*orm.Query, error) {
		return query.Where("usuario_proyecto.estatus=?", Constants.USUARIO_ACTIVO), nil
	}).
		Where("proyecto.id=?", proyecto).First()
	if err != nil {
		return err, nil
	}
	return nil, &proyect
}

func GetProyects(userId string, libreta int64, tags []string, filter string) ([]Schemas.Proyecto, error) {
	db := models.GetDataBase()
	ids := make([]string, 0, 0)
	q1 := db.Model((*Schemas.UsuarioProyecto)(nil)).
		ColumnExpr("array_agg(proyecto_id)")
	if libreta > 0 && filter == "" {
		q1 = q1.Where("libreta_id=?", libreta)
	}
	if tags != nil && len(tags) > 0 && filter == "" {
		for _, v := range tags {
			if strings.TrimSpace(v) != "" {
				//if i == 0 {
				q1 = q1.Where("?=ANY (etiquetas)", strings.TrimSpace(v))
				//	continue
				//}
				//q1 = q1.WhereOr("?=ANY (etiquetas)", strings.TrimSpace(v))
			}
		}
	}

	err := q1.Where("usuario_id=?", userId).Select(pg.Array(&ids))
	if err != nil {
		return nil, err
	}

	if len(ids) == 0 && filter == "" {
		return nil, errors.New("No hay ningun proyecto")
	}
	var res []Schemas.Proyecto
	q2 := db.Model(&res).Column("proyecto.*", "Creador.email", "Creador.nombre",
		"Creador.foto_perfil",
		"Participantes", "Archivos",
		"Actividades",
		"Participantes.Usuario").
		Relation("Participantes", func(query *orm.Query) (*orm.Query, error) {
		return query.Where("usuario_proyecto.estatus=?", Constants.USUARIO_ACTIVO), nil
	})

	if len(ids) > 0 && filter == "" {
		q2 = q2.Where("proyecto.id IN (?)", pg.In(ids))
	}
	if filter != "" {
		filter = "%" + filter + "%"
		q2 = q2.WhereOr("proyecto.nombre ilike ?", filter).
			WhereOr("proyecto.descripcion  ilike ?", filter)
	}
	err = q2.Select()
	if err != nil {
		return nil, err
	}

	return res, nil
}

func CreateProyect(proyecto *Schemas.Proyecto, participantes []string, libretaP int64,
	etiquetas, archivos []string) error {
	db := models.GetDataBase()
	var existente Schemas.Proyecto
	err := db.Model(&existente).Where("creador_id=?", proyecto.CreadorID).
		Where("nombre=?", strings.TrimSpace(proyecto.Nombre)).First()
	if err == nil {
		return errors.New("Ya tiene un proyecto con ese nombre")
	}
	if err != nil && err != pg.ErrNoRows {
		return err
	}

	tx, err := db.Begin()
	if err != nil {
		return err
	}
	proyecto.Id = xid.New().String()
	err = tx.Insert(proyecto)
	if err != nil {
		tx.Rollback()
		return err
	}
	var libretas []Schemas.Libreta
	err = tx.Model(&libretas).
		Where("libreta.default = ?", true).
		Where("usuario_id IN (?)", pg.In(participantes)).Select()
	if err != nil {
		tx.Rollback()
		return err
	}

	auxLibretas := make(map[string]Schemas.Libreta, 0)

	for _, v := range libretas {
		auxLibretas[v.UsuarioID] = v
	}

	for i := range participantes {
		aux := Schemas.UsuarioProyecto{
			Estatus:    Constants.USUARIO_ACTIVO,
			ProyectoID: proyecto.Id,
			Id:         xid.New().String(),
			UsuarioID:  participantes[i],
			CreatedAt:  time.Now(),
			UpdatedAt:  time.Now(),}
		if participantes[i] == proyecto.CreadorID {
			aux.LibretaID = libretaP
			aux.Etiquetas = etiquetas
		} else {
			aux.LibretaID = auxLibretas[participantes[i]].Id
		}
		err := tx.Insert(&aux)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	if len(archivos) > 0 {
		_, err = tx.Model((*Schemas.ArchivoReunion)(nil)).
			Set("proyecto_id=?", proyecto.Id).
			Set("reunion_id=?", nil).
			Where("id IN (?)", pg.In(archivos)).Update()
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	tx.Commit()
	return nil
}

func GetParticipantes(proyect, user string) ([]Schemas.UsuarioProyecto, error) {
	db := models.GetDataBase()
	var up []Schemas.UsuarioProyecto
	err := db.Model(&up).Column("usuario_proyecto.*", "Usuario").
		Where("proyecto_id=?", proyect).
		Where("usuario_proyecto.estatus=?", Constants.USUARIO_ACTIVO).
		Select(&up)
	return up, err
}

func UpdateProject(proyecto *Schemas.Proyecto, usuario string, libretaP int64,
	etiquetas []string, participantes, archivos []string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	if proyecto != nil && proyecto.CreadorID == usuario {
		_, err = tx.Model(proyecto).Set("nombre=?", proyecto.Nombre).
			Set("descripcion=?", proyecto.Descripcion).
			Set("updated_at=?", time.Now()).
			Where("id=?", proyecto.Id).
			Update()
		if err != nil {
			tx.Rollback()
			return err
		}
		var previos []Schemas.UsuarioProyecto
		tx.Model(&previos).Where("proyecto_id=?", proyecto.Id).Select()

		var aux Schemas.UsuarioProyecto
		_, err = tx.Model(&aux).
			Set("estatus=?", Constants.INACTIVO).
			Where("proyecto_id=?", proyecto.Id).
			Where("usuario_id!=?", proyecto.CreadorID).Update()
		for _, v := range participantes {
			listo := false
			for _, w := range previos {
				if v == w.UsuarioID {
					_, err = tx.Model(&aux).Set("estatus=?", Constants.ACTIVO).
						Where("proyecto_id=?", proyecto.Id).
						Where("usuario_id=?", v).Update()
					listo = true
					break
				}
			}

			if !listo {
				nuevo := Schemas.UsuarioProyecto{
					Id:         xid.New().String(),
					ProyectoID: proyecto.Id,
					UsuarioID:  v,
					CreatedAt:  time.Now()}
				err = tx.Insert(&nuevo)
			}
		}

	}
	up := Schemas.UsuarioProyecto{
		ProyectoID: proyecto.Id,
		UsuarioID:  usuario,
		UpdatedAt:  time.Now(),
		LibretaID:  libretaP,
		Etiquetas:  etiquetas,
	}
	q := tx.Model(&up)
	if (up.LibretaID > 0) {
		q = q.Set("libreta_id=?", up.LibretaID)
	}
	_, err = q.
		Set("updated_at=?", time.Now()).
		Where("proyecto_id=?", up.ProyectoID).
		Where("usuario_id=?", usuario).
		Update()
	if err != nil {
		fmt.Println("err 21", err)
		tx.Rollback()
		return err
	}
	if etiquetas != nil && len(etiquetas) > 0 {
		params := make([]string, 0, len(etiquetas))
		for i := range etiquetas {
			params = append(params, fmt.Sprintf("'%v'", etiquetas[i]))
		}
		query := fmt.Sprintf("UPDATE usuario_proyectos SET etiquetas = ARRAY[%s] where proyecto_id='%s' and usuario_id='%s'", strings.Join(params, ", "), proyecto.Id, usuario)
		_, err = tx.Exec(query, nil)

		if err != nil {
			fmt.Println("err 2", err)
			tx.Rollback()
			return err
		}
	}

	if len(archivos) > 0 {
		_, err = tx.Model((*Schemas.ArchivoReunion)(nil)).
			Set("proyecto_id=?", proyecto.Id).
			Set("reunion_id=?", nil).
			Where("id IN (?)", pg.In(archivos)).Update()
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	tx.Commit()
	return nil
}

func DeleteProject(id, usuario string) (error) {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	var lib Schemas.Proyecto
	err = tx.Model(&lib).Where("id=?", id).First()
	if err != nil {
		tx.Rollback()
		return err
	}
	var res []struct {
		Actividades int64
		ProyectoId  string
	}
	err = tx.Model((*Schemas.Actividad)(nil)).
		Column("proyecto_id").
		ColumnExpr("count(*) AS actividades").
		Group("proyecto_id").
		Where("proyecto_id=?", id).
		Select(&res)
	if err != nil {
		tx.Rollback()
		return err
	}
	if res != nil && res[0].Actividades > 0 {
		tx.Rollback()
		return errors.New("El proyecto tiene actividades asignadas, no se puede eliminar")
	}

	if lib.CreadorID != usuario {
		err = EliminarParticipante(usuario, id, tx)
		if err != nil {
			tx.Rollback()
			return err
		}
		tx.Commit()
		return nil
	}
	_, err = tx.Model(&Schemas.UsuarioProyecto{}).Where("proyecto_id=?", id).Delete()
	if err != nil {
		tx.Rollback()
		return err
	}
	r, err := tx.Model(&Schemas.Proyecto{}).Where("id=?", id).Delete()
	if err != nil {
		tx.Rollback()
		return err
	}
	if r.RowsAffected() != 1 {
		tx.Rollback()
		return errors.New("No se pudo eliminar proyecto")
	}
	tx.Commit()
	return nil
}

func EliminarParticipante(participante, proyecto string, tx *pg.Tx) error {
	var commit bool
	var err error
	if tx == nil {
		db := models.GetDataBase()
		tx, err = db.Begin()
		if err != nil {
			return err
		}
	}
	_, err = tx.Model(&Schemas.UsuarioProyecto{}).
		Set("estatus=?", Constants.INACTIVO).
		Where("usuario_id=?", participante).
		Where("proyecto_id=?", proyecto).Update()
	if err != nil {
		if commit {
			tx.Rollback()
		}
		return err
	}

	if commit {
		return tx.Commit()
	}
	return nil
}

func AddParticipante(proyecto, usuario, participante string) (error, *Schemas.UsuarioProyecto) {
	db := models.GetDataBase()
	var user Schemas.UsuarioProyecto
	if usuario == participante {
		return errors.New("El usuario ya esta en el proyecto"), nil
	}
	err := db.Model(&user).Where("proyecto_id=?", proyecto).
		Where("usuario_id=?", usuario).First()
	if err != nil {
		if err == pg.ErrNoRows {
			return errors.New("No tiene permiso para realizar esta accion"), nil
		}
		return err, nil
	}
	var auxLibreta Schemas.Libreta
	err = db.Model(&auxLibreta).Where("usuario_id=?", participante, "").
		Where("libreta.default = ?", true).
		First()

	err = db.Model(&user).Where("proyecto_id=?", proyecto).
		Where("usuario_id=?", participante).First()
	if err != nil {
		if err != pg.ErrNoRows {
			return err, nil
		}
	}
	if user.UsuarioID == participante {
		if user.Estatus == Constants.USUARIO_ACTIVO {
			return errors.New("El usuario ya esta en el proyecto"), nil
		}
		_, err = db.Model(&user).Set("estatus=?", Constants.USUARIO_ACTIVO).
			Set("libreta_id=?",auxLibreta.Id).
			Where("id=?", user.Id).
			Update()
		if err != nil {
			return err, nil
		}
		return nil, &user
	}

	nuevo := Schemas.UsuarioProyecto{UsuarioID: participante,
		Estatus: Constants.USUARIO_ACTIVO,
		CreatedAt: time.Now(),
		LibretaID: auxLibreta.Id,
		ProyectoID: proyecto,
		Id: xid.New().String()}
	return db.Insert(&nuevo), &nuevo
}

func ToggleFavorito(proyecto, usuario string) (error) {
	db := models.GetDataBase()
	var user Schemas.UsuarioProyecto
	err := db.Model(&user).Where("proyecto_id=?", proyecto).
		Where("usuario_id=?", usuario).First()
	if err != nil {
		if err == pg.ErrNoRows {
			return errors.New("No tiene permiso para realizar esta accion")
		}
		return err
	}

	_, err = db.Model(&user).Set("favorito=?", !user.Favorito).Where("id=?", user.Id).Update()
	if err != nil {
		return err
	}
	return nil
}

func ClasificarProyecto(usuario, proyecto string, libreta int64, etiquetas []string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	var participante Schemas.UsuarioProyecto
	_, err = tx.Model(&participante).
		Where("proyecto_id=?", proyecto).
		Where("usuario_id=?", usuario).
		Set("libreta_id=?", libreta).
		Set("etiquetas=?", pg.Array(etiquetas)).
		Update()
	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}

func RemoveParticipanteProyect(proyecto, participante, userID string) (error, *Schemas.UsuarioProyecto) {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err, nil
	}
	var user Schemas.UsuarioProyecto
	err = tx.Model(&user).Column("usuario_proyecto.*", "Usuario").Where("proyecto_id=?", proyecto).
		Where("usuario_id=?", userID).First()
	if err != nil {
		tx.Rollback()
		if err == pg.ErrNoRows {
			return errors.New("No tiene permiso para realizar esta accion"), nil
		}
		return err, nil
	}

	r, err := tx.Model((*Schemas.UsuarioProyecto)(nil)).
		Set("estatus=?", Constants.USUARIO_INACTIVO).
		Where("usuario_id=?", participante).
		Where("proyecto_id=?", proyecto).Update()
	if err != nil {
		tx.Rollback()
		return err, nil
	}

	if r.RowsAffected() != 1 {
		tx.Rollback()
		return errors.New("No se pudo realizar la operacion"), nil
	}
	return tx.Commit(), &user
}

func CambiarEstatusProject(proyect, user, estatus string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	var previo Schemas.Proyecto
	err = tx.Model(&previo).Where("creador_id=?", user).
		Where("id=?", proyect).First()
	if err != nil {
		tx.Rollback()
		if err == pg.ErrNoRows {
			return errors.New("No tiene permiso para esta acción")
		}
		return err
	}

	_, err = tx.Model(&previo).Set("estatus=?", estatus).Where("id=?", proyect).Update()
	if err != nil {
		tx.Rollback()
		return err
	}
	if estatus == Constants.PROYECTO_FINALIZADO {
		_, err = tx.Model((*Schemas.Actividad)(nil)).Set("estatus=?", Constants.ACTIVIDAD_FINALIZADA).
			Where("estatus=?", Constants.ACTIVIDAD_EN_PROCESO).
			Where("proyecto_id=?", proyect).Update()
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit()
}
