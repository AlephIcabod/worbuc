package Querys

import (
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/models"
	"github.com/go-pg/pg"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
)

func GetNotifications(user string) ([]*Schemas.Notification, error) {
	db := models.GetDataBase()
	var res []*Schemas.Notification
	_,err := db.Query(&res,`select DISTINCT on (date_trunc('day',n.fecha),n.adicional,n.texto) *  from notifications n
          where receptor_id = ? and checked is null
          order by date_trunc('day',n.fecha) DESC`,user)
	if err != nil {
		if err == pg.ErrNoRows {
			return []*Schemas.Notification{}, nil
		}
		return nil, err
	}
	if len(res) < 10 {
		var aux []*Schemas.Notification
		err = db.Model(&aux).Column("notification.*").
			Where("receptor_id=?", user).
			Where("checked = ?", true).
			Order("notification.fecha DESC").
			Limit(5).
			Select()
		if err != nil {
			return res, nil
		}
		res = append(res, aux...)
	}
	return res, nil
}

func CreateNotification(notification ...*Schemas.Notification) error {
	db := models.GetDataBase()
	var err error
	for _, v := range notification {
		Functions.NotifyUser(v, v.ReceptorID)
		err = db.Insert(v)
	}

	return err
}

func CheckNotification(id, user string) (error) {
	db := models.GetDataBase()
	_, err := db.Model((*Schemas.Notification)(nil)).
		Set("checked=?", true).
		Where("receptor_id=?", user).
		Where("id=?", id).Update()
	if err != nil {
		return err
	}
	return nil
}
