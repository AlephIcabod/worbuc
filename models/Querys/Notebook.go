package Querys

import (
	"errors"
	"strings"
	"github.com/go-pg/pg"
	"bitbucket.org/AlephIcabod/worbuc/models"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"fmt"
)

func CreateNotebook(nb *Schemas.Libreta) error {
	db := models.GetDataBase()
	var existe Schemas.Libreta
	err := db.Model(&existe).Where("usuario_id=?", nb.UsuarioID).
		Where("nombre=?", strings.TrimSpace(nb.Nombre)).First()
	if err != nil {
		if err != pg.ErrNoRows {
			return err
		}
		return db.Insert(nb)
	}

	return errors.New("Ya hay una libreta con ese nombre")
}

func GetNotebook(id int64) (*Schemas.Libreta, error) {
	db := models.GetDataBase()
	var res Schemas.Libreta
	err := db.Model(&res).Where("id=?", id).First()
	return &res, err

}

func UpdateNotebook(id int64, notebook *Schemas.Libreta) error {
	db := models.GetDataBase()
	_, err := db.Model(notebook).
		Set("nombre=?", notebook.Nombre).
		Set("descripcion=?", notebook.Descripcion).
		Where("id=?", id).Update()

	return err
}

func DeleteNotebook(id int64, user string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	var lib Schemas.Libreta
	err = tx.Model(&lib).Where("id=?", id).First()
	if err != nil {
		tx.Rollback()
		return err
	}
	if lib.UsuarioID != user {
		tx.Rollback()
		return errors.New("No tiene permisos sobre esa libreta")
	}

	var res []struct {
		Proyectos int64
		LibretaId int64
		Reuniones int64
	}
	err = tx.Model((*Schemas.UsuarioProyecto)(nil)).
		Column("libreta_id").
		ColumnExpr("count(*) AS proyectos").
		Group("libreta_id").
		Where("libreta_id=?", id).
		Select(&res)
	if err != nil {
		return err
	}
	if res != nil && res[0].Proyectos > 0 {
		tx.Rollback()
		return errors.New("La libreta tiene proyectos asignados, no se puede borrar")
	}

	err = tx.Model((*Schemas.Participante)(nil)).
		Column("libreta_id").
		ColumnExpr("count(*) AS reuniones").
		Group("libreta_id").
		Where("libreta_id=?", id).
		Select(&res)
	if err != nil {
		tx.Rollback()
		return err
	}
	if res != nil && res[0].Reuniones > 0 {
		tx.Rollback()
		return errors.New("La libreta tiene reuniones asignadas, no se puede borrar")
	}

	r, err := tx.Model(&Schemas.Libreta{}).Where("id=?", id).Delete()
	if err != nil {
		tx.Rollback()
		return err
	}

	if r.RowsAffected() != 1 {
		tx.Rollback()
		return errors.New("No se pudo eliminar libreta")
	}
	tx.Commit()
	return nil
}

func GetNotebooks(userID string) ([]Schemas.Libreta, error) {
	var res []Schemas.Libreta
	db := models.GetDataBase()

	err := db.Model(&res).Where("usuario_id=?", userID).
		Select()
	if err != nil {
		return nil, err
	}

	return res, nil
}

func UpdateDefaultNotebook(id int64, user string) (error) {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	_, err = tx.Model((*Schemas.Libreta)(nil)).
		Set(`"default" = ?`, false).
		Where(`"default" = ?`, true).
		Where("usuario_id=?", user).Update()
	if err != nil {
		fmt.Println(err)
		tx.Rollback()
		return err
	}
	_, err = tx.Model((*Schemas.Libreta)(nil)).
		Set(`"default" = ?`, true).
		Where("id=?", id).Update()
	if err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit()
}
