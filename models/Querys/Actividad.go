package Querys

import (
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/models"
	"github.com/go-pg/pg"
	"errors"
	"github.com/go-pg/pg/orm"
	"time"
)

func CreateActividad(proyecto, usuario string, actividad *Schemas.Actividad) error {
	db := models.GetDataBase()
	var user Schemas.UsuarioProyecto
	err := db.Model(&user).Where("proyecto_id=?", proyecto).
		Where("usuario_id=?", usuario).First()
	if err != nil {
		if err == pg.ErrNoRows {
			return errors.New("No tiene permiso para realizar esta accion")
		}
		return err
	}
	prev := Schemas.Actividad{}
	err = db.Model(&prev).Where("proyecto_id=?", proyecto).
		Where("nombre ilike ?", actividad.Nombre).First()
	if err != nil && err != pg.ErrNoRows {
		return err
	}
	if &prev != nil && prev.Id != "" {
		return errors.New("Ya existe una actividad con el mismo nombre en este proyecto")
	}
	return db.Insert(actividad)
}

func GetActividad(actividad, user string) (*Schemas.Actividad, error) {
	db := models.GetDataBase()
	var res Schemas.Actividad
	var aux Schemas.UsuarioProyecto
	err := db.Model(&res).Column("actividad.*", "Proyecto", "Tareas").
		Relation("Tareas", func(query *orm.Query) (*orm.Query, error) {
		return query, nil
	}).
		Where("actividad.id=?", actividad).First()
	if err != nil {
		return nil, err
	}
	err = db.Model(&aux).Where("usuario_id=?", user).
		Where("proyecto_id=?", res.ProyectoID).First()
	if err != nil {
		if err == pg.ErrNoRows {
			return nil, errors.New("No tiene permiso para esta accion")
		}
		return nil, err
	}

	return &res, nil
}

func UpdateActividad(act *Schemas.Actividad, user string) error {
	db := models.GetDataBase()
	prev, err := GetActividad(act.Id, user)
	if err != nil {
		return err
	}
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	prev.Nombre = act.Nombre
	prev.Descripcion = act.Descripcion
	prev.FechaProgramada = act.FechaProgramada
	r, err := tx.Model(act).Set("nombre=?", act.Nombre).
		Set("descripcion=?", act.Descripcion).
		Set("fecha_programada=?", act.FechaProgramada).
		Set("updated_at=?", time.Now()).Where("id=?", act.Id).
		Update()
	if err != nil {
		tx.Rollback()
		return err
	}
	if r.RowsAffected() > 1 {
		tx.Rollback()
		return errors.New("Error al actualizar se actualizo mas de un registro")
	}
	*act = *prev
	return tx.Commit()
}

func DeleteActividad(id string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	r, err := tx.Model((*Schemas.Actividad)(nil)).Where("id=?", id).Delete()
	if err != nil {
		tx.Rollback()
		return err
	}
	if r.RowsAffected() != 1 {
		tx.Rollback()
		return errors.New("No se pudo procesar la transaccion")
	}
	return tx.Commit()
}
