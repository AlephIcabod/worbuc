package Querys

import (
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/models"
	"github.com/go-pg/pg"
	"errors"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"fmt"
	"github.com/rs/xid"
	"github.com/go-pg/pg/orm"
	"time"
)

func CreateTarea(tarea *Schemas.Tarea, proyecto string, archivos []string) error {
	db := models.GetDataBase()
	var creador Schemas.UsuarioProyecto
	err := db.Model(&creador).Where("usuario_id=?", tarea.CreadorID).First()
	if err != nil {
		if err == pg.ErrNoRows {
			return errors.New("No tiene permiso para esta accion")
		}
		return err
	}
	tarea.CreadorID = creador.Id
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	err = tx.Insert(tarea)
	if err != nil {
		tx.Rollback()
		return err
	}
	for i := range tarea.Responsables {
		err = tx.Insert(tarea.Responsables[i])
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	for i := range tarea.Supervisores {
		err = tx.Insert(tarea.Supervisores[i])
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	if len(tarea.Archivos) > 0 {
		for i := range tarea.Archivos {
			err = tx.Insert(tarea.Archivos[i])
			if err != nil {
				tx.Rollback()
				return err
			}
		}
		_, err = tx.Model((*Schemas.ArchivoReunion)(nil)).
			Set("proyecto_id=?", proyecto).
			Set("reunion_id=?", nil).
			Where("id IN (?)", pg.In(archivos)).Update()
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}

func GetTarea(id, user string) (*Schemas.Tarea, error) {
	db := models.GetDataBase()
	var res Schemas.Tarea
	err := db.Model(&res).Column("tarea.*",
		"Creador",
		"Creador.Usuario",
		"Responsables", "Responsables.Usuario", "Responsables.Usuario.Usuario",
		"Supervisores", "Supervisores.Usuario", "Supervisores.Usuario.Usuario",
		"Actividad", "Actividad.Proyecto", "Actividad.Proyecto.Archivos",
		"Actividad.Proyecto.Participantes", "Actividad.Proyecto.Participantes.Usuario",
		"Actividad.Proyecto.Actividades",
		"Archivos",
		"Archivos.*",
		"Archivos.Archivo",
		"Comentarios",
		"Comentarios.Participante",
		"Comentarios.Participante.Usuario",
	).
		Relation("Responsables", func(query *orm.Query) (*orm.Query, error) {
		return query.Where("rol=?", Constants.ROL_REPONSABLE), nil
	}).
		Relation("Supervisores", func(query *orm.Query) (*orm.Query, error) {
		return query.Where("rol=?", Constants.ROL_SUPERVISOR), nil
	}).
		Relation("Comentarios", func(query *orm.Query) (*orm.Query, error) {
		return query.Order("fecha ASC"), nil
	}).
		Where("tarea.id=?", id).First()
	if err != nil {
		return nil, err
	}
	permiso := false

	for _, v := range res.Actividad.Proyecto.Participantes {
		if v.UsuarioID == user {
			permiso = true
			break
		}
	}

	if !permiso {
		return nil, errors.New("No tiene permiso para ver esta tarea")
	}

	return &res, nil
}

func CreateComentario(comentario *Schemas.ComentarioTarea, user string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	var tarea Schemas.Tarea
	err = tx.Model(&tarea).
		Column("tarea.*", "Actividad", "Actividad.Proyecto").
		Where("tarea.id=?", comentario.TareaID).First()
	if err != nil {
		tx.Rollback()
		return err
	}
	var pAux Schemas.UsuarioProyecto
	err = tx.Model(&pAux).
		Where("proyecto_id=?", tarea.Actividad.ProyectoID).
		Where("usuario_id=?", user).First()
	if err != nil {
		tx.Rollback()
		if err == pg.ErrNoRows {
			return errors.New("No tiene permiso para esta accion")
		}
		return err
	}

	if comentario.AdjuntoID != "" {
		archTarea := Schemas.ArchivoTarea{Id: xid.New().String(),
			TareaID: tarea.Id,
			ArchivoID: comentario.AdjuntoID,
		}
		err = tx.Insert(&archTarea)
		if err != nil {
			tx.Rollback()
			return err
		}
		res, err := tx.Model((*Schemas.ArchivoReunion)(nil)).
			Set("reunion_id=?", nil).
			Set("proyecto_id=?", pAux.ProyectoID).
			Where("id=?", comentario.AdjuntoID).Update()
		fmt.Println(res.RowsAffected())
		if err != nil {
			tx.Rollback()
			return err
		}
		comentario.AdjuntoID = archTarea.Id
	}

	comentario.ParticipanteID = pAux.Id
	comentario.Estatus = Constants.ACTIVO
	err = tx.Insert(comentario)
	if err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit()
}

func GetComentario(id string) (*Schemas.ComentarioTarea, error) {
	var res Schemas.ComentarioTarea
	db := models.GetDataBase()
	err := db.Model(&res).Column("comentario_tarea.*",
		"Participante",
		"Participante.Usuario",
		"Tarea",
		"Tarea.Creador",
		"Adjunto").
		Where("comentario_tarea.id=?", id).First()
	if err != nil {

	}
	return &res, nil
}

func UpdateComentario(comentario *Schemas.ComentarioTarea, user string) error {
	db := models.GetDataBase()
	var tarea Schemas.Tarea
	var previo Schemas.ComentarioTarea
	err := db.Model(&previo).Where("id=?", comentario.Id).First()
	if err != nil {
		return err
	}
	err = db.Model(&tarea).
		Column("tarea.*", "Actividad", "Actividad.Proyecto").
		Where("tarea.id=?", comentario.TareaID).First()
	if err != nil {
		return err
	}
	var pAux Schemas.UsuarioProyecto
	err = db.Model(&pAux).
		Where("proyecto_id=?", tarea.Actividad.ProyectoID).
		Where("usuario_id=?", user).First()
	if err != nil {
		if err == pg.ErrNoRows {
			return errors.New("No tiene permiso para esta accion")
		}
		return err
	}
	if previo.ParticipanteID != pAux.Id {
		return errors.New("No tiene permiso para esta accion")
	}
	comentario.ParticipanteID = previo.ParticipanteID
	comentario.Fecha = previo.Fecha
	comentario.OriginalID = previo.OriginalID
	err = db.Update(comentario)
	if err != nil {
		return err
	}
	return nil
}

func UpdateTarea(tarea *Schemas.Tarea, usuario string) error {
	db := models.GetDataBase()

	previo, err := GetTarea(tarea.Id, usuario)
	if err != nil {
		return err
	}
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	if previo.Estatus == Constants.TAREA_FINALIZADA || previo.Estatus == Constants.TAREA_CANCELADA {
		return errors.New("No puede realizar esa accion con esta tarea")
	}
	admin := false
	for _, v := range previo.Supervisores {
		if v.Usuario.UsuarioID == usuario {
			admin = true
		}
	}
	if !admin && previo.Creador.UsuarioID != usuario {
		return errors.New("No tiene permiso para esta accion")
	}

	r, err := tx.Model(tarea).
		Set("nombre=?", tarea.Nombre).
		Set("descripcion=?", tarea.Descripcion).
		Set("actividad_id=?", tarea.ActividadID).
		Set("fecha_programada=?", tarea.FechaProgramada).
		Set("prioridad=?", tarea.Prioridad).
		Where("id=?", tarea.Id).
		Update()

	if err != nil || r.RowsAffected() != 1 {
		tx.Rollback()
		return err
	}

	_, err = tx.Model((*Schemas.ParticipanteTarea)(nil)).Where("tarea_id=?", tarea.Id).Delete()
	if err != nil {
		tx.Rollback()
		return err
	}
	for i := range tarea.Responsables {
		err = tx.Insert(tarea.Responsables[i])
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	for i := range tarea.Supervisores {
		err = tx.Insert(tarea.Supervisores[i])
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit()

}

func CambiarEstatusTarea(id, user, nuevoEstatus string, onlyAdmin bool) (*Schemas.Tarea, error) {
	db := models.GetDataBase()
	tarea, err := GetTarea(id, user)
	if err != nil {
		return nil, err
	}
	tx, err := db.Begin()
	if err != nil {
		return nil, err
	}
	if tarea.Estatus == Constants.TAREA_FINALIZADA || tarea.Estatus == Constants.TAREA_CANCELADA {
		return nil, errors.New("No puede realizar esa accion con esta tarea")
	}
	if onlyAdmin {
		admin := false
		for _, v := range tarea.Supervisores {
			if v.Usuario.UsuarioID == user {
				admin = true
			}
		}
		if !admin && tarea.Creador.UsuarioID != user {
			return nil, errors.New("No tiene permiso para esta accion")
		}
	}
	q := tx.Model(tarea).
		Set("estatus=?", nuevoEstatus)
	if nuevoEstatus == Constants.TAREA_FINALIZADA {
		q = q.Set("fecha_finalizacion=?", time.Now().UTC())
	}
	r, err := q.
		Where("id=?", id).
		Update()

	if err != nil || r.RowsAffected() != 1 {
		tx.Rollback()
		return nil, err
	}
	return tarea, tx.Commit()
}

func GetTareasMes(user string, mes, anio int32) ([]Schemas.Tarea, error) {
	db := models.GetDataBase()
	var res []Schemas.Tarea
	ids := make([]string, 0)
	ids2 := make([]string, 0, 0)
	q2 := db.Model((*Schemas.UsuarioProyecto)(nil)).
		ColumnExpr("array_agg(id)").
		Where("usuario_id=?", user)
	err := q2.Select(pg.Array(&ids2))
	if err != nil {

		if err != pg.ErrNoRows {
			return nil, err
		}
		return nil, nil
	}
	q2 = db.Model((*Schemas.ParticipanteTarea)(nil)).
		ColumnExpr("array_agg(tarea_id)").
		Where("usuario_id IN (?)", pg.In(ids2))
	err = q2.Select(pg.Array(&ids))
	if err != nil {

		if err != pg.ErrNoRows {
			return nil, err
		}
		return nil, nil
	}

	err = db.Model(&res).Column("tarea.*",
		"Creador",
		"Creador.Usuario",
		"Responsables", "Responsables.Usuario", "Responsables.Usuario.Usuario",
		"Supervisores", "Supervisores.Usuario", "Supervisores.Usuario.Usuario",
		"Actividad", "Actividad.Proyecto",
	).
		Relation("Responsables", func(query *orm.Query) (*orm.Query, error) {
		return query.Where("rol=?", Constants.ROL_REPONSABLE), nil
	}).
		Relation("Supervisores", func(query *orm.Query) (*orm.Query, error) {
		return query.Where("rol=?", Constants.ROL_SUPERVISOR), nil
	}).
		Where("tarea.id IN (?)", pg.In(ids)).
		Where("EXTRACT (MONTH FROM tarea.fecha_programada) = ?", mes).
		Where("EXTRACT (YEAR FROM tarea.fecha_programada) = ?", anio).Select()

	if err != nil {

		return nil, err
	}

	return res, nil

}

func RemoveComentario(comentario, user string) error {
	db := models.GetDataBase()
	var tarea Schemas.Tarea
	var previo Schemas.ComentarioTarea
	err := db.Model(&previo).Where("id=?", comentario).First()
	if err != nil {
		return err
	}
	err = db.Model(&tarea).
		Column("tarea.*", "Actividad", "Actividad.Proyecto").
		Where("tarea.id=?", previo.TareaID).First()
	if err != nil {
		return err
	}
	var pAux Schemas.UsuarioProyecto
	err = db.Model(&pAux).
		Where("proyecto_id=?", tarea.Actividad.ProyectoID).
		Where("usuario_id=?", user).First()
	if err != nil {
		if err == pg.ErrNoRows {
			return errors.New("No tiene permiso para esta accion")
		}
		return err
	}
	if previo.ParticipanteID != pAux.Id {
		return errors.New("No tiene permiso para esta accion")
	}
	_, err = db.Model((*Schemas.ComentarioTarea)(nil)).
		Set("estatus=?", Constants.ELIMINADO).
		Where("id=?", comentario).
		WhereOr("original_id=?", comentario).Update()
	if err != nil {
		return err
	}
	return nil
}
