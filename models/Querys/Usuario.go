package Querys

import (
	"time"
	"fmt"
	"github.com/go-pg/pg/orm"
	"github.com/rs/xid"
	"errors"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/models"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"github.com/go-pg/pg"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/config"
)

func Login(email, password string) (*Schemas.Usuario, error) {
	var user Schemas.Usuario
	var err error
	db := models.GetDataBase()
	err = db.Model(&user).Where("email=?", email).
		Where("estatus=?", Constants.REGISTRADO).
		First()
	if err != nil {
		if err == pg.ErrNoRows {
			return nil, errors.New("Correo no encontrado")
		}
		return nil, err
	}
	p1, _ := Functions.Decrypt(user.Password)

	if p1 != password {
		return nil, errors.New("Contraseña incorrecta")
	}
	return &user, err
}

func GetUser(id string) (*Schemas.Usuario, error) {
	var user Schemas.Usuario
	db := models.GetDataBase()
	err := db.Model(&user).Where("id=?", id).
		Column("usuario.*", "Libretas").Relation("Libretas", func(query *orm.Query) (*orm.Query, error) {
		return query, nil
	}).First()
	if err != nil {
		return nil, err
	}
	var proyects []*Schemas.Proyecto
	err = db.Model(&proyects).Where("creador_id=?", id).Select()
	if err != nil {
		return nil, err
	}
	user.Direccion, err = getDirecction(user.Id, db)
	if err != nil {
		return nil, err
	}
	user.Telefonos, err = getTels(user.Id, db)
	if err != nil {
		return nil, err
	}
	user.Empleo, err = getEmpleo(user.Id, db)
	if err != nil {
		return nil, err
	}
	user.Educacion, err = getEducation(user.Id, db)
	user.Proyectos = proyects

	return &user, err
}

func GetUserByEmail(email string) (*Schemas.Usuario, error) {
	var user Schemas.Usuario
	db := models.GetDataBase()
	err := db.Model(&user).Where("email=?", email).
		First()
	if err != nil {
		if err == pg.ErrNoRows {
			return nil, errors.New(Constants.ErrorNoResultsUser)
		}
		return nil, err
	}
	return &user, nil
}

func Create(usuario *Schemas.Usuario) error {
	usuario.Id = xid.New().String()
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	r, err := tx.Model(usuario).OnConflict("DO NOTHING").Insert()
	if err != nil {
		fmt.Println("Error insertand usuario", err)
		tx.Rollback()
		return err
	}
	if r.RowsAffected() != 1 {
		tx.Commit()
		return nil
	}

	libreta := Schemas.Libreta{Nombre: "Libreta Personal",
		Usuario:     usuario,
		UsuarioID:   usuario.Id,
		Estatus:     Constants.ACTIVO,
		Default:     true,
		Descripcion: "Tu primer libreta en la que se cargarán tus reuniones, proyectos y tareas no organizadas",
	}
	err = tx.Insert(&libreta)
	if err != nil {
		tx.Rollback()
		return err
	}
	libreta.Usuario = nil

	if usuario.Estatus == Constants.REGISTRADO {
		periodPrueba := config.GetConfig().DiasPrueba
		vigencia := time.Now().UTC().Add(time.Hour * 24 * time.Duration(periodPrueba)).UTC()
		planPrueba := config.GetConfig().PlanPrueba
		plan := Schemas.PlanUsuario{Id: xid.New().String(), UsuarioID: usuario.Id, PlanID: planPrueba,
			Vigencia:  &vigencia,
			Renovar:   false,
			CreatedAt: time.Now()}
		err = tx.Insert(&plan)

		if err != nil {
			tx.Rollback()
			return err
		}
	}
	usuario.Libretas = []*Schemas.Libreta{&libreta}
	return tx.Commit()
}

func CreateOrUpdateSchoolAndJon(u *Schemas.Usuario) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	err = CreateOrUpdateJob(u, tx)
	if err != nil {
		fmt.Println("Error job")
		tx.Rollback()
		return err
	}
	err = CreateOrUpdateSchoolar(u, tx)
	if err != nil {
		tx.Rollback()
		return err
	}
	_, err = tx.Model(u).
		Set("escolaridad =?", u.Escolaridad).
		Where("id=?", u.Id).
		Update()
	if err != nil {
		fmt.Println("Error user")
		tx.Rollback()
		return err
	}
	tx.Commit()
	return nil
}

func CreateOrUpdateJob(u *Schemas.Usuario, tx *pg.Tx) error {
	var aux Schemas.Empleo
	var err error
	var commit bool
	if tx == nil {
		tx, err = models.GetDataBase().Begin()
		if err != nil {
			return err
		}
		commit = true
	}

	if u.Empleo == nil {
		_, err = tx.Model(&aux).Where("usuario_id= ?", u.Id).Delete()
		return err
	}
	u.Empleo.Empresa, err = FindOrCreateEmpresa(u.Empleo.Empresa.Nombre, u.Empleo.Empresa.Sector)
	if err != nil {
		return err
	}
	err = tx.Model(&aux).Where("usuario_id=?", u.Id).First()
	if err != nil {
		if err == pg.ErrNoRows {
			err = tx.Insert(u.Empleo)
		}
	} else {
		_, err = tx.Model(u.Empleo).
			Set("empresa_id =?", u.Empleo.Empresa.Id).
			Set("puesto =?", u.Empleo.Puesto).
			Set("updated_at =?", time.Now()).
			Where("usuario_id=?", u.Id).Update()

	}

	if commit {
		return tx.Commit()
	}
	return err
}

func CreateOrUpdateSchoolar(u *Schemas.Usuario, tx *pg.Tx) error {
	var aux Schemas.Educacion
	var err error
	var commit bool
	if tx == nil {
		tx, err = models.GetDataBase().Begin()
		if err != nil {
			return err
		}
		commit = true
	}

	if u.Educacion == nil {
		_, err = tx.Model(&aux).Where("usuario_id= ?", u.Id).Delete()
		return err
	}
	u.Educacion.Institucion, err = FindOrCreateSchool(u.Educacion.Institucion.Nombre)
	if err != nil {
		return err
	}
	err = tx.Model(&aux).Where("usuario_id=?", u.Id).First()
	if err != nil {
		if err == pg.ErrNoRows {
			err = tx.Insert(u.Educacion)
		}
	} else {
		_, err = tx.Model(u.Educacion).
			Set("institucion_id=?", u.Educacion.Institucion.Id).
			Set("periodo=?", u.Educacion.Periodo).
			Set("updated_at=?", time.Now()).
			Where("usuario_id=?", u.Id).Update()
	}
	_, err = tx.Model(u).
		Set("escolaridad=?", u.Escolaridad).
		Where("id=?", u.Id).Update()

	if commit {
		return tx.Commit()
	}

	return err
}

func CreateOrUpdateAddress(u *Schemas.Usuario, tx *pg.Tx) error {
	var aux Schemas.Direccion
	var err error
	var closeHere bool
	if tx == nil {
		tx, err = models.GetDataBase().Begin()
		if err != nil {
			return err
		}
		closeHere = true
	}
	if u.Direccion == nil {
		_, err = tx.Model(&aux).Where("usuario_id=?", u.Id).Delete()
		return err
	}
	err = tx.Model(&aux).Where("usuario_id=?", u.Id).First()
	fmt.Println("1", err)
	if err != nil {
		if err == pg.ErrNoRows {
			err = tx.Insert(u.Direccion)
			fmt.Println("insertar", err)
		}
	} else {
		if aux.PaisID == u.Direccion.PaisID {
			_, err = tx.Model(u.Direccion).
				Set("estado_id=?", u.Direccion.EstadoID).
				Set("poblacion=?", u.Direccion.Poblacion).
				Set("codigo_postal=?", u.Direccion.CodigoPostal).
				Set("updated_at=?", time.Now()).
				Where("usuario_id=?", u.Id).
				Update()
		} else {
			q := tx.Model(u.Direccion).
				Set("poblacion=?", u.Direccion.Poblacion).
				Set("pais_id=?", u.Direccion.PaisID).
				Set("codigo_postal=?", u.Direccion.CodigoPostal).
				Set("updated_at=?", time.Now())
			if aux.EstadoID != 0 {
				_, err = q.
					Set("estado_id=?", nil).
					Where("usuario_id=?", u.Id).
					Update()
			} else {
				_, err = q.
					Set("estado_id=?", u.Direccion.EstadoID).
					Where("usuario_id=?", u.Id).
					Update()
			}
		}
	}
	if closeHere {
		return tx.Commit()
	}

	return err
}

func UpdatePhoto(u *Schemas.Usuario, nuevo string) error {
	db := models.GetDataBase()
	_, err := db.Model(u).
		Set("foto_perfil =?", nuevo).
		Where("id=?", u.Id).
		Update()
	return err
}

func getDirecction(user string, db *pg.DB) (*Schemas.Direccion, error) {
	if db == nil {
		db = models.GetDataBase()
	}
	var direcc Schemas.Direccion
	err := db.Model(&direcc).Where("usuario_id=?", user).Select()
	if err != nil && err != pg.ErrNoRows {
		return nil, err
	}
	if err == pg.ErrNoRows {
		return nil, nil
	}
	return &direcc, nil
}

func getTels(user string, db *pg.DB) ([]*Schemas.Telefono, error) {
	if db == nil {
		db = models.GetDataBase()
	}
	var tels []*Schemas.Telefono
	err := db.Model(&tels).Where("usuario_id=?", user).Select()
	if err != nil && err != pg.ErrNoRows {
		return nil, err
	}
	return tels, nil
}

func getEmpleo(user string, db *pg.DB) (*Schemas.Empleo, error) {
	if db == nil {
		db = models.GetDataBase()
	}
	var emp Schemas.Empleo
	err := db.Model(&emp).Where("usuario_id=?", user).Select()
	if err != nil && err != pg.ErrNoRows {
		return nil, err
	}
	if err == pg.ErrNoRows {
		return nil, nil
	}
	if err == nil {
		var empresa Schemas.Empresa
		err = db.Model(&empresa).Where("id=?", emp.EmpresaID).Select()
		if err != nil && err != pg.ErrNoRows {
			return nil, err
		}

		emp.Empresa = &empresa
	}
	return &emp, nil
}

func getEducation(user string, db *pg.DB) (*Schemas.Educacion, error) {
	if db == nil {
		db = models.GetDataBase()
	}
	var emp Schemas.Educacion
	err := db.Model(&emp).Where("usuario_id=?", user).Select()
	if err != nil && err != pg.ErrNoRows {
		return nil, err
	}
	if err == pg.ErrNoRows {
		return nil, nil
	}
	if err == nil {
		var inst Schemas.Institucion
		err = db.Model(&inst).Where("id=?", emp.InstitucionID).Select()
		if err != nil && err != pg.ErrNoRows {
			return nil, err
		}
		emp.Institucion = &inst
	}
	return &emp, nil
}

func UpdateBasics(u *Schemas.Usuario, name, apPat, apMat, gen string, fecNac time.Time) error {
	db := models.GetDataBase()
	_, err := db.Model(u).
		Set("nombre =?", name).
		Set("apellido_paterno =?", apPat).
		Set("apellido_materno =?", apMat).
		Set("genero =?", gen).
		Set("fecha_nacimiento =?", fecNac).
		Where("id=?", u.Id).
		Update()
	return err
}

func UpdateUser(u *Schemas.Usuario, nuevo *Schemas.Usuario) error {
	db, err := models.GetDataBase().Begin()
	if err != nil {
		return err
	}
	crearPrueba := nuevo.Estatus == Constants.REGISTRADO && u.Estatus == Constants.PREREGISTRADO
	_, err = db.Model(u).
		Set("nombre =?", nuevo.Nombre).
		Set("apellido_paterno =?", nuevo.ApellidoPaterno).
		Set("apellido_materno =?", nuevo.ApellidoMaterno).
		Set("genero =?", nuevo.Genero).
		Set("estatus=?", nuevo.Estatus).
		Set("password=?", nuevo.Password).
		Where("id=?", u.Id).
		Update()

	if crearPrueba {
		periodPrueba := config.GetConfig().DiasPrueba
		vigencia := time.Now().UTC().Add(time.Hour * 24 * time.Duration(periodPrueba)).UTC()
		planPrueba := config.GetConfig().PlanPrueba
		plan := Schemas.PlanUsuario{Id: xid.New().String(), UsuarioID: u.Id, PlanID: planPrueba,
			Vigencia:  &vigencia,
			Renovar:   false,
			CreatedAt: time.Now()}
		err = db.Insert(&plan)
		if err != nil {
			db.Rollback()
			return err
		}
	}
	return db.Commit()
}

func CreateOrUpdatePhone(phone *Schemas.Telefono, id int) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	if id >= 0 {
		_, err = tx.Model(phone).
			Set("telefono=?", phone.Telefono).
			Set("tipo=?", phone.Tipo).
			Set("updated_at=?", time.Now()).
			Where("id=?", id).Update()

	} else {
		err = tx.Insert(phone)
	}
	if err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit()
}

func RemovePhone(id int64, user string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	phone := Schemas.Telefono{Id: id}
	if id >= 0 {
		res, err := tx.Model(&phone).Where("id = ?", id).
			Where("usuario_id=?", user).
			Delete()
		if err != nil {
			tx.Rollback()
			return err
		}
		if res.RowsAffected() == 1 {
			return tx.Commit()
		} else {
			tx.Rollback()
			return errors.New("Se ha eliminado mas de un registro")
		}
	}
	tx.Rollback()
	return errors.New("Id invalido")
}

func UpdateSettings(id, tipoPerfil, zonaHoraria string) error {
	db := models.GetDataBase()
	var usuario Schemas.Usuario
	_, err := db.Model(&usuario).
		Set("tipo_perfil=?", tipoPerfil).
		Set("zona_horaria=?", zonaHoraria).
		Where("id=?", id).
		Update()
	return err
}

func UpdatePassword(id, previo, nuevo string) error {
	var user Schemas.Usuario
	db := models.GetDataBase()
	err := db.Model(&user).Where("id=?", id).First()
	if err != nil {
		if err == pg.ErrNoRows {
			return errors.New("No existe el usuario especificado")
		}
		return err
	}
	decoded, err := Functions.Decrypt(user.Password)
	if err != nil {
		return err
	}

	if decoded != previo {
		return errors.New("La contraseña anterior no es válida");
	}
	decoded, err = Functions.Encrypt(nuevo)
	if err != nil {
		return err
	}
	_, err = db.Model(&user).
		Set("password=?", decoded).
		Where("id=?", id).
		Update()
	if err != nil {
		return err
	}
	return nil
}

func ChangePassword(id, nuevo string) error {
	var user Schemas.Usuario
	db := models.GetDataBase()
	err := db.Model(&user).Where("id=?", id).First()
	if err != nil {
		if err == pg.ErrNoRows {
			return errors.New("No existe el usuario especificado")
		}
		return err
	}
	decoded, err := Functions.Encrypt(nuevo)
	if err != nil {
		return err
	}
	_, err = db.Model(&user).
		Set("password=?", decoded).
		Where("id=?", id).
		Update()
	if err != nil {
		return err
	}
	return nil
}

func AddContact(id, contact string) (error) {
	db := models.GetDataBase()
	var existente Schemas.Contacto
	err := db.Model(&existente).Where("usuario_id=?", id).
		Where("contacto_usuario_id=?", contact).Where("estatus=?", Constants.ACTIVO).First()
	if err != nil && err != pg.ErrNoRows {
		return err
	}
	if err == pg.ErrNoRows {
		var contacto = Schemas.Contacto{
			Id:                xid.New().String(),
			UsuarioID:         id,
			ContactoUsuarioID: contact,
			CreatedAt:         time.Now(),
			UpdatedAt:         time.Now(),
			Estatus:           Constants.ACTIVO}
		return db.Insert(&contacto)
	}

	existente.Estatus = Constants.ACTIVO
	return db.Update(&existente)

}

func GetUserContacts(id string) ([]Schemas.Contacto, error) {
	db := models.GetDataBase()
	/*q := `select c.contacto_id as id ,u.nombre as nombre,u.apellido_paterno as apellido_paterno,
	u.apellido_materno as apellido_materno,u.foto_perfil as foto_perfil, u.email as email
	from usuarios u INNER JOIN contactos c
	on u.id=c.contacto_id
	where c.usuario_id=? and c.estatus=?`*/
	var res []Schemas.Contacto
	err := db.Model(&res).Column("contacto.*", "ContactoUsuario").
		Relation("ContactoUsuario", func(q *orm.Query) (*orm.Query, error) {
			return q.Column("ContactoUsuario", "Empleo", "Educacion").
				Relation("Empleo", func(query *orm.Query) (*orm.Query, error) {
					return q, nil
				}).Relation("Educacion", func(query *orm.Query) (*orm.Query, error) {
				return q, nil
			}), nil
		}).
		Where("usuario_id=?", id).Where("contacto.estatus=?", Constants.ACTIVO).Select()
	return res, err
}

func RemoveContact(user, contact string) error {
	db := models.GetDataBase()
	contacto := Schemas.Contacto{Estatus: Constants.ACTIVO,
		ContactoUsuarioID: contact,
		UsuarioID:         user,}
	_, err := db.Model(&contacto).Where("contacto_usuario_id = ?", contact).
		Where("usuario_id=?", user).
		Delete()
	return err
}

func GetEmails(ids []string) ([]map[string]string, error) {
	db := models.GetDataBase()
	res := make([]map[string]string, 0, 0)
	var usuarios = []Schemas.Usuario{}
	err := db.Model(&usuarios).
		Column("id", "nombre", "email", "zona_horaria").
		Where("id in (?)", pg.In(ids)).
		Select()
	if err != nil {
		return nil, err
	}

	for _, v := range usuarios {
		res = append(res, map[string]string{"correo": v.Email,
			"timezone": v.ZonaHoraria,
			"id":       v.Id,})
	}

	return res, nil
}

func GetUserPlan(id string) (*Schemas.PlanUsuario, float64, error) {
	var r Schemas.PlanUsuario
	db := models.GetDataBase()
	err := db.Model(&r).Column("plan_usuario.*", "Plan").
		Relation("Plan", func(query *orm.Query) (*orm.Query, error) {
			return query, nil
		}).
		Where("usuario_id=?", id).
		First()
	if err != nil {
		if err != pg.ErrNoRows {
			return nil, 0, err
		}
		err = ChangePlan(id, nil, config.GetConfig().PlanPrueba, config.GetConfig().DiasPrueba)
		if err != nil {
			return nil, 0, err
		}
		db.Model(&r).Column("plan_usuario.*", "Plan").
			Relation("Plan", func(query *orm.Query) (*orm.Query, error) {
				return query, nil
			}).
			Where("usuario_id=?", id).
			First()
	}
	if r.Vigencia == nil || r.Vigencia.IsZero() {
		err = ChangePlan(r.UsuarioID, nil, r.PlanID, config.GetConfig().DiasPrueba)
		if err != nil {
			return nil, 0, err
		}
		db.Model(&r).Column("plan_usuario.*", "Plan").
			Relation("Plan", func(query *orm.Query) (*orm.Query, error) {
				return query, nil
			}).
			Where("usuario_id=?", id).
			First()
	}
	var ocupado float64
	err = db.Model((*Schemas.ArchivoReunion)(nil)).
		ColumnExpr("sum(size) AS ocupado").
		Group("usuario_id").
		Where("usuario_id=?", id).
		Select(&ocupado)
	if err != nil && err != pg.ErrNoRows {
		return nil, 0, err
	}
	return &r, (ocupado / 1024) / 1024, nil
}

func GetFiles(user string) ([]Schemas.ArchivoReunion, error) {
	db := models.GetDataBase()
	var files []Schemas.ArchivoReunion
	err := db.Model(&files).Where("usuario_id=?", user).Select()
	if err != nil {
		return nil, err
	}
	return files, nil
}

func ChangePlan(user string, tarjeta *string, plan string, nuevaVigencia int) (error) {
	db := models.GetDataBase()
	tx, err := db.Begin()
	renovar := tarjeta != nil
	vigencia := time.Now().UTC().Add(time.Hour * 24 * time.Duration(nuevaVigencia)).UTC()
	if err != nil {
		return err
	}
	var previo Schemas.PlanUsuario
	err = tx.Model(&previo).Where("usuario_id=?", user).Last()
	if err != nil {
		if err != pg.ErrNoRows {
			tx.Rollback()
			return err
		}
		previo = Schemas.PlanUsuario{Id: xid.New().String(),
			PlanID:    plan,
			Vigencia:  &vigencia,
			UsuarioID: user,
			Renovar:   renovar,
			CreatedAt: time.Now(),
			TarjetaId: tarjeta,
			UpdatedAt: time.Now(),
		}

		err = tx.Insert(&previo)
		if err != nil {
			tx.Rollback()
			return err
		}
		fmt.Println("Commit")
		return tx.Commit()
	}

	_, err = tx.Model((*Schemas.PlanUsuario)(nil)).
		Set("plan_id=?", plan).
		Set("renovar=?", renovar).
		Set("tarjeta_id=?", tarjeta).
		Set("updated_at=?", time.Now()).
		Set("vigencia=?", &vigencia).
		Where("usuario_id=?", user).
		Update()
	if err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit()
}

func CreateCuenta(usuario, clienteId string) (error) {
	db, err := models.GetDataBase().Begin()
	if err != nil {
		return err
	}
	_, err = db.Model((*Schemas.Usuario)(nil)).
		Set("cliente_id=?", clienteId).
		Where("id=?", usuario).
		Update()
	if err != nil {
		return err
	}
	return db.Commit()
}

func CancelarSuscripcion(user string) error {
	db := models.GetDataBase()
	tx, err := db.Begin()
	var previo Schemas.PlanUsuario
	err = tx.Model(&previo).Where("usuario_id=?", user).Last()
	if err != nil {
		if err != pg.ErrNoRows {
			tx.Rollback()
			return err
		}
	}
	_, err = tx.Model(&previo).
		Set("renovar=?", false).
		Set("tarjeta_id=?", nil).
		Where("usuario_id=?", user).
		Update()
	if err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit()
}

func ObtenerSuscripciones() (suscripciones []*Schemas.PlanUsuario, err error) {
	db := models.GetDataBase()
	suscripciones = make([]*Schemas.PlanUsuario, 0)
	err = db.Model(&suscripciones).
		Column("plan_usuario.*", "Plan", "Usuario").
		Where(" date_trunc('day',vigencia) =?", time.Now().UTC().Format("01-02-2006")).
		Select()
	return
}
