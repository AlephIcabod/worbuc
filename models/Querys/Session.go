package Querys

import (
	"bitbucket.org/AlephIcabod/worbuc/models"
	"time"
	"github.com/go-pg/pg"
	"github.com/rs/xid"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
)

func IniciarSession(user, device, deviceID string) error {
	db := models.GetDataBase()
	var aux Schemas.Sesion
	err := db.Model(&aux).Where("usuario_id=?", user).Where("dispositivo=?", deviceID).
		Where("fecha_termino is NULL").Last()

	if err != nil {
		if err == pg.ErrNoRows {
			return db.Insert(&Schemas.Sesion{Id: xid.New().String(), UsuarioID: user, FechaInicio: time.Now().UTC(),
				TipoDispositivo: device, Dispositivo: deviceID})
		}
		return err
	}
	return nil
}

func TerminarSession(user, deviceID string) error {
	db := models.GetDataBase()

	_, err := db.Model((*Schemas.Sesion)(nil)).
		Set("fecha_termino=?", time.Now()).
		Where("usuario_id=?", user).
		Where("dispositivo=?", deviceID).Update()
	return err
}
