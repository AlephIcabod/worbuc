package Querys

import (
	"bitbucket.org/AlephIcabod/worbuc/models"
	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
)

type UserFinded struct {
	Schemas.Usuario
	Contacto bool
}

func FindOrCreateEmpresa(nombre, sector string) (*Schemas.Empresa, error) {
	var emp Schemas.Empresa
	db := models.GetDataBase()
	err := db.Model(&emp).Where("nombre ILIKE ?", nombre).
		Where("sector=?", sector).Limit(1).Select()
	if err == pg.ErrNoRows {
		emp = Schemas.Empresa{Nombre: nombre, Sector: sector}
		err = db.Insert(&emp)
		if err != nil {
			return nil, err
		}
		return &emp, err
	} else {
		if err != nil {
			return nil, err
		}
		return &emp, nil
	}

	return &emp, nil
}

func FindOrCreateSchool(nombre string) (*Schemas.Institucion, error) {
	var emp Schemas.Institucion
	db := models.GetDataBase()
	err := db.Model(&emp).Where("nombre ILIKE ?", nombre).
		Limit(1).Select()
	if err == pg.ErrNoRows {
		emp = Schemas.Institucion{Nombre: nombre}
		err = db.Insert(&emp)
		if err != nil {
			return nil, err
		}
		return &emp, err
	} else {
		if err != nil {
			return nil, err
		}
		return &emp, nil
	}
	return &emp, nil
}

func GetEstados(pais int) ([]Schemas.Estado, error) {
	db := models.GetDataBase()
	var estados []Schemas.Estado
	err := db.Model(&estados).
		Where("pais_id=?", pais).
		Select()
	if err != nil {
		return nil, err
	}
	return estados, nil
}

func GetPais(p int64) (*Schemas.Pais, error) {
	var pais Schemas.Pais
	db := models.GetDataBase()
	err := db.Model(&pais).Where("id=?", p).
		Column("pais.*", "Estados").Relation("Estados", func(query *orm.Query) (*orm.Query, error) {
		return query, nil
	}).First()
	if err != nil {
		return nil, err
	}
	return &pais, nil
}

func GetPaises() ([]Schemas.Pais, error) {
	var pais []Schemas.Pais
	db := models.GetDataBase()
	err := db.Model(&pais).
		Column("pais.*", "Estados").Relation("Estados", func(query *orm.Query) (*orm.Query, error) {
		return query, nil
	}).Select()
	if err != nil {
		return nil, err
	}
	return pais, nil
}

func GetSchools(limit, page int, name string) ([]Schemas.Institucion, error) {
	var res []Schemas.Institucion
	db := models.GetDataBase()
	err := db.Model(&res).
		Where("nombre ILIKE ?", "%"+name+"%").
		Order("id").
		Limit(limit).
		Offset((page - 1) * limit).Select()
	if err != nil {
		if err == pg.ErrNoRows {
			return []Schemas.Institucion{}, nil
		}
		return nil, err
	}
	return res, nil
}

func GetEmpresas(limit, page int, sector string) ([]Schemas.Empresa, error) {
	var res []Schemas.Empresa
	db := models.GetDataBase()
	err := db.Model(&res).
		Where("sector=?", sector).
		Order("id").
		Limit(limit).
		Offset((page - 1) * limit).Select()
	if err != nil {
		if err == pg.ErrNoRows {
			return []Schemas.Empresa{}, nil
		}
		return nil, err
	}
	return res, nil
}

func FindUsers(name string, limit, page int, userID string, ocultos bool) ([]UserFinded, error) {
	db := models.GetDataBase()
	var aux []Schemas.Usuario
	var res = make([]UserFinded, 0)
	if ocultos {
		name = "%" + name + "%"
		err := db.Model(&aux).Where("nombre ilike ?", name).WhereOr("email ilike ?", name).Select()
		if err != nil {
			return nil, err
		}
		for i := range aux {
			res = append(res, UserFinded{aux[i], false})
		}
		return res, nil
	}
	q := `SELECT distinct( u.id) as id, u.nombre as nombre ,u.apellido_paterno as apellido_paterno,u.apellido_materno as apellido_materno,u.email as email,
       u.foto_perfil as foto_perfil,
  CASE WHEN c2.usuario_id=? THEN TRUE
  ELSE FALSE END as contacto
FROM usuarios u LEFT JOIN contactos c2 ON u.id = c2.contacto_usuario_id
WHERE u.id <> ? AND tipo_perfil = ? AND u.estatus=?
AND (u.nombre ILIKE ? OR u.apellido_paterno ILIKE ? OR u.apellido_materno ILIKE ?)`
	name = "%" + name + "%"

	_, err := db.Query(&res,
		q, userID, userID, Constants.PERFIL_PUBLICO, Constants.REGISTRADO, name, name, name)
	return res, err
}

func GetEtiquetas(user, tipo string) (map[string]int, error) {
	db := models.GetDataBase()
	res := make(map[string]int, 0)
	switch tipo {
	case "proyectos":
		res1 := make([]Schemas.UsuarioProyecto, 0)
		err := db.Model(&res1).Column("etiquetas", "Proyecto").
			Where("usuario_id=?", user).
			Where("proyecto.estatus<>?", Constants.ELIMINADO).
			Select()
		if err != nil {
			return nil, err
		}
		for _, v := range res1 {
			for _, w := range v.Etiquetas {
				res[w]++
			}
		}
	case "reuniones":
		res1 := make([]Schemas.Participante, 0)
		err := db.Model(&res1).Column("etiquetas").
			Where("usuario_id=?", user).
			Select()
		if err != nil {
			return nil, err
		}
		for _, v := range res1 {
			for _, w := range v.Etiquetas {
				res[w]++
			}
		}
	}

	return res, nil
}
