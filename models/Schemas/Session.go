package Schemas

import (
	"time"
)

type Sesion struct {
	Id              string    `sql:",pk,type:varchar(255)"`
	UsuarioID       string    `sql:",type:varchar(255)"`
	Usuario         *Usuario  `sql:"-"`
	FechaInicio     time.Time `sql:"type:timestamp"`
	TipoDispositivo string    `sql:",type:varchar(255)"`
	Dispositivo     string    `sql:",type:varchar(255)"`
	FechaTermino    time.Time `sql:"type:timestamp"`
}

