package Schemas

import "time"

type Reunion struct {
	Id            string           `sql:",pk,type:varchar(255)"`
	Fecha         time.Time        `sql:"type:timestamp"`
	Titulo        string           `sql:"type:varchar(255)"`
	Objetivo      string           `sql:"type:varchar(255)"`
	Lugar         string           `sql:"type:varchar(255)"`
	HoraInicio    time.Time        `sql:"type:timestamp"`
	HoraTermino   time.Time        `sql:"type:timestamp"`
	Duracion      int64            `sql:"type:integer"`
	Estatus       string           `sql:"type:varchar(255)"`
	OrganizadorID string           `sql:"type:varchar(255)"`
	Organizador   *Usuario         `sql:"-"`
	CreatedAt     time.Time        `sql:"default:now()"`
	UpdatedAt     time.Time        `sql:"default:now()"`
	Participantes []Participante   `sql:"-"`
	Minuta        []Minuta         `sql:"-"`
	Archivos      []ArchivoReunion `sql:"-"`
}

type Participante struct {
	Id            int64    `sql:",pk;type:serial"`
	Reunion       *Reunion `sql:"-"`
	ReunionID     string   `sql:"type:varchar(255)"`
	Usuario       *Usuario `sql:"-"`
	UsuarioID     string   `sql:"type:varchar(255)"`
	Rol           string   `sql:"type:varchar(255)"`
	LibretaID     int64
	Libreta       *Libreta `sql:"-"`
	Alias         string   `sql:"type:varchar(5)"`
	Etiquetas     []string `pg:",array"`
	Administrador bool
	Asistencia    bool
	Confirmacion  bool
}

type Minuta struct {
	Id            int64     `sql:",pk;type:serial"`
	Reunion       *Reunion  `sql:"-"`
	ReunionID     string    `sql:"type:varchar(255)"`
	Tema          string    `sql:"type:varchar(255)"`
	Descripcion   string
	Estatus       string    `sql:"type:varchar(255)"`
	ResponsableID string    `sql:"type:varchar(255)"`
	Responsable   *Usuario  `sql:"-"`
	NumeroOrden   int
	Notas         []Nota    `sql:"-"`
	CreatedAt     time.Time `sql:"default:now()"`
	UpdatedAt     time.Time `sql:"default:now()"`
}
type Nota struct {
	Reunion   *Reunion  `sql:"-"`
	ReunionID string    `sql:"type:varchar(255)"`
	UsuarioID string    `sql:"type:varchar(255)"`
	Usuario   *Usuario  `sql:"-"`
	Minuta    *Minuta   `sql:"-"`
	MinutaID  int64
	Texto     string
	CreatedAt time.Time `sql:"default:now()"`
	UpdatedAt time.Time `sql:"default:now()"`
}

type ArchivoReunion struct {
	ID         string    `sql:",pk,type:varchar(255)"`
	Reunion    *Reunion  `sql:"-"`
	ReunionID  string    `sql:"type:varchar(255)"`
	Url        string    `sql:"type:varchar(255)"`
	Size       float64
	Name       string    `sql:"type:varchar(255)"`
	UsuarioID  string    `sql:"type:varchar(255)"`
	Usuario    *Usuario  `sql:"-"`
	ProyectoID string    `sql:"type:varchar(255)"`
	Proyecto   *Proyecto `sql:"-"`
	CreatedAt  time.Time `sql:"default:now()"`
}
