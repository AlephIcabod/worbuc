package Schemas

import "time"

type Libreta struct {
	Id          int64     `sql:",pk;type:serial"`
	Nombre      string    `sql:"type:varchar(255)"`
	Descripcion string    `sql:"type:varchar(255)"`
	UsuarioID   string    `sql:"type:varchar(255)"`
	Estatus     string    `sql:"type:varchar(255)"`
	Usuario     *Usuario  `json:"-",sql:"-"`
	Default     bool
	CreatedAt   time.Time `sql:"default:now()"`
	UpdatedAt   time.Time `sql:"default:now()"`
}

type Pais struct {
	Id      int64
	Nombre  string    `sql:"type:varchar(255)"`
	Codigo  string    `sql:"type:varchar(255)"`
	Estados []*Estado `sql:"-"`
}

type Estado struct {
	Id     int64  `sql:"type:serial"`
	PaisID int64
	Pais   *Pais  `json:"-",sql:"-"`
	Nombre string `sql:"type:varchar(255)"`
}

type Empresa struct {
	Id        int64     `sql:"type:serial"`
	Nombre    string    `sql:"type:varchar(255)"`
	Sector    string    `sql:"type:varchar(255)"`
	Empleados []*Empleo `sql:"-"`
}

type Institucion struct {
	Id          int64        `sql:"type:serial"`
	Nombre      string       `sql:"type:varchar(255)"`
	Estudiantes []*Educacion `sql:"-"`
}

type Consulta struct {
	Campo    string      `json:"campo"`
	Valor    interface{} `json:"valor"`
	Operador string      `json:"operador"`
	Tipo     string      `json:"tipo"`
	Or       bool        `json:"or"`
}
