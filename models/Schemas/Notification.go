package Schemas

import "time"

type Notification struct {
	Id         string `sql:",pk,type:varchar(255)"`
	Texto      string
	Tipo       string    `sql:"type:varchar(255)"`
	Emisor     *Usuario  `sql:"-"`
	Receptor   *Usuario  `sql:"-"`
	ReceptorID string    `sql:"type:varchar(255)"`
	EmisorID   string    `sql:"type:varchar(255)"`
	Adicional  string    `sql:"type:varchar(255)"`
	Fecha      time.Time `sql:"type:timestamp"`
	Titulo     string
	Checked    bool
}
