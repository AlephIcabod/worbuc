package Schemas

import (
	"time"
)

type Usuario struct {
	Id              string  `sql:",pk,type:varchar(255)"`
	ClienteId       *string `sql:"type:varchar(255)"`
	Email           string  `sql:",unique:idx_email,type:varchar(255)"`
	Password        string  `json:"-",sql:"type:varchar(255)"`
	Nombre          string  `sql:"type:varchar(255)"`
	ApellidoPaterno string  `sql:"type:varchar(255)"`
	ApellidoMaterno string  `sql:"type:varchar(255)"`
	FechaNacimiento time.Time
	Estatus         string      `sql:"type:varchar(50)"`
	Genero          string      `sql:"type:char(1)"`
	Escolaridad     string      `sql:"type:varchar(255)"`
	TipoPerfil      string      `sql:"type:varchar(20),default:'Publico'"`
	FotoPerfil      string      `sql:"type:varchar(255),default:'/public/defaults/avatar.png'"`
	ZonaHoraria     string      `sql:"type:varchar(255),default:'Mexico/General'"`
	IsAdmin         bool        `json:"-",sql:"default:false"`
	CreatedAt       time.Time   `sql:"default:now()"`
	UpdatedAt       time.Time   `sql:"default:now()"`
	Direccion       *Direccion  `sql:"-"`
	Telefonos       []*Telefono `sql:"-"`
	Libretas        []*Libreta  `sql:"-"`
	Proyectos       []*Proyecto `sql:"-"`
	Empleo          *Empleo     `sql:"-"`
	Educacion       *Educacion  `sql:"-"`
	Contactos       []*Contacto `sql:"-"`
}

type PlanUsuario struct {
	Id        string     `sql:",pk,type:varchar(255)"`
	UsuarioID string     `sql:"type:varchar(255)"`
	Usuario   *Usuario   `sql:"-"`
	Plan      *Plan      `sql:"-"`
	PlanID    string     `sql:"type:varchar(255)"`
	Vigencia  *time.Time `sql:"type:timestamp"`
	TarjetaId *string    `sql:"type:varchar(255)"`
	Renovar   bool
	CreatedAt time.Time `sql:"default:now()"`
	UpdatedAt time.Time `sql:"default:now()"`
}

type Plan struct {
	Id          string    `sql:",pk,type:varchar(255)"`
	Nombre      string    `sql:"type:varchar(255)"`
	Precio      float64   `sql:"default:0"`
	Descripcion string    `sql:"type:varchar(255)"`
	CreatedAt   time.Time `sql:"default:now()"`
	UpdatedAt   time.Time `sql:"default:now()"`
	Estatus     string    `sql:"default:'Activo'"`
	Apps        []int     `pg:",array"`
	LimiteMb    float64
}

type Direccion struct {
	Id           int64 `sql:",pk,type:serial"`
	EstadoID     int64
	Estado       *Estado  `sql:"-"`
	Poblacion    string   `sql:"type:varchar(255)"`
	CodigoPostal string   `sql:"type:varchar(10)"`
	UsuarioID    string   `sql:"type:varchar(255)"`
	Usuario      *Usuario `json:"-",sql:"-"`
	PaisID       int64
	Pais         *Pais     `sql:"-"`
	CreatedAt    time.Time `sql:"default:now()"`
	UpdatedAt    time.Time `sql:"default:now()"`
}

type Telefono struct {
	Id        int64     `sql:",pk"`
	Telefono  string    `sql:"type:varchar(15)"`
	Tipo      string    `sql:"type:varchar(255)"`
	UsuarioID string    `sql:"type:varchar(255)"`
	Usuario   *Usuario  `json:"-",sql:"-"`
	CreatedAt time.Time `sql:"default:now()"`
	UpdatedAt time.Time `sql:"default:now()"`
}

type Empleo struct {
	Id        int64 `sql:",pk"`
	EmpresaID int64
	Empresa   *Empresa  `sql:"-"`
	Puesto    string    `sql:"type:varchar(255)"`
	UsuarioID string    `sql:"type:varchar(255)"`
	Usuario   *Usuario  `json:"-",sql:"-"`
	CreatedAt time.Time `sql:"default:now()"`
	UpdatedAt time.Time `sql:"default:now()"`
}

type Educacion struct {
	Id            int64 `sql:",pk"`
	InstitucionID int64
	Institucion   *Institucion `sql:"-"`
	Periodo       string       `sql:"type:varchar(255)"`
	UsuarioID     string       `sql:"type:varchar(255)"`
	Usuario       *Usuario     `json:"-",sql:"-"`
	CreatedAt     time.Time    `sql:"default:now()"`
	UpdatedAt     time.Time    `sql:"default:now()"`
}

type Contacto struct {
	Id                string    `json:"-",sql:",pk"`
	Usuario           *Usuario  `json:"-",sql:"-"`
	UsuarioID         string    `sql:"type:varchar(255)"`
	ContactoUsuario   *Usuario  `sql:"-"`
	ContactoUsuarioID string    `sql:"type:varchar(255)"`
	Estatus           string    `sql:"type:varchar(10)"`
	CreatedAt         time.Time `sql:"default:now()"`
	UpdatedAt         time.Time `sql:"default:now()"`
}
