package Schemas

import (
	"time"
	"bitbucket.org/AlephIcabod/worbuc/config"
	"fmt"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
)

type Proyecto struct {
	Id            string             `sql:",pk,type:varchar(255)"`
	Nombre        string             `sql:"type:varchar(255)"`
	Estatus       string             `sql:"type:varchar(50)"`
	Descripcion   string             `sql:"type:varchar(255)"`
	CreadorID     string             `sql:"type:varchar(255)"`
	Creador       *Usuario           `sql:"-"`
	CreatedAt     time.Time          `sql:"type:timestamp"`
	UpdatedAt     time.Time          `sql:"default:now()"`
	Participantes []*UsuarioProyecto `sql:"-"`
	Actividades   []*Actividad       `sql:"-"`
	Archivos      []*ArchivoReunion  `sql:"-"`
}

type UsuarioProyecto struct {
	Id         string    `sql:",pk,type:varchar(255)"`
	Proyecto   *Proyecto `sql:"-"`
	ProyectoID string    `sql:"type:varchar(255)"`
	Usuario    *Usuario  `sql:"-"`
	LibretaID  int64
	Libreta    *Libreta `sql:"-"`
	Etiquetas  []string `pg:",array"`
	UsuarioID  string   `sql:"type:varchar(255)"`
	Estatus    string   `sql:"type:varchar(255),default:'Activo'"`
	Favorito   bool
	CreatedAt  time.Time `sql:"type:timestamp"`
	UpdatedAt  time.Time `sql:"default:now()"`
}

type Actividad struct {
	Id                string    `sql:",pk,type:varchar(255)"`
	Nombre            string    `sql:"type:varchar(255)"`
	Descripcion       string    `sql:"type:varchar(255)"`
	Proyecto          *Proyecto `sql:"-"`
	ProyectoID        string    `sql:"type:varchar(255)"`
	FechaCreacion     time.Time `sql:"type:timestamp"`
	FechaProgramada   time.Time `sql:"type:timestamp"`
	FechaFinalizacion time.Time `sql:"type:timestamp"`
	Estatus           string    `sql:"type:varchar(255)"`
	UpdatedAt         time.Time `sql:"type:timestamp"`
	Tareas            []*Tarea  `sql:"-"`
}

type Tarea struct {
	Id                string     `sql:",pk,type:varchar(255)"`
	Actividad         *Actividad `sql:"-"`
	ActividadID       string     `sql:"type:varchar(255)"`
	Nombre            string     `sql:"type:varchar(255)"`
	Descripcion       string
	Prioridad         string
	FechaCreacion     time.Time `sql:"type:timestamp"`
	FechaProgramada   time.Time `sql:"type:timestamp"`
	FechaFinalizacion time.Time `sql:"type:timestamp"`
	Estatus           string
	CreadorID         string               `sql:"type:varchar(255)"`
	Creador           *UsuarioProyecto     `sql:"-"`
	AprobadorID       string               `sql:"type:varchar(255)"`
	Aprobador         *UsuarioProyecto     `sql:"-"`
	Responsables      []*ParticipanteTarea `sql:"-"`
	Supervisores      []*ParticipanteTarea `sql:"-"`
	Comentarios       []*ComentarioTarea   `sql:"-"`
	Archivos          []*ArchivoTarea      `sql:"-"`
}

type ParticipanteTarea struct {
	Id        string           `sql:",pk,type:varchar(255)"`
	TareaID   string           `sql:"type:varchar(255)"`
	Tarea     *Tarea           `sql:"-"`
	Usuario   *UsuarioProyecto `sql:"-"`
	UsuarioID string           `sql:"type:varchar(255)"`
	Rol       string           `sql:"type:varchar(255)"`
	Archivado bool
}

type ComentarioTarea struct {
	Id             string           `sql:",pk,type:varchar(255)"`
	Participante   *UsuarioProyecto `sql:"-"`
	ParticipanteID string           `sql:"type:varchar(255)"`
	TareaID        string           `sql:"type:varchar(255)"`
	Tarea          *Tarea           `sql:"-"`
	Comentario     string
	Fecha          time.Time          `sql:"type:timestamp"`
	Adjunto        *ArchivoTarea      `sql:"-"`
	AdjuntoID      string             `sql:"type:varchar(255)"`
	Original       *ComentarioTarea   `sql:"-"`
	OriginalID     string             `sql:"type:varchar(255)"`
	Respuestas     []*ComentarioTarea `sql:"-"`
	Estatus        string             `sql:"type:varchar(255)"`
}

type ArchivoTarea struct {
	Id         string           `sql:",pk,type:varchar(255)"`
	ArchivoID  string           `sql:"type:varchar(255)"`
	Archivo    *ArchivoReunion  `sql:"-"`
	TareaID    string           `sql:"type:varchar(255)"`
	Comentario *ComentarioTarea `sql:"-"`
}

func (p *Proyecto) EnviarCorreo(tipo int, unico *UsuarioProyecto, emisor string) {
	// serverAddress := config.GetServerAddress() + "/"
	auxP := make([]*UsuarioProyecto, 0)
	if unico == nil {
		auxP = p.Participantes
	} else {
		auxP = append(auxP, unico)
	}
	bodyString := `<div style="margin:0px auto;max-width:600px;">
    <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
        <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
                                        <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                            <p><span style="font-size:16px;">%s
                                                </span></p>
                                            <p><span style="font-size:16px;">Ingresa a worbuc para poder dar seguimiento a tus proyectos y gestiona tu tiempo.</span></p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
</div> `
	accion := "te ha agregado al proyecto "
	if tipo == 2 {
		accion = ` te ha eliminado del proyecto `
	}
	if tipo == 3 {
		accion = ` ha eliminado el proyecto`
	}
	titulo := "Invitacion a proyecto"
	for _, v := range auxP {
		if v.Usuario.Id != emisor {
			body := fmt.Sprintf(bodyString,
				p.Creador.Nombre+" "+p.Creador.ApellidoPaterno+accion+
					p.Nombre, )
			body = fmt.Sprint(`%s %s %s`, Constants.CABECERA_CORREO, body, Constants.PIE_CORREO)
			if tipo == 3 {
				titulo = "Cancelación de proyecto"
			}
			m, _ := config.CargarDatosCorreo(titulo, body, v.Usuario.Email, true)
			go config.EnviarCorreo(m)
		}
	}
}

func (t *Tarea) EnviarCorreo(tipo int, origen string, emisor string) {
	// serverAddress := config.GetServerAddress() + "/"
	bodyString := `<div style="margin:0px auto;max-width:600px;">
    <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
        <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
                                        <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                            <p><span style="font-size:16px;">%s
                                                </span></p>
                                            <p><span style="font-size:16px;">Ingresa a worbuc para poder dar seguimiento a tus proyectos y gestiona tu tiempo.</span></p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
</div>`
	accion := ""
	switch tipo {
	case Constants.ASIGNACION:
		accion = "te ha asignado como %s en la tarea"
		for _, v := range t.Responsables {
			if v.Usuario.Usuario.Id == emisor {
				continue
			}
			body := fmt.Sprintf(bodyString,
				origen+accion+
					v.Rol+" "+t.Nombre)
			body = fmt.Sprint(`%s %s %s`, Constants.CABECERA_CORREO, body, Constants.PIE_CORREO)
			m, _ := config.CargarDatosCorreo("Asignación de tarea", body, v.Usuario.Usuario.Email, true)

			go config.EnviarCorreo(m)
		}
		for _, v := range t.Supervisores {
			if v.Usuario.Usuario.Id == emisor {
				continue
			}
			body := fmt.Sprintf(bodyString,
				origen+accion+
					v.Rol+" "+t.Nombre)
			body = fmt.Sprint(`%s %s %s`, Constants.CABECERA_CORREO, body, Constants.PIE_CORREO)
			m, _ := config.CargarDatosCorreo("Asignación de tarea", body, v.Usuario.Usuario.Email, true)
			go config.EnviarCorreo(m)
		}
	case Constants.CANCELACION:
		accion = ` ha cancelado la tarea `
		destinatarios := make([]string, 0)
		for _, v := range t.Responsables {
			if v.Usuario.Usuario.Id == emisor {
				continue
			}
			destinatarios = append(destinatarios, v.Usuario.Usuario.Email)
		}
		for _, v := range t.Supervisores {
			if v.Usuario.Usuario.Id == emisor {
				continue
			}
			destinatarios = append(destinatarios, v.Usuario.Usuario.Email)
		}
		body := fmt.Sprintf(bodyString,
			origen+accion+
				t.Nombre)
		body = fmt.Sprint(`%s %s %s`, Constants.CABECERA_CORREO, body, Constants.PIE_CORREO)
		if len(destinatarios) > 0 {
			m, _ := config.CargarDatosCorreo("Tarea cancelada", body, destinatarios[0], true, destinatarios...)
			go config.EnviarCorreo(m)
		}
	case Constants.CAMBIA_ESTATUS:
		accion = ` ha indicado que la tarea `
		destinatarios := make([]string, 0)
		for _, v := range t.Responsables {
			if v.Usuario.Usuario.Id == emisor {
				continue
			}
			destinatarios = append(destinatarios, v.Usuario.Usuario.Email)
		}
		for _, v := range t.Supervisores {
			if v.Usuario.Usuario.Id == emisor {
				continue
			}
			destinatarios = append(destinatarios, v.Usuario.Usuario.Email)
		}
		body := fmt.Sprintf(bodyString,
			origen+accion+t.Nombre+" ahora esta "+
				t.Estatus)
		body = fmt.Sprint(`%s %s %s`, Constants.CABECERA_CORREO, body, Constants.PIE_CORREO)
		if len(destinatarios) > 0 {
			m, _ := config.CargarDatosCorreo("Tarea actualizada", body, destinatarios[0], true, destinatarios...)
			go config.EnviarCorreo(m)
		}
	case Constants.ACTUALIZACION:
		accion = ` ha modificado la tarea `
		destinatarios := make([]string, 0)
		for _, v := range t.Responsables {
			if v.Usuario.Usuario.Id == emisor {
				continue
			}
			destinatarios = append(destinatarios, v.Usuario.Usuario.Email)
		}
		for _, v := range t.Supervisores {
			if v.Usuario.Usuario.Id == emisor {
				continue
			}
			destinatarios = append(destinatarios, v.Usuario.Usuario.Email)
		}
		body := fmt.Sprintf(bodyString,
			origen+accion+
				t.Nombre)
		body = fmt.Sprint(`%s %s %s`, Constants.CABECERA_CORREO, body, Constants.PIE_CORREO)
		if len(destinatarios) > 0 {
			m, _ := config.CargarDatosCorreo("Tarea modificada", body, destinatarios[0], true, destinatarios...)
			go config.EnviarCorreo(m)
		}
	}
}

func (c *ComentarioTarea) EnviarCorreo(origen, emisor string, t *Tarea) {
	//serverAddress := config.GetServerAddress() + "/"
	bodyString := `<div style="margin:0px auto;max-width:600px;">
    <table role="presentation" cellpadding="0" cellspacing="0" style="font-size:0px;width:100%%;" align="center" border="0">
        <tbody>
            <tr>
                <td style="text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:9px 0px 9px 0px;">
                    <!--[if mso | IE]>      <table role="presentation" border="0" cellpadding="0" cellspacing="0">        <tr>          <td style="vertical-align:top;width:600px;">      <![endif]-->
                    <div class="mj-column-per-100 outlook-group-fix" style="vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%%;">
                        <table role="presentation" cellpadding="0" cellspacing="0" width="100%%" border="0">
                            <tbody>
                                <tr>
                                    <td style="word-wrap:break-word;font-size:0px;padding:0px 20px 0px 20px;" align="center">
                                        <div style="cursor:auto;color:#000000;font-family:Ubuntu, Helvetica, Arial, sans-serif;font-size:11px;line-height:22px;text-align:center;">
                                            <p><span style="font-size:16px;">%s
                                                </span></p>
                                            <p><span style="font-size:16px;">Ingresa a worbuc para poder dar seguimiento a tus proyectos y gestiona tu tiempo.</span></p>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]>      </td></tr></table>      <![endif]-->
                </td>
            </tr>
        </tbody>
    </table>
</div>`
	accion := " ha comentado la tarea "
	body := fmt.Sprintf(bodyString,
		origen+accion+
			t.Nombre)
	body = fmt.Sprint(`%s %s %s `, Constants.CABECERA_CORREO, body, Constants.PIE_CORREO)
	if c.OriginalID != "" {
		accion = ` ha respondido al comentario de `
		body = fmt.Sprintf(bodyString,
			origen+accion+
				c.Original.Participante.Usuario.Nombre+" "+c.Original.Participante.Usuario.ApellidoPaterno+" en la tarea "+
				t.Nombre)
		body = fmt.Sprint(`%s %s %s `, Constants.CABECERA_CORREO, body, Constants.PIE_CORREO)
	}

	destinatarios := make([]string, 0)
	for _, v := range t.Responsables {
		if v.Usuario.Usuario.Id == emisor {
			continue
		}
		destinatarios = append(destinatarios, v.Usuario.Usuario.Email)
	}
	for _, v := range t.Supervisores {
		if v.Usuario.Usuario.Id == emisor {
			continue
		}
		destinatarios = append(destinatarios, v.Usuario.Usuario.Email)
	}

	m, _ := config.CargarDatosCorreo("Comentario de tarea", body, destinatarios[0], true,
		destinatarios...)
	go config.EnviarCorreo(m)
}
