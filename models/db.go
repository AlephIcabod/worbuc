package models

import (
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"sync"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"io/ioutil"
	"strings"
	"fmt"
	"strconv"
	"github.com/rs/xid"

	"bitbucket.org/AlephIcabod/worbuc/models/Schemas"
	"bitbucket.org/AlephIcabod/worbuc/Functions"
	"bitbucket.org/AlephIcabod/worbuc/Constants"
	"bitbucket.org/AlephIcabod/worbuc/config"
)

var db *pg.DB
var once sync.Once

func GetDataBase() *pg.DB {
	var err error
	once.Do(func() {
		db = pg.Connect(&pg.Options{
			User:     config.GlobalConfig.Database.User,
			Password: config.GlobalConfig.Database.Password,
			Database: config.GlobalConfig.Database.Dbname,
			Addr:     config.GlobalConfig.Database.Host + ":" + config.GlobalConfig.Database.Port,
		})
		err = createSchema(db)
		if err != nil {
			panic(err)
		}
		err = initCountries(db)
		if err != nil {
			panic(err)
		}
		err = initPlains(db)
		if err != nil {
			panic(err)
		}

		err = initStates(db)
		if err != nil {
			panic(err)
		}
		err = initAdmin(db)
		if err != nil {
			panic(err)
		}
		err = initReunion(db)
		if err != nil {
			panic(err)
		}
	})

	return db
}

func createSchema(db *pg.DB) error {
	for _, model := range []interface{}{
		&Schemas.Plan{},
		&Schemas.Pais{},
		&Schemas.Estado{},
		&Schemas.Empresa{},
		&Schemas.Institucion{},
		&Schemas.Usuario{},
		&Schemas.Direccion{},
		&Schemas.Contacto{},
		&Schemas.PlanUsuario{},
		&Schemas.Telefono{},
		&Schemas.Educacion{},
		&Schemas.Empleo{},
		&Schemas.Libreta{},
		&Schemas.Proyecto{},
		&Schemas.UsuarioProyecto{},
		&Schemas.Reunion{},
		&Schemas.Participante{},
		&Schemas.Minuta{},
		&Schemas.Nota{},
		&Schemas.ArchivoReunion{},
		&Schemas.Actividad{},
		&Schemas.Tarea{},
		&Schemas.ParticipanteTarea{},
		&Schemas.ArchivoTarea{},
		&Schemas.ComentarioTarea{},
		&Schemas.Notification{},
		&Schemas.Sesion{},
	} {
		err := db.CreateTable(model, &orm.CreateTableOptions{
			FKConstraints: true,
			IfNotExists:   true,
		})

		if err != nil {
			return err
		}
	}
	return nil
}

func initCountries(db *pg.DB) error {
	var aux Schemas.Pais
	if err := db.Model(&aux).First(); err != nil {
		if err != pg.ErrNoRows {
			return err
		}
	}
	if aux.Id > 0 {
		return nil
	}
	countriesString, err := ioutil.ReadFile("./paises.csv")
	if err != nil {
		return err
	}
	ac := strings.Split(string(countriesString), "\n")
	for i := 0; i < len(ac); i++ {
		ac[i] = strings.Replace(ac[i], `"`, "", -1)
		cols := strings.Split(ac[i], ",")
		aux := Schemas.Pais{Nombre: cols[0], Codigo: cols[1]}
		err = db.Insert(&aux)
		if err != nil {
			fmt.Println("Error al insertar: ", i)
		}
	}
	return nil
}

func initStates(db *pg.DB) error {
	var aux Schemas.Estado
	if err := db.Model(&aux).First(); err != nil {
		if err != pg.ErrNoRows {
			return err
		}
	}
	if aux.Id > 0 {
		return nil
	}
	countriesString, err := ioutil.ReadFile("./ESTADOS.CSV")
	if err != nil {
		return err
	}
	ac := strings.Split(string(countriesString), "\n")
	for i := 0; i < len(ac); i++ {
		ac[i] = strings.Replace(ac[i], `"`, "", -1)
		cols := strings.Split(ac[i], ",")
		id, _ := strconv.ParseInt(cols[0], 10, 64)
		aux := Schemas.Estado{Nombre: cols[1], PaisID: id}
		err = db.Insert(&aux)
		if err != nil {
		}
	}
	return nil
}

func initAdmin(bd *pg.DB) error {
	var admin Schemas.Usuario
	err := bd.Model(&admin).Where(`is_admin=?`, true).First()
	if err != nil && err != pg.ErrNoRows {
		return err
	}
	if err == pg.ErrNoRows {
		pass, err := Functions.Encrypt("worbucpassword")
		if err != nil {
			return err
		}
		admin = Schemas.Usuario{Id: xid.New().String(),
			Nombre:     "worbuc_admin",
			Estatus:    Constants.REGISTRADO,
			IsAdmin:    true,
			Genero:     Constants.NOESPECIFICADO,
			Email:      "worbuc_admin@worbuc.com",
			TipoPerfil: Constants.PERFIIL_PRIVADO,
			Password:   pass,
		}
		err = db.Insert(&admin)
		if err != nil {
			return err
		}
		fmt.Println("Creado usuario administrador")
	}
	return nil
}

func initPlains(bd *pg.DB) error {
	var basico Schemas.Plan
	if err := db.Model(&basico).First(); err != nil {
		if err != pg.ErrNoRows {
			return err
		}
	}
	if basico.Id != "" {
		return nil
	}
	/*
	basico = Schemas.Plan{
		Id:          "",
		Nombre:      "Básico",
		Precio:      0.0,
		Descripcion: "Plan básico de reuniones y proyectos",
		Apps: []int{
			Constants.Aplicaciones["Reuniones"],
			Constants.Aplicaciones["Proyectos"],
		},
		Estatus:  Constants.ACTIVO,
		LimiteMb: 5,
	}
	return db.Insert(&basico)*/
	return nil
}

func initReunion(bd *pg.DB) error {
	var admin Schemas.Reunion
	err := bd.Model(&admin).Where(`id=?`, "PENDIENTE").First()
	if err != nil && err != pg.ErrNoRows {
		return err
	}
	if err == pg.ErrNoRows {
		admin = Schemas.Reunion{Id: "PENDIENTE",
			Titulo: "REUNION_AUXILIAR",
		}
		err = db.Insert(&admin)
		if err != nil {
			return err
		}
		fmt.Println("Creada reunion inicial")
	}
	return nil
}
